/* jshint ignore:start */
define(['jquery', 'jqueryui', 'core/ajax', 'core/templates', 'core/notification', 'mod_centraladmin/jtable', 'mod_centraladmin/select2'], function($, ui, ajax, templates, notification) {
        
    var str;
    var readOnlyMode;
    var isInit;
    var allInstancesList;
    var selectedInstances;
    var returnurl;

    var constantfields = {}
    var fields = {};    

    var roleSelected = false;
    var updatingRole = true;


    function init(lstr, readOnly, instances, backurl) {
        isInit = false;
        str = lstr;
        readOnlyMode = readOnly;
        allInstancesList = Object.entries(instances);
        selectedInstances = {};
        returnurl = backurl;

        constantfields = 
        {
            id : {
                type: 'hidden',
                key: true,
            },
            capability : {
                title: str.array_header_capability,
                width: "10%",
                create: false,
                edit: false,
                list: true
            },
            archetype: {
                title: str.array_header_archetype,
                width: "5%",
                create: false,
                edit: false,
                list: true,
                listClass: 'global-element'
            },
            changeall: {
                title: str.array_header_changeall,
                width: "5%",
                create: false,
                edit: false,
                list: true,
                listClass: 'global-element'
            }
        };

        $('.multi-select').select2({
            closeOnSelect : false,
            width: '100%'
        });
        $('.single-select').select2({
            closeOnSelect : true,
            width: '100%'
        });

        $('.multi-select').on('change', handleOnChangeFilter);
        $('.single-select').on('change', handleOnChangeFilter);
        $('#onlyshowdiffinstance').on('change', handleOnChangeFilter);
        $('#onlyshowdiffarchetype').on('change', handleOnChangeFilter);
        
        exportbtn = $('#manage-role-export');
        exportbtn.on('click', exportdata);
        exportbtn.prop('disabled', true); 
        
        //select role 
        $('#select-role').on('change', updateSelectedRole);

        //save button
        $('#manage-role-save').on('click', chargeModalRecap);

        // cancel button
        $('#manage-role-cancel').on('click', handleOnClickCancel);

        // reset button
        $('#manage-role-reset').on('click', handleOnClickReset);
        
        getupdatingrole();
    }
    
    function updateSelectedRole(){
        roleSelected = true;
        changeExportButtonState();
    }
    
    function getupdatingrole(){
        var promise = ajax.call([
            { methodname: 'centraladminconf_role_updating' , args: {}},
        ]);
        
        promise[0].done((response) => {
            updateStateEditingRole(response.data);
        });    
    }
    
    function updateStateEditingRole(count){
        updatingRole = count > 0 ? true : false;
        changeExportButtonState();
    }
    
    function changeExportButtonState(){
        if ( roleSelected && !updatingRole ){
            exportbtn.removeClass('btn-default disabled');
            exportbtn.prop('disabled', false);
        } else {
            exportbtn.addClass('btn-default disabled');
            exportbtn.prop('disabled', true);
        }
        updateExportTitle();
    }
    

    function updateExportTitle(){
        desc = '';

        if (roleSelected && !updatingRole) {
            desc = 'Cliquez pour exporter';
        } else {
            desc = 'Export impossible : '; 
            
            if (!roleSelected){
                desc += ' aucun role choisi';
            }
            
            if (!roleSelected && updatingRole) {
                desc += ' et';
            }
            if(updatingRole) {
                desc += ' des modifications sont en cours';
            }
        }
        desc += '.';
        
        exportbtn.attr('title' , desc);
    }
    
        
    function exportdata(){
        //download CSV
        var postdata = formdata();
        var promises = ajax.call([
            { methodname: 'centraladminconf_role_getexport', args: postdata },
        ])
        
        promises[0].done((response) => {
            var a = document.createElement('a');
            if (window.URL && window.Blob && ('download' in a) && window.atob) {
                var blob = base64ToBlob(response.file, 'text/octet-stream');
                var url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = getFileName(); 
                a.click();
                window.URL.revokeObjectURL(url);
            }
        });
        
    }
    
    function formatDate() {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('');
    }
    
    function getFileName(){
        
        date   = formatDate();
        data   = formdata();
        role   = data.roleshortname;
        suffix = '-magistere_permissions.csv';
        filename = date + '-'+ role + suffix;
        
        return filename;
    }
    
    function base64ToBlob(base64, mimetype, slicesize) {
        if (!window.atob || !window.Uint8Array) {
            return null;
        }
        mimetype = mimetype || '';
        slicesize = slicesize || 512;
        var bytechars = atob(base64);
        var bytearrays = [];
        for (var offset = 0; offset < bytechars.length; offset += slicesize) {
            var slice = bytechars.slice(offset, offset + slicesize);
            var bytenums = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                bytenums[i] = slice.charCodeAt(i);
            }
            var bytearray = new Uint8Array(bytenums);
            bytearrays[bytearrays.length] = bytearray;
        }
        return new Blob(bytearrays, {type: mimetype});
    }

    function initJtable() {
        $("#roletable").jtable({
            title: str.preview_header_title,
            paging: true,
            pageSize: 20,
            pageSizes: [20,30,50,100],
            selecting: false,
            multiselect: false,
            selectingCheckboxes: false,
            sorting: true,
            defaultSorting: "capability ASC",
            jqueryuiTheme: true,
            defaultDateFormat: "dd/mm/yy ",
            gotoPageArea: "none",
            selectOnRowClick: false,
            actions: {
                listAction: function (postData, jtParams) {
                    return $.Deferred(function ($dfd) {
                        var postdata = formdata();
                        postdata.si = (jtParams.jtStartIndex==undefined?0:jtParams.jtStartIndex);
                        postdata.ps = (jtParams.jtPageSize==undefined?0:jtParams.jtPageSize);
                        postdata.so = (jtParams.jtSorting==undefined?'':jtParams.jtSorting);
                        postdata.selected = null;

                        var promises = ajax.call([
                            { methodname: 'centraladminconf_role_get_capabilities', args: postdata },
                        ]);

                        promises[0].done((response) => {
                            $dfd.resolve(JSON.parse(response.data));
                        });
                    });
                }
            },
            fields: fields,
        });

        $("#roletable").on('change', 'select[name*="changepermission"]', function(){
            handleOnChangeSelect(this);
        });

        $("#roletable").on('change', 'select[name*="changeall"]', function(){
            select_value = this.value;
            if (select_value != null && select_value != "") {
                parent = $(this).parents("tr");
                parent.find('select[name*="changepermission"]').val(select_value).change();
            }
        });
    }

    function handleOnChangeFilter() {
        selectedInstances = [];
        instancesSelect = $('#instances-select .multi-select').select2('data').map(val => val.id);
        if (instancesSelect.length == 0) {
            allInstancesList.forEach(instance => {
                selectedInstances[instance[1]] = {
                    title: instance[1],
                    create: false,
                    edit: false,
                    list: true,
                    listClass: 'permission-cell',
                    display: (data) => {
                        if (!data.record.instances[instance[0]]) {
                            return 'Non défini'; // todo use lang string
                        }
                        return '<div class="'+data.record.instances[instance[0]].classes+'">'+data.record.instances[instance[0]].permission+'</div>';
                    }
                }
            });
        } else {
            allInstancesList.forEach(instance => {
                if (instancesSelect.includes(instance[0])) {
                    selectedInstances[instance[1]] = {
                        title: instance[1],
                        create: false,
                        edit: false,
                        list: true,
                        listClass: 'permission-cell',
                        display: (data) => {
                            if (!data.record.instances[instance[0]]) {
                                return 'Non défini'; // todo use lang string
                            }
                            return '<div class="'+data.record.instances[instance[0]].classes+'">'+data.record.instances[instance[0]].permission+'</div>';
                        }
                    }
                }
            });
        }
        fields = {
            ...constantfields,
            ...selectedInstances
        }

        if (isInit) {
            $('#roletable').jtable('destroy');
        }
        
        initJtable();
        isInit = true;
        $('#roletable').jtable('load');
        
    }
    
    function formdata() {
        var data = new Object();

        data.roleshortname = $('#role-select option:selected').val();
        data.instances = $('#instances-select .multi-select').select2('data').map(val => val.id);
        data.capabilities = $('#capabilities-select .multi-select').select2('data').map(val => val.id);

        data.diffinstance = $('#onlyshowdiffinstance').is(":checked");
        data.diffarchetype = $('#onlyshowdiffarchetype').is(":checked");
        
        return data;
    }

    function handleOnChangeSelect(select) {
        var postdata = formdata();
        postdata.si = null;
        postdata.ps = null;
        postdata.so = null;
        postdata.selected = select.value;
        postdata.capabilities = [$(select).parents("tr").find("td:first-child").text()];
        postdata.instances = [$(select).attr("id").split('_')[2]];

        var promises = ajax.call([
            { methodname: 'centraladminconf_role_get_capabilities', args: postdata },
        ]);

        promises[0].done((response) => {
            data = JSON.parse(response.data);
            instances = data.Records[0].instances;
            classes = instances[Object.keys(instances)[0]].classes;
            parent = $(select).parent();
            parent.removeClass();
            parent.addClass(classes);
            getupdatingrole();
        });

        
    }

    function chargeModalRecap(){
        promise = ajax.call([
            { methodname: 'centraladminconf_role_get_recap', args: []}
        ]);

        promise[0].done((response) => {
            data = JSON.parse(response.data);
            templates.render('centraladminconf_role/modal-body', data)
                .then(function (html, js) {
                    $('.modal-body').remove();
                    $('.modal-footer').before(html);
                    $('#manage-role-confirm').on('click', () => {
                        var promise = handleOnClickConfirm();
                        promise.done((response) => {
                            if (isInit) {
                                $("#roletable").jtable('reload');
                            }
                            $('#roleRecapModal').modal('hide');
                            notification.addNotification({
                                message: "Les permissions ont bien été mises à jour",
                                type: "success"
                            });
                        });
                    });
                });
        }).fail(function (problem) {
            // handle failure
            notification.addNotification({
                message: "Erreur lors de la mise à jour des permissions",
                type: "error"
              });
        });
    }

    function handleOnClickCancel() {
        location.href = returnurl;
    }
    
    function handleOnClickReset() {
        var data = new Object();
        promiseResetTodo = ajax.call([
            { methodname: 'centraladminconf_role_reset_todo', args: data}
        ]);

        promiseResetTodo[0].done((response) => {
            $("#roletable").jtable('reload');
            getupdatingrole();
            notification.addNotification({
                message: "Les modifications en cours ont bien été annulées",
                type: "success"
            });
        });
    }

    function handleOnClickConfirm() {
        // make the query to confirm
        var data = new Object();
        promise = ajax.call([
            { methodname: 'centraladminconf_role_confirm', args: data}
        ]);

        return promise[0];
    }

    return {
        init : function(str, readOnlyMode, instances, returnurl) {
            init(str, readOnlyMode, instances, returnurl);
        }
    };

});
