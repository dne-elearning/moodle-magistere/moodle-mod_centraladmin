<?php

require_once(dirname(dirname(dirname(dirname(__DIR__)))).'/config.php');

require_once($CFG->dirroot.'/mod/centraladmin/conf/role/role.php');
require_once($CFG->dirroot.'/mod/centraladmin/conf/role/recap_role_todo.php');
require_once($CFG->dirroot.'/mod/centraladmin/conf/role/lib.php');

$context = context_system::instance();

require_capability('centraladminconf/role:viewpermissions', $context);

$readOnlyMode = !has_capability('centraladminconf/role:managepermissions', $context);

$PAGE->set_cacheable(false);

$PAGE->set_context($context);
$PAGE->set_url('/mod/centraladmin/conf/role/manage.php');
$PAGE->set_pagelayout('standard');

$returnurl = new moodle_url($CFG->wwwroot);

$strs = new stdClass();

$strs->array_header_capability = get_string('array_header_capability','centraladminconf_role');
$strs->array_header_archetype = get_string('array_header_archetype','centraladminconf_role');
$strs->array_header_changeall = get_string('array_header_changeall','centraladminconf_role');

$confrole = new centraladminconf_role();
$status = $confrole->get_cache_status();
if ($status != centraladminconf_role::CACHEUPDATE_NONE){
    $PAGE->requires->js_call_amd('centraladminconf_role/role_perm_update', 'init');
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading('Mise à jour des données');
    echo $OUTPUT->footer();
    exit;
}

$roles = $confrole->getRolesNames();
$instances = $confrole->getInstances();
$capabilities = $confrole->getCapabilities();
$recap = new recap_role_todo();

$PAGE->requires->js_call_amd('centraladminconf_role/manage_role', 'init', array('str'=>$strs, 'readOnlyMode' => $readOnlyMode, 'instances' => $instances, 'returnurl' => $returnurl->out()));

$PAGE->set_title(get_string('pagedesc', 'centraladminconf_role'));
$PAGE->set_heading(get_string('pagedesc', 'centraladminconf_role'));
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pagedesc', 'centraladminconf_role'));

$select_role = createMultiSelect('select-role', array_keys($roles), array_values($roles), 'single-select', false);
$select_instances =  createMultiSelect('select-instances', array_keys($instances), array_values($instances), 'multi-select');
$select_capabilities = createMultiSelect('select-capabilities', array_values($capabilities), array_values($capabilities), 'multi-select');

echo $OUTPUT->render_from_template('centraladminconf_role/manage', array(
    'select_role' => $select_role,
    'select_instances' => $select_instances,
    'select_capabilities' => $select_capabilities,
    'notreadonly' => !$readOnlyMode
));

echo $OUTPUT->render_from_template('centraladminconf_role/recap', $recap);

echo $OUTPUT->footer();
