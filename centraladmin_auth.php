<?php

defined('MOODLE_INTERNAL') || die();

require_once($GLOBALS['CFG']->dirroot.'/mod/centraladmin/SlaveQuery.php');

class centraladmin_auth {
    
    public const ROLE_STATSUSER = 'statsuser';

    function __construct(){
        
    }

    /** Get user mainacademy
     *
     */
    private static function get_user_mainaca($username){
        $sql = "
SELECT uid.data
FROM [[[MAGDB_frontal]]].{user} u
INNER JOIN [[[MAGDB_frontal]]].{user_info_data} uid ON (uid.userid = u.id)
WHERE uid.fieldid = (
  SELECT id
  FROM [[[MAGDB_frontal]]].{user_info_field} uif
  WHERE uif.shortname = 'mainacademy'
)
AND u.auth = 'shibboleth'
AND u.username = :username";

        $slavename = 'dgesco';

        $slave = SlaveQuery::get_slave_by_name($slavename);

        if ($slave === false){
            return false;
        }

        $records = SlaveQuery::get_records_sql($slave, $sql, array('username'=>$username));

        if (count($records) != 1){
            return false;
        }

        return array_shift($records)->data;
    }

    /** Get user mainacademy
     *
     */
    private static function user_is_localadmin($username, $mainaca){
        $slave = SlaveQuery::get_slave_by_name($mainaca);

        if ($slave === false){
            return false;
        }

        $sql = "
SELECT *
FROM {user} u
INNER JOIN {role_assignments} ra ON (ra.userid = u.id)
INNER JOIN {role} r ON(r.id = ra.roleid)
WHERE u.username = :username
AND ra.contextid = :contextid
AND r.shortname = :roleshortname";

        $context = CONTEXT_SYSTEM::instance();

        $param = array(
            'username'=>$username,
            'contextid' =>$context->id ,
            'roleshortname'=>'administrateurlocal'
        );

        $records = SlaveQuery::get_records_sql($slave, $sql, $param);

        return count($records) != 0;
    }

    /**
     * Used to define the rights for a new user (only at the first connexion)
     * @param  $user : the user
     * @return boolean
     */
    public static function auth_user($user){
        global $CFG, $DB;

        // Execute only once at the first connection
        if ($user->timecreated < time()-15){
            return false;
        }

        $mainaca = self::get_user_mainaca($user->username);

        if (!$mainaca){
            return false;
        }

        $islocaladmin = self::user_is_localadmin($user->username, $mainaca);

        if (!$islocaladmin){
            return false;
        }

        // list contain only one user (for using next permission functions)
        $users = array($user->id => $user->id);

        $role = $DB->get_record('role', array('shortname' => self::ROLE_STATSUSER));
        
        if ($role === false) {
            mtrace('Role "'.$role.'" not found');
            return false;
        }
        
        $context = context_system::instance();
        role_assign($role->id, $user->id, $context->id);
        
        // list all plugins (stats) / subplugin
        $views = array();
        $enabled_plugins = array_keys(\mod_centraladmin\plugininfo\centraladminstats::get_enabled_plugins());
        foreach($enabled_plugins as $plugin){
            $classPath = $CFG->dirroot.'/mod/centraladmin/stats/'.$plugin.'/'.$plugin.'.php';
            $views['centraladminstats_'.$plugin] = true;
            if (file_exists($classPath)) {
                require_once($classPath);
                $classname = 'centraladminstats_'.$plugin;
                $statsplugininstance = new $classname;
                foreach($statsplugininstance->get_subplugins() as $subplugin){
                    $views[$subplugin['type'].'_'.$subplugin['id']] = true;
                }
            }
        }

        centraladmin_permission_set_view($users, $views, true);
        centraladmin_permission_set_instance($users, array($mainaca => $mainaca), true);
    }

    /** Update autoset permissions
     * @param $user
     */
    public static function update_permissions(){
        global $DB;

        $users = $DB->get_records_sql("
SELECT DISTINCT u.id, u.username
FROM {user} u
WHERE u.id IN (
  SELECT DISTINCT cpi.userid
  FROM {centraladmin_perms_instance} cpi
  WHERE cpi.autoset = 1
  UNION
  SELECT DISTINCT cpp.userid
  FROM {centraladmin_perms_plugin} cpp
  WHERE cpp.autoset = 1
)");

        mtrace('We found '.count($users).' users to check');

        foreach ($users AS $user) {
            mtrace('Checking user : "'.$user->username.'" ('.$user->id.')');

            $mainaca = self::get_user_mainaca($user->username);

            if (!$mainaca) {
                mtrace('=> Mainaca not found');
            }

            $islocaladmin = self::user_is_localadmin($user->username, $mainaca);

            if (!$islocaladmin) {
                mtrace('=> The user is not a localadmin, we remove his permissions');
                $fakeuser = array($user->id => $user->id);
                centraladmin_permission_set_view($fakeuser, array());
                centraladmin_permission_set_instance($fakeuser, array());
            } else {
                mtrace('=> The user is still a localadmin, we keep his permissions');
            }
        }

    }
}
