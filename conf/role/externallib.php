<?php

use mod_centraladmin\plugininfo\centraladminconf;

require_once($CFG->libdir.'/externallib.php');
require_once($CFG->dirroot.'/mod/centraladmin/conf/role/role.php');
require_once($CFG->dirroot.'/mod/centraladmin/conf/role/recap_role_todo.php');

class centraladminconf_role_external extends external_api {

    public const PLUGIN_NAME = 'centraladminconf_role';
    
    public static function get_capabilities_parameters() {
        return new external_function_parameters(
            array(
                'roleshortname'   => new external_value(PARAM_RAW, ''),
                'instances'  => new external_multiple_structure(new external_value(PARAM_INT, '')),
                'capabilities' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'diffinstance'=> new external_value(PARAM_BOOL, ''),
                'diffarchetype'=> new external_value(PARAM_BOOL, ''),
                'si' => new external_value(PARAM_INT, ''),
                'ps' => new external_value(PARAM_INT, ''),
                'so' => new external_value(PARAM_RAW, ''),
                'selected' => new external_value(PARAM_RAW, ''),
            )
        );
    }
    
    public static function get_capabilities_returns() {
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_RAW, '')
            )
        );
    }
    
    public static function get_capabilities($roleshortname, $instances, $capabilities, $diffinstance, $diffarchetype, $si, $ps, $so, $selected = null) {
        $confrole = new centraladminconf_role();

        $context = context_system::instance();

        $readOnly = !has_capability('centraladminconf/role:managepermissions', $context);
        $results = $confrole->get_permissions($roleshortname, $instances, $capabilities, $diffinstance, $diffarchetype, $si, $ps, $so, $selected, $readOnly);

        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $results['count'];
        $jTableResult['Records'] = array_values($results['result']);
        return array('data' => json_encode($jTableResult));
    }

    public static function get_recap() {
        $recap = new recap_role_todo();

        return array('data' => json_encode($recap));
    }

    public static function get_recap_returns() {
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_RAW, '')
            )
        );
    }

    public static function get_recap_parameters() {
        return new external_function_parameters(
            array()
        );
    }
    
    public static function get_export_returns() {
        return new external_single_structure(
            array(
                'file' => new external_value(PARAM_RAW, '')
            )
        );
    }
     
    public static function get_export_parameters() {
        return new external_function_parameters(
            array(
                'roleshortname'   => new external_value(PARAM_RAW, ''),
                'instances'  => new external_multiple_structure(new external_value(PARAM_INT, '')),
                'capabilities' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'diffinstance'=> new external_value(PARAM_BOOL, ''),
                'diffarchetype'=> new external_value(PARAM_BOOL, ''),
            )
        );
    }
    
    public static function get_export($roleshortname, $instances, $capabilities, $diffinstance, $diffarchetype){
        
        $confrole = new centraladminconf_role();
        $results = $confrole->get_permissions($roleshortname, $instances, $capabilities, $diffinstance, $diffarchetype, null, null, null, null, true, true);
        $records = $results['result'];
        $delimiter = get_string('listsep','core_langconfig');
        
        $headers = array(
            get_string('array_header_capability','centraladminconf_role'),
            get_string('array_header_archetype','centraladminconf_role'),
        );
        
        
        if ($instances == null){
            $instances = $confrole->getInstances();
        }
        
        foreach ( $instances as $instancename){
            $headers[] = $instancename;
        }
        
        $filename = tempnam(sys_get_temp_dir(),'exp');
        $tfile = fopen($filename,'w+');
        
        fputs($tfile, (chr(0xEF) . chr(0xBB) . chr(0xBF)));     
        fputcsv($tfile, $headers, $delimiter);
            
        foreach ($records as $record) {
            $line = array();
            $line[] = $record->capability;
            $line[] = $record->archetype;
            foreach($record->instances as $instanceInfo) {
                $line[] = $instanceInfo->permission;
            }
            fputcsv($tfile, $line, $delimiter);
        }
        
        fclose($tfile);
        $csvfile = base64_encode(file_get_contents($filename));
        
        return array('file'=> $csvfile);
    }
    
    public static function role_updating_returns() {
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_INT, '')
            )
        );
    }
    
    public static function role_updating_parameters() {
        return new external_function_parameters(
            array()
        );
    }
    
    public static function role_updating() {
        global $DB, $USER;
        $count = $DB->count_records(centraladminconf_role::TABLE_ROLE_TODO, array('userid' => $USER->id));
        return array('data' => $count);
    }

    public static function reset_todo_returns() {
        return new external_single_structure(
            array()
        );
    }
    
    public static function reset_todo_parameters() {
        return new external_function_parameters(
            array(
                'userid' => new external_value(PARAM_INT, '', VALUE_DEFAULT, null)
            )
        );
    }
    
    public static function reset_todo($userid) {
        global $DB, $USER;
        if (is_null($userid)) {
            $userid = $USER->id;
        }
        $context = context_system::instance();
        require_capability('centraladminconf/role:managepermissions', $context, $userid);


        $DB->delete_records(centraladminconf_role::TABLE_ROLE_TODO, array('userid' => $userid));
        return array();
    }
    
    public static function confirm_returns() {
        return new external_single_structure(
            array()
        );
    }
    
    public static function confirm_parameters() {
        return new external_function_parameters(
            array(
                'userid' => new external_value(PARAM_INT, '', VALUE_DEFAULT, null)
            )
        );
    }
    
    public static function confirm($userid) {
        global $USER;
        if (is_null($userid)) {
            $userid = $USER->id;
        }
        
        $context = context_system::instance();
        require_capability('centraladminconf/role:managepermissions', $context, $userid);
        
        $confrole = new centraladminconf_role();
        $confrole->propagatePermission($userid);
        
        return array();
    }
    
    
    
    public static function updatecache_parameters() {
        return new external_function_parameters(array());
    }
    
    public static function updatecache_returns() {
        return new external_single_structure(
            array(
                'status' => new external_value(PARAM_ALPHA)
            )
        );
    }
    
    public static function updatecache() {
        
        require_capability('centraladminconf/role:managepermissions', context_system::instance());
        
        $role = new centraladminconf_role();
        $cs = $role->get_cache_status();
        if ($cs == centraladminconf_role::CACHEUPDATE_RUNNING){
            return array('status'=>'running');
        }else if ($cs == centraladminconf_role::CACHEUPDATE_TODO){
            $role->refresh_cache();
            return array('status'=>'todo');
        }
        return array('status'=>'noupdate');
    }
    
    
    
}

