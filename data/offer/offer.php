<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/centraladmin/centraladmindata_plugin.php');

class centraladmindata_offer extends centraladmindata_plugin {
    
    public static $tablename = 'centraladmindata_offer';
    public static $completion_tablename = 'centraladmindata_offercmpl';
    public static $attendance_tablename = 'centraladmindata_attendance';

    public function get_name() {
        return get_string('pluginname', 'centraladmindata_offer');
    }

    /*
     * Return the last updated timecode
     */
    public static function get_lastupdate_timecode_str() {
        $timestamp = get_config('centraladmindata_offer', 'lastupdate_timestamp');
        if (!$timestamp) {
            return get_string('lastupdate_never', 'centraladmindata_offer');
        } else {
            return date('d/m/Y H:i', $timestamp);
        }
    }
    
}