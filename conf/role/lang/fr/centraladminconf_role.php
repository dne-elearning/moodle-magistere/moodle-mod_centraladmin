<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'centraladmin', language 'en'
 *
 * @package   mod_centraladmin
 * @copyright 2022 Reseau Canope
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$string['pluginname'] = 'Gestion des rôles';
$string['pagedesc'] = 'Gestion des permissions';
$string['cacherefreshdelay'] = 'Délai de rafraichissement du cache';
$string['cacherefreshdelay_desc'] = 'Délai de rafraichissement du cache des permissions (en seconde)';
$string['array_header_capability'] = 'Capacité';
$string['array_header_archetype'] = 'Archetype def';
$string['array_header_changeall'] = 'Tous';
$string['role'] = 'Rôle';
$string['instances'] = 'Instances';
$string['capabilities'] = 'Capacités';
$string['onlyshowdiffarchetype'] = 'N\'afficher que les capacités qui diffèrent de l\'archétype du rôle sélectionné';
$string['onlyshowdiffinstance'] = 'N\'afficher que les capacités qui diffèrent sur certains domaines';
$string['filter'] = 'Filtrer';
$string['save'] = 'Enregistrer';
$string['cancel'] = 'Annuler';
$string['export'] = 'Exporter';
$string['reset'] = 'Réinitialiser';
$string['changessummary'] = 'Résumé des modifications';
$string['todolifetime'] = 'Durée de vie des modifications temporaires';
$string['todolifetime_desc'] = 'Les modifications temporaires des permissions seront effacées au bout de cette durée (en seconde)';
$string['clean_todos'] = 'Nettoyage des modifications temporaires des permissions';
$string['sync_archetype'] = 'Sync archetype';
$string['changerequestsrecap'] = '{$a->recap_capa} modifications de permission au total sur {$a->recap_instances} instances.';
$string['conflictsrecap'] = '<p>Il y a {$a} conflits visibles ci dessous.</p><p>Attention, sauvegarder ces changements écrasera les précédents conflits.</p>';
$string['role:viewpermissions'] = 'View the manage capabilities page';
$string['role:managepermissions'] = 'Manage the capabilities of the instances';