/* jshint ignore:start */
define(['jquery', 'core/ajax', 'mod_centraladmin/colors'], function($, ajax, colors) {

	var hcharts;
    function init() {
        
    	hcharts = Highcharts.chart('attendance', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Participation aux formations en libre accès'
            },
            xAxis: {
                categories: ['1D','2D','Non-défini']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Nb participants'
                },
                minTickInterval: 1,
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Participants actifs',
                data : [],
            },{
                name: 'Participants',
                data : [],
            }],
            noData: {
                style: {
                    fontWeight: 'bold',
                    fontSize: '15px',
                    color: '#303030'
                }
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}'
            },
            lang: {
                noData: "Aucune donnée à afficher"
            },
            credits: {
                enabled: false,
            },
            colors: colors.greenbluegradient,
        });
    	
    }
    
    function refresh(formdata) {
    	// refresh graph stats
    	formdata.interval = 'none';
    	
    	var promises = ajax.call([
            { methodname: 'centraladmincollec_attendance_per_degree', args: formdata },
        ])

        promises[0].done(function(response) {
            hcharts.series[0].setData(response.attended.split(',').map(Number));
        	hcharts.series[1].setData(response.enroled.split(',').map(Number));
        	
        }).fail(function(ex) {
            document.getElementById('attendance').innerHTML = '<p>Une erreur est survenue lors de la récupération des données du graphiques "Répartition des formations par plateforme d\'origines"</p>';
            console.error(ex);
        });
    }
    
	return {
    	init : function() {
            init();
        },
        refresh : function(formdata) {
        	refresh(formdata);
        }
    };
});
