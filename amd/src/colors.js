define([],function() {
    
    // these colors are from the marianne graphical chart
    var list = {
        a: ['#958B62', '#958B627F'],
        b: ['#91AE4F', '#91AE4F7F'],
        c: ['#169B62', '#169B627F'],
        d: ['#466964', '#4669647F'],
        e: ['#00AC8C', '#00AC8C7F'],
        f: ['#5770BE', '#5770BE7F'],
        g: ['#484D7A', '#484D7A7F'],
        h: ['#FF8D7E', '#FF8D7E7F'],
        i: ['#D08A77', '#D08A777F'],
        j: ['#FFC29E', '#FFC29E7F'],
        k: ['#FFE800', '#FFE8007F'],
        l: ['#FDCF41', '#FDCF417F'],
        m: ['#FF9940', '#FF99407F'],
        n: ['#E18B63', '#E18B637F'],
        o: ['#FF6F4C', '#FF6F4C7F'],
        p: ['#7D4E5B', '#7D4E5B7F'],
        r: ['#A26859', '#A268597F'],
    };

    var greenbluegradient = [
        '#91AE4F',
        '#169B62',
        '#466964',
        '#00AC8C',
        '#5770BE',
        '#484D7A',
        '#91AE4F7F',
        '#169B627F',
        '#4669647F',
        '#00AC8C7F',
        '#5770BE7F',
        '#484D7A7F',
    ];

    var yellowredgradient = [
        '#FFE800',
        '#FDCF41',
        '#FF9940',
        '#E18B63',
        '#FF6F4C',
        '#7D4E5B',
        '#FFE8007F',
        '#FDCF417F',
        '#FF99407F',
        '#E18B637F',
        '#FF6F4C7F',
        '#7D4E5B7F',
    ];



    return {
        list,
        greenbluegradient,
        yellowredgradient
    }

});