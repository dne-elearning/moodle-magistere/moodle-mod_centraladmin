define(["core/ajax", "mod_centraladmin/colors"],function(ajax, colors) {
    function init() { 

        var divId = 'courses_by_origin';

        var promises = ajax.call([
            { methodname: 'centraladminstats_offerview_origins', args: {} },
        ])

        promises[0].done(function(response) {

            var origines = response.map(val => val.origine)

            var series = [{
                name: 'Nombre de formations',
                data : response.map((val) => val.nbcourses),
            }];


            Highcharts.chart(divId, {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Répartition des formations par plateforme d\'origines'
                },
                xAxis: {
                    categories: origines
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Nombre de formations'
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: series,
                noData: {
                    style: {
                        fontWeight: 'bold',
                        fontSize: '15px',
                        color: '#303030'
                    }
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}'
                },
                lang: {
                    noData: "Aucune donnée à afficher"
                },
                credits: {
                    enabled: false,
                },
                colors: [colors.list.h[0]],
            });

        }).fail(function(ex) {
            document.getElementById(divId).innerHTML = '<p>Une erreur est survenue lors de la récupération des données du graphiques "Répartition des formations par plateforme d\'origines"</p>';
            console.error(ex);
        });

    }

    return {
    	init : function() {
            init();
        }
    };
});