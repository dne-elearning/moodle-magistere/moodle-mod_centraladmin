<?php

defined('MOODLE_INTERNAL') || die();

class SlaveQuery {
    
    public const MASTER_SERVICE_NAME = 'mod_centraladmin_service';
    public const SLAVE_SERVICE_NAME = 'local_centraladmin_agent_service';
    
    private function __construct() {}
    
    
    public static function pair($url, $username, $password) {
        global $DB, $CFG;
        
        $url = trim($url, '/');
        $oldslave = $DB->get_record_sql('SELECT * FROM {centraladmin_slave} WHERE url = ?', array($url));
        if ( $oldslave !== false && $oldslave->deleted == 0){
            throw new Exception("Error: slave '".$url."' already exist in the DB");
        }
        
        $query = new stdClass();
        $query->url = $CFG->wwwroot;
        $query->username = $username;
        $query->password = $password;
        $query->servicename = self::MASTER_SERVICE_NAME;
        $query->wsfunction = 'local_centraladmin_agent_pair';
        $query->moodlewsrestformat = 'json';
        
        list($query->wstoken,$privatetoken) = self::get_token($url, $username, $password);
        
        list($error, $resAPI) = self::post(self::get_webservice_url($url), $query);
        
        if ($error) {
            throw new Exception("MASTER:CURL http query error \n#######\nError no: ".$error."\n".$resAPI."\n#######");
        }
        
        $decodedAPIRes = json_decode($resAPI);
        if ($decodedAPIRes === null) {
            throw new Exception("MASTER:json cannot be decoded for payload \n#######\n".$resAPI."\n#######");
        } else if($decodedAPIRes->error) {
            throw new Exception("MASTER:API returned an error \n#######\n".$decodedAPIRes->errormsg."\n#######");
        }
        
        $slave = new stdClass();
        $slave->name = $decodedAPIRes->name;
        $slave->disabled = 0;
        $slave->deleted = 0;
        $slave->url = $url;
        $slave->token = $query->wstoken;
        $slave->private_token = $privatetoken;
        $slave->username = $username;
        $slave->password = $password;
        $slave->timecreated = time();
        $slave->timemodified = time();
        $slave->lastquery = time();
        
        if (isset($oldslave->deleted) && $oldslave->deleted == 1) {
            $slave->id = $oldslave->id;
            $DB->update_record('centraladmin_slave', $slave);
        }else{
            $DB->insert_record('centraladmin_slave', $slave);
        }
        
        return true;
    }
    
    public static function unpair($slaveid) {
        global $DB, $CFG;
        
        $slave = $DB->get_record_sql('SELECT * FROM {centraladmin_slave} WHERE id = ?', array($slaveid));
        if ( $slave === false || $slave->deleted == 1){
            throw new Exception("Error: slave '".$slaveid."' does not exist in the DB");
        }
        
        $query = new stdClass();
        $query->url = $CFG->wwwroot;
        $query->wsfunction = 'local_centraladmin_agent_unpair';
        $query->moodlewsrestformat = 'json';
        
        $res = self::api_query($slave, $query);
        
        if ($res->error){
            throw new Exception("MASTER:API query error \n#######\n".$res->errormsg."\n#######");
        }
        
        //TODO call all subplugins pre slave delete function
        /*
        // Allow plugins to use this slave before we completely delete it.
        if ($pluginsfunction = get_plugins_with_function('pre_slave_delete')) {
            foreach ($pluginsfunction as $plugintype => $plugins) {
                foreach ($plugins as $pluginfunction) {
                    $pluginfunction($course);
                }
            }
        }
        */
        
        //$DB->delete_records('centraladmin_slave', array('id'=>$slave->id));
        $slave->deleted = 1;
        $DB->update_record('centraladmin_slave', $slave);
        
        return true;
    }
    
    public static function ping($slave) {
        $query = new stdClass();
        $query->wsfunction = 'local_centraladmin_agent_ping';
        $query->moodlewsrestformat = 'json';
        
        self::api_query($slave, $query);
        
        return true;
    }
    
    public static function api_query($slave, $query) {
        global $DB;
        $query->wstoken = $slave->token;
        $query->moodlewsrestformat = 'json';
        $i=0;
        while($i<5){
            list($error, $response) = self::post(self::get_webservice_url($slave->url), $query);
            
            if ($error) {
                throw new Exception("MASTER:CURL http query error \n#######\nError no: ".$response."\n#######");
            }
            
            $decodedResponse = json_decode($response);
            if ($decodedResponse === null) {
                if ($i<3){
                    self::repair_slave($slave);
                }else{
                    throw new Exception("MASTER:json cannot be decoded for payload \n#######\n".$response."\n#######");
                }
            } else if(isset($decodedResponse->error) && $decodedResponse->error) {
                if ($i<3){
                    self::repair_slave($slave);
                }else{
                    throw new Exception("MASTER:API returned an error \n#######\n".$decodedResponse->errormsg."\n#######");
                }
            } else if(isset($decodedResponse->errorcode) && $decodedResponse->errorcode == 'invalidtoken') {
                if ($i==3){
                    throw new Exception("MASTER:API returned an invalidtoken exception and we can't get a new token \n#######\n".$decodedResponse->errormsg."\n#######");
                }
                
                if ($i==2){
                    self::repair_slave($slave);
                }
                
                try{
                    list($query->wstoken) = self::get_token($slave->url, $slave->username, $slave->password);
                }catch(Exception $e){
                    self::repair_slave($slave);
                    list($query->wstoken) = self::get_token($slave->url, $slave->username, $slave->password);
                }
                $slave->token = $query->wstoken;
                $slave->lastquery = time();
                $DB->update_record('centraladmin_slave', $slave);
                $i++;
                continue;
            } else if(isset($decodedResponse->errorcode)){
                if ($i<3){
                    self::repair_slave($slave);
                }else{
                    throw new Exception("MASTER:API returned an exception \n#######\n".(isset($decodedResponse->errormsg)?$decodedResponse->errormsg:$decodedResponse->message)."\n#######");
                }
            }
            break;
        }
        return $decodedResponse;
    }
    
    private static function repair_slave($slave){
        global $CFG;
        $url = $slave->url.'/local/centraladmin_agent/checkandrepair.php';
        
        $query = new stdClass();
        $query->k = 'PyESrq1vmVzaBSnN6W1VcPitqw04MT1CEWbU2VvkIVkZh5qpOV';
        
        $cprocess = curl_init($url);
        curl_setopt($cprocess, CURLOPT_POST, true);
        curl_setopt($cprocess, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cprocess, CURLOPT_REFERER, '');
        curl_setopt($cprocess, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($cprocess, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($cprocess, CURLOPT_POSTFIELDS, $query);
        
        if (!empty($CFG->proxyhost) && !is_proxybypass($url)) {
            if ($CFG->proxyport === '0') {
                curl_setopt($cprocess, CURLOPT_PROXY, $CFG->proxyhost);
            } else {
                curl_setopt($cprocess, CURLOPT_PROXY, $CFG->proxyhost.':'.$CFG->proxyport);
            }
        }
        curl_exec($cprocess);
    }
    
    
    public static function get_token($url, $username, $password, $service = self::SLAVE_SERVICE_NAME) {
        
        $query = new stdClass();
        $query->url = $url;
        $query->username = $username;
        $query->password = $password;
        $query->service = $service;
        
        list($error, $resToken) = self::post(self::get_token_url($url), $query);
        
        if ($error) {
            throw new Exception("MASTER_TOKEN:CURL http query error \n#######\nError no: ".$error."\n".$resToken."\n#######");
        }
        
        $decodedTokenRes = json_decode($resToken);
        if ($decodedTokenRes === null) {
            throw new Exception("MASTER_TOKEN:json cannot be decoded for payload \n#######\n".$resToken."\n#######");
        } else if(isset($decodedTokenRes->error)) {
            throw new Exception("MASTER_TOKEN:API returned an error \n#######\n".$resToken."\n#######");
        }
        
        return array($decodedTokenRes->token,$decodedTokenRes->privatetoken);
    }

    public static function get_and_save_ws_token($slave, $url, $username, $password, $service) {
        global $DB;
        list($token, $privatetoken) = self::get_token($url, $username, $password, $service);

        $tokeninfo = new stdClass();
        $tokeninfo->slaveid = $slave->id;
        $tokeninfo->servicename = $service;
        $tokeninfo->token = $token;
        $tokeninfo->private_token = $privatetoken;
        $tokeninfo->username = $username;
        $tokeninfo->password = $password;
        $tokeninfo->timecreated = time();
        $tokeninfo->timemodified = time();
        $tokeninfo->lastquery = time();

        if ($record = $DB->get_record('centraladmin_token', array('slaveid' => $tokeninfo->slaveid, 'servicename' => $tokeninfo->servicename, 'username' => $tokeninfo->username))) {
            $tokeninfo->id = $record->id;
            $tokeninfo->timecreated = $record->timecreated;
            $DB->update_record('centraladmin_token', $tokeninfo);
        } else {
            $DB->insert_record('centraladmin_token', $tokeninfo);
        }

        return array($token, $privatetoken);

    }

    /**
     * Generic function to call any webservice, token is saved for futur use
     */
    public static function call_ws($slave, $service, $function, $username, $password, $params = array()) {
        global $DB;

        $query = new stdClass();
        $query->wsfunction = $function;
        $query->moodlewsrestformat = 'json';
        foreach($params AS $name=>$value) {
            $query->$name = $value;
        }
        
        if ($tokeninfo = $DB->get_record('centraladmin_token', array('slaveid' => $slave->id, 'servicename' => $service, 'username' => $username))) {
            $query->wstoken = $tokeninfo->token;
        } else {
            list($query->wstoken) = self::get_and_save_ws_token($slave, $slave->url, $username, $password, $service);
        }

        $i=0;
        while($i<5){
            list($error, $response) = self::post(self::get_webservice_url($slave->url), $query);
            
            if ($error) {
                throw new Exception("MASTER:CURL http call_ws error \n#######\nError no: ".$response."\n#######");
            }
            
            $decodedResponse = json_decode($response);
            if ($decodedResponse === null) {
                throw new Exception("MASTER:json cannot be decoded for payload \n#######\n".$response."\n#######");
            } else if(isset($decodedResponse->error) && $decodedResponse->error) {
                throw new Exception("MASTER:API returned an error \n#######\n".$decodedResponse->errormsg."\n#######");
            } else if(isset($decodedResponse->errorcode) && $decodedResponse->errorcode == 'invalidtoken') {
                if ($i==2){
                    throw new Exception("MASTER:API returned an invalidtoken exception and we can't get a new token \n#######\n".$decodedResponse->errormsg."\n#######");
                }
                
                list($query->wstoken) = self::get_and_save_ws_token($slave, $slave->url, $username, $password, $service);

                $i++;
                continue;
            } else if(isset($decodedResponse->errorcode)){
                throw new Exception("MASTER:API returned an exception \n#######\n".(isset($decodedResponse->errormsg)?$decodedResponse->errormsg:$decodedResponse->message)."\n#######");
            }
            break;
        }
        return $decodedResponse;


    }
    
    public static function get_records_sql($slave, $sql, $params = array()) {
        
        $query = new stdClass();
        $query->wsfunction = 'local_centraladmin_agent_get_records_sql';
        $query->query = self::encode($sql);
        $query->params = self::encode($params);
        
        $result = self::slave_query($slave,$query);
        
        return self::decode($result->result);
    }
    
    public static function get_records_sql_allslave($sql, $params) {
        
        $query = new stdClass();
        $query->wsfunction = 'local_centraladmin_agent_get_records_sql';
        $query->query = self::encode($sql);
        $query->params = self::encode($params);
        
        $slaves = self::all_slave_query($query);
        foreach($slaves AS $slavename => $slave) {
            $slaves[$slavename] = self::decode($slave->result);
        }
        return $slaves;
    }
    
    public static function compare_permissions($slave, $permissions) {
        $query = new stdClass();
        $query->wsfunction = 'local_centraladmin_agent_compare_permissions';
        $query->permissions = base64_encode(gzcompress(serialize($permissions),3));
        
        $result = self::slave_query($slave,$query);
        
        return self::decode($result->result);
    }
    
    public static function get_slave($slaveid){
        return $GLOBALS['DB']->get_record_sql('SELECT * FROM {centraladmin_slave} WHERE disabled = 0 AND deleted = 0 AND id=?',array($slaveid));
    }
    
    public static function get_slave_by_name($slavename){
        return $GLOBALS['DB']->get_record('centraladmin_slave',array('name'=>$slavename));
    }
    
    
    private static function all_slave_query($query){
        $slaves = self::get_all_slaves();
        $res = array();
        foreach($slaves AS $slave){
            $res[$slave->name] = self::slave_query($slave,$query);
        }
        return $res;
    }
    
    private static function slave_query($slave, $query){
        $query->moodlewsrestformat = 'json';
        $query->wstoken = $slave->token;
        return self::api_query($slave, $query);
    }
    
    public static function get_all_slaves(){
        return $GLOBALS['DB']->get_records_select('centraladmin_slave', 'disabled = 0 AND deleted = 0');
    }
    
    private static function get_token_url($baseurl){
        return $baseurl.'/login/token.php';
    }
    
    private static function get_webservice_url($baseurl){
        return $baseurl.'/webservice/rest/server.php';
    }
    
    public static function encode($data){
        return base64_encode(serialize($data));
    }
    public static function decode($data){
        return unserialize(base64_decode($data));
    }
    
    private static function post($url, $data, $refUrl='')
    {
        global $CFG;
        $cprocess = curl_init($url);
        curl_setopt($cprocess, CURLOPT_POST, true);
        curl_setopt($cprocess, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cprocess, CURLOPT_REFERER, $refUrl);
        curl_setopt($cprocess, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($cprocess, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($cprocess, CURLOPT_POSTFIELDS, $data);
        
        if (!empty($CFG->proxyhost) && !is_proxybypass($url)) {
            if ($CFG->proxyport === '0') {
                curl_setopt($cprocess, CURLOPT_PROXY, $CFG->proxyhost);
            } else {
                curl_setopt($cprocess, CURLOPT_PROXY, $CFG->proxyhost.':'.$CFG->proxyport);
            }
        }
        
        $res = curl_exec($cprocess);
        if (!$res) {
            $returnArray = array(true, curl_errno($cprocess).' : '.curl_error($cprocess));
        } else {
            
            $returnArray = array(false, $res);
        }
        
        curl_close($cprocess);
        return $returnArray;
    }
}
