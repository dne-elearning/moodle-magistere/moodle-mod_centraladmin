<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'centraladminstats_offerfull', language 'en'
 *
 * @package   centraladminstats_offerfull
 * @copyright 2022 DNE - Ministere de l'Education Nationale
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginadministration'] = 'Administration centralisée';
$string['pluginname'] = 'Statistiques des formations en libre accès';
$string['offerview:view'] = 'Voir les statistiques des formations en libre accès';
$string['csv_courseachievement'] = 'Suivi d\'achèvement activé';
$string['csv_courseachievements'] = 'Nb participants ayant achevé le parcours';
$string['csv_createdate'] = 'Date de création';
$string['csv_domaines'] = 'Domaines professionnels';
$string['csv_duration'] = 'Durée de la formation';
$string['csv_durationconduct'] = 'Déroulé de la formation';
$string['csv_durationlocal'] = 'Temps en présentiel';
$string['csv_durationremote'] = 'Temps à distance';
$string['csv_startdate'] = 'Date d\'ouverture';
$string['csv_firstpublish'] = 'Date de première publication';
$string['csv_gaiausers'] = 'Modalités d\'accès à la formation';
$string['csv_keywords'] = 'Mots clés';
$string['csv_lastpublish'] = 'Date de dernière publication';
$string['csv_name'] = 'Nom';
$string['csv_nature'] = 'Nature';
$string['csv_nbactivities'] = 'Nb activités';
$string['csv_nbbadges'] = 'Badges paramétrés';
$string['csv_nbreceptiveactivities'] = 'Nb activités réceptives';
$string['csv_nbactiveactivities'] = 'Nb activités actives';
$string['csv_nbcreativeactivities'] = 'Nb activités créatives';
$string['csv_nbinteractiveactivities'] = 'Nb activités interactives';
$string['csv_nbdeliveredbadges'] = 'Badges délivrés';
$string['csv_nbbadgedparticipants'] = 'Nb participants badgés';
$string['csv_nbparticipants'] = 'Nb participants';
$string['csv_nbengagedparticipants'] = 'Nb participants actifs';
$string['csv_1D'] = '1D';   
$string['csv_2D'] = '2D';   
$string['csv_other'] = 'Autres';    
$string['csv_public'] = 'Public';  
$string['csv_private'] = 'Privé';   
$string['csv_withoutschool'] = 'Sans établissement';
$string['csv_origine'] = 'Plateforme d\'origines';
$string['csv_category'] = 'Catégorie';
$string['csv_publics'] = 'Public cible';
$string['csv_authors'] = 'Auteurs';
$string['csv_courseurl'] = 'Url de parcours';
$string['duration_remote'] = 'Distanciel';
$string['duration_hybrid'] = 'Hybride';
$string['exporttocsv'] = 'Exporter les données (csv)';
$string['form_btn_filter'] = 'Filtrer';
$string['form_btn_clear_filter'] = 'Effacer le filtre';
$string['form_btn_preview'] = 'Prévisualiser les données';
$string['form_btn_export'] = 'Exporter les données';
$string['form_date_placeholder'] = 'JJ/MM/AAAA';
$string['form_domain'] = 'Domaine professionnel';
$string['form_domain_placeholder'] = 'Domaine professionnel';
$string['form_durationconduct'] = 'Déroulé de la formation';
$string['form_durationconduct_placeholder'] = 'Déroulé de la formation';
$string['form_first_publication_date'] = 'Date de première publication';
$string['form_free_search'] = 'Recherche libre';
$string['form_from'] = 'De';
$string['form_gaiausers'] = 'Modalités d\'accès à la formation';
$string['form_gaiausers_placeholder'] = 'Modalités d\'accès à la formation';
$string['form_keywords'] = 'Mots clés';
$string['form_last_publication_date'] = 'Date de dernière publication';
$string['form_more_filter'] = 'Plus de filtres';
$string['form_nature'] = 'Nature';
$string['form_nature_placeholder'] = 'Nature';
$string['form_origin'] = 'Origine';
$string['form_origin_placeholder'] = 'Origine';
$string['form_public'] = 'Public cible';
$string['form_public_placeholder'] = 'Public';
$string['form_engaged_users'] = 'Nb participants actifs';
$string['form_to'] = 'À';
$string['gaia_prescription'] = 'Prescription';
$string['gaia_selftraining'] = 'Autoformation';
$string['no'] = 'non';
$string['preview_header_title'] = 'Prévisualisation';
$string['preview_header_name'] = 'Nom du parcours';
$string['preview_header_lastpublish'] = 'Date de publication';
$string['preview_header_keywords'] = 'Mots clés';
$string['title'] = 'Statistiques des formations en libre accès';
$string['totalpublished'] = 'Formations publiées au total';
$string['totaleditedseptember'] = 'Formations publiées modifiées depuis le 1er septembre';
$string['totalpublishedseptember'] = 'Formations publiées au 1er septembre';
$string['yes'] = 'oui';
$string['documentation'] = 'Documentation';