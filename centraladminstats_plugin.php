<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/centraladmin/centraladmin_plugin.php');
require_once($CFG->dirroot.'/mod/centraladmin/lib.php');

abstract class centraladminstats_plugin extends centraladmin_plugin {

     /**
     * Return subtype name of the plugin.
     *
     * @return string
     */
    public function get_subtype() {
        return 'centraladminstatistics';
    }
    
    public abstract function get_content();

    protected function check_permission() {
        $class = get_class($this);
        if (!centraladmin_permission_can_access_plugin($class)){
            print_error('no_permission_for_this_plugin', 'centraladmin', '', $class);
        }
    }

}