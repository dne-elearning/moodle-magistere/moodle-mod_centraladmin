/* jshint ignore:start */
define(['jquery', 'core/ajax', 'jqueryui', 'centraladminconf_role/jquery.loadingModal'], function($, ajax) {
    function init() {
        
    	$("body").loadingModal({
            position: "auto",
            text: "Mise à jour des permissions en cours.<br>Veuillez patientez quelques secondes.",
            color: "#fff",
            opacity: "0.7",
            backgroundColor: "rgb(0,0,0)",
            animation: "circle"
        });
        setTimeout(checkupdate,1000);
    }
    
    function checkupdate(){
		var promise = ajax.call([
            { methodname: 'centraladminconf_role_updatecache' , args: {}},
        ]);
        
        promise[0].done((response) => {
			if (response.status == 'running') {
            	setTimeout(checkupdate,1000);
            }else{
				location.reload();
			}
        });
	}

    return {
        init: function() {
            init();
        }
    };

});