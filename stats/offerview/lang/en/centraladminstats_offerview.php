<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'centraladmin', language 'en'
 *
 * @package   centraladminstats_offerview
 * @copyright 2022 DNE - Ministere de l'Education Nationale
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginadministration'] = 'Administration centralisée';
$string['pluginname'] = 'Vue globale des formations en libre accès';
$string['totalpublished'] = 'Formations publiées au total';
$string['totaleditedseptember'] = 'Formations publiées, modifiées durant l\'année scolaire en cours (à partir du 1er septembre)';
$string['totalpublishedseptember'] = 'Nouvelles formations publiées durant l\'année en cours (à partir du 1er septembre)';
$string['exporttocsv'] = 'Exporter les données (csv)';
$string['csv_name'] = 'Nom';
$string['csv_createdate'] = 'Date de création';
$string['csv_startdate'] = 'Date d\'ouverture';
$string['csv_lastpublish'] = 'Date de dernière publication';
$string['csv_firstpublish'] = 'Date de première publication';
$string['csv_duration'] = 'Durée de la formation';
$string['csv_durationconduct'] = 'Déroulé de la formation';
$string['csv_durationlocal'] = 'Temps en présentiel';
$string['csv_durationremote'] = 'Temps à distance';
$string['csv_gaiausers'] = 'Modalités d\'accès à la formation';
$string['csv_nbparticipants'] = 'Nb participants';
$string['csv_nbengagedparticipants'] = 'Nb participants actifs';
$string['csv_1D'] = '1D';   
$string['csv_2D'] = '2D';   
$string['csv_other'] = 'Autres';    
$string['csv_public'] = 'Public';  
$string['csv_private'] = 'Privé';   
$string['csv_withoutschool'] = 'Sans établissement';
$string['csv_domaines'] = 'Domaines professionnels';
$string['csv_origine'] = 'Plateforme d\'origine';
$string['csv_category'] = 'Catégorie';
$string['csv_nature'] = 'Nature';
$string['csv_publics'] = 'Public cible';
$string['csv_keywords'] = 'Mots clés';
$string['csv_courseachievement'] = 'Suivi d\'achèvement activé';
$string['csv_courseachievements'] = 'Nb participants ayant achevé le parcours';
$string['csv_nbactivities'] = 'Nb activités';
$string['csv_nbreceptiveactivities'] = 'Nb activités réceptives';
$string['csv_nbactiveactivities'] = 'Nb activités actives';
$string['csv_nbcreativeactivities'] = 'Nb activités créatives';
$string['csv_nbinteractiveactivities'] = 'Nb activités interactives';
$string['csv_nbbadges'] = 'Badges paramétrés';
$string['csv_nbdeliveredbadges'] = 'Badges délivrés';
$string['csv_nbbadgedparticipants'] = 'Nb participants badgés';
$string['csv_authors'] = 'Auteurs';
$string['csv_courseurl'] = 'Url de parcours';
$string['duration_remote'] = 'Distanciel';
$string['duration_hybrid'] = 'Hybride';
$string['gaia_prescription'] = 'Prescription';
$string['gaia_selftraining'] = 'Autoformation';
$string['no'] = 'non';
$string['offerview:view'] = 'Voir la vue globale de l\'offre de formation';
$string['offerview:export'] = 'Exporter les données de l\'offre de formation';
$string['yes'] = 'oui';
$string['documentation'] = 'Documentation';