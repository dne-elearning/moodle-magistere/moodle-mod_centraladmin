<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * centraladminconf_role external functions and service definitions.
 *
 * @package    centraladminconf_role
 * @copyright  2022 Reseau Canope
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$services = array(
    'centraladminconf_role_service' => array(
        'functions' => array(
            'centraladminconf_role_get_capabilities',
            'centraladminconf_role_getexport',
            'centraladminconf_role_updating',
            'centraladminconf_role_get_recap',
            'centraladminconf_role_reset_todo',
            'centraladminconf_role_confirm',
            'centraladminconf_role_updatecache'
        ),
        'requiredcapability' => '',
        'restrictedusers' => 0,
        'enabled' => 1,
        'shortname' =>  'centraladminconf_role_service',
        'downloadfiles' => 0,
        'uploadfiles'  => 0,
    ),
);

$functions = array(
    'centraladminconf_role_get_capabilities' => array(
        'classname' => 'centraladminconf_role_external',
        'methodname' => 'get_capabilities',
        'classpath' => 'mod/centraladmin/conf/role/externallib.php',
        'description' => 'Get capabilities for a role',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'centraladminconf_role_getexport' => array(
        'classname' => 'centraladminconf_role_external',
        'methodname' => 'get_export',
        'classpath' => 'mod/centraladmin/conf/role/externallib.php',
        'description' => 'Get data for export',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ), //Get roles being updated
    'centraladminconf_role_updating' => array(
        'classname' => 'centraladminconf_role_external',
        'methodname' => 'role_updating',
        'classpath' => 'mod/centraladmin/conf/role/externallib.php',
        'description' => 'Get role updating',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'centraladminconf_role_get_recap' => array(
        'classname' => 'centraladminconf_role_external',
        'methodname' => 'get_recap',
        'classpath' => 'mod/centraladmin/conf/role/externallib.php',
        'description' => 'Get recap for all role',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'centraladminconf_role_reset_todo' => array(
        'classname' => 'centraladminconf_role_external',
        'methodname' => 'reset_todo',
        'classpath' => 'mod/centraladmin/conf/role/externallib.php',
        'description' => 'Reset todos for a specific user',
        'type' => 'write',
        'capabilities' => '',
        'ajax' => true,
    ),
    'centraladminconf_role_confirm' => array(
        'classname' => 'centraladminconf_role_external',
        'methodname' => 'confirm',
        'classpath' => 'mod/centraladmin/conf/role/externallib.php',
        'description' => 'Confirm the todos of a specified user and propogate the new permisisons to the instances',
        'type' => 'write',
        'capabilities' => '',
        'ajax' => true,
    ),
    'centraladminconf_role_updatecache' => array(
        'classname' => 'centraladminconf_role_external',
        'methodname' => 'updatecache',
        'classpath' => 'mod/centraladmin/conf/role/externallib.php',
        'description' => 'Update cache',
        'type' => 'write',
        'capabilities' => '',
        'ajax' => true,
    ),
);
