<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * centraladminstats_offerview external functions and service definitions.
 *
 * @package    centraladminstats_offerview
 * @copyright  2021 TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */



$services = array(
    'centraladminstats_offerview_service' => array(
        'functions' => array(
            'centraladminstats_offerview_domains', 
            'centraladminstats_offerview_origins', 
            'centraladminstats_offerview_evolution',
            'centraladminstats_offerview_achievement'
        ),
        'requiredcapability' => '',
        'restrictedusers' => 0,
        'enabled' => 1,
        'shortname' =>  'centraladminstats_offerview_service',
        'downloadfiles' => 0,
        'uploadfiles'  => 0,
    )
);

$functions = array(
    'centraladminstats_offerview_domains' => array(
        'classname' => 'centraladminstats_offerview_external',
        'methodname' => 'get_courses_by_domain',
        'classpath' => 'mod/centraladmin/stats/offerview/externallib.php',
        'description' => 'Get data for course by domain pie chart',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'centraladminstats_offerview_origins' => array(
        'classname' => 'centraladminstats_offerview_external',
        'methodname' => 'get_courses_by_origin',
        'classpath' => 'mod/centraladmin/stats/offerview/externallib.php',
        'description' => 'Get data for course by origin bar chart',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'centraladminstats_offerview_evolution' => array(
        'classname' => 'centraladminstats_offerview_external',
        'methodname' => 'get_offer_evolution',
        'classpath' => 'mod/centraladmin/stats/offerview/externallib.php',
        'description' => 'Get data for the offer evolution chart',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'centraladminstats_offerview_achievement' => array(
        'classname' => 'centraladminstats_offerview_external',
        'methodname' => 'get_offer_achievement',
        'classpath' => 'mod/centraladmin/stats/offerview/externallib.php',
        'description' => 'Get data for the offer achievement chart',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    )
);
