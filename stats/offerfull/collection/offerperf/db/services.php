<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * centraladminstats_offerfull external functions and service definitions.
 *
 * @package    centraladmincollec_offerperf
 * @copyright  2022 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */



$services = array(
    'centraladmincollec_offerperf_service' => array(
        'functions' => array(
            'centraladmincollec_offerperf_top_courses'
        ),
        'requiredcapability' => '',
        'restrictedusers' => 0,
        'enabled' => 1,
        'shortname' =>  'centraladmincollec_offerperf_service',
        'downloadfiles' => 0,
        'uploadfiles'  => 0,
    )
);

$functions = array(
    'centraladmincollec_offerperf_top_courses' => array(
        'classname' => 'centraladmincollec_attendance_external',
        'methodname' => 'get_top_courses_data',
        'classpath' => 'mod/centraladmin/stats/offerfull/collection/offerperf/externallib.php',
        'description' => 'Get data for the top_courses graph',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    )
);
