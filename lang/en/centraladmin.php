<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'centraladmin', language 'en'
 *
 * @package   mod_centraladmin
 * @copyright 2021 TCS
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$string['pluginadministration'] = 'Administration centralisée';
$string['pluginname'] = 'Administration centralisée';
$string['modulename'] = 'Administration centralisée';
$string['modulenameplural'] = 'Administration centralisée';
$string['slaveurl'] = 'Slave URL';
$string['username'] = 'username';
$string['password'] = 'password';
$string['addslave'] = 'add a Slave';
$string['pagedesc'] = 'Manage slaves';
$string['confirmslave'] = 'Validate slave';
$string['name'] = 'Nom';
$string['url'] = 'Url';
$string['lastquery'] = 'Dernière requête';
$string['addslave_success'] = 'Slave added';
$string['remove'] = 'Supprimer';
$string['removeslaveconfirm'] = 'Voulez-vous vous vraiment supprimer le Slave {$a} ?';
$string['removeslave_success'] = 'Le Slave a été supprimé avec succès';
$string['centraladmin:manageslave'] = 'Gérer les slaves';
$string['errorlogin'] = 'Le login ou le mot de passe est incorrect';
$string['errorurl'] = 'La connexion n\'a pas pu être établie, vérifiez l\'URL de la plateforme';
$string['ping'] = 'Ping';
$string['ping_success'] = 'Ping envoyé avec succès sur {$a}';
$string['manageslaves'] = 'Manage slaves';
$string['centraladmin:view'] = '';
$string['centraladmin:webservice'] = 'Peut utiliser le webservice';
$string['centraladmin:wf_ping'] = 'Peut utiliser la fonction ping du webservice';
$string['centraladmin:addinstance'] = 'Peut ajouter une instance de centraladmin sur un parcours';

$string['addpermission_success'] = 'Les droits des utilisateurs sélectionnés ont été ajoutés avec succès';
$string['permission_pagedesc'] = 'Gestion des droits utilisateur';
$string['unselectall'] = 'Tout désélectionner';
$string['selectall'] = 'Tout sélectionner';
$string['search'] = 'Rechercher';
$string['managepermission'] = 'Gérer les droits utilisateur';
$string['available_views'] = 'Vues disponibles';
$string['available_slaves'] = 'Instances disponibles';
$string['submit'] = 'Valider';
$string['reset'] = 'Reset';
$string['errordelete'] = 'Une erreur est intervenue lors de la suppression des anciens droits';
$string['centraladmin:managepermission'] = 'Gérer les droits utilisateur';


$string['update_permission'] = 'Update users permissions';
$string['no_permission_for_this_plugin'] = 'You don\'t have the permission to access this view';
$string['no_collection_available'] = 'You don\'t have the permission to access this view (collec)';

$string['ws_usagemonitor_username'] = 'Username for usagemonitor service';
$string['ws_usagemonitor_username_desc'] = 'The username used for the usagemonitor service';
$string['ws_usagemonitor_password'] = 'Password for usagemonitor service';
$string['ws_usagemonitor_password_desc'] = 'Password used for the usagemonitor service';
$string['settings'] = 'Settings';