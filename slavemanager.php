<?php

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once($CFG->dirroot.'/mod/centraladmin/slavemanager_form.php');
require_once($CFG->dirroot.'/mod/centraladmin/SlaveQuery.php');
require_once($CFG->libdir.'/adminlib.php');

$action = optional_param('action', '', PARAM_ALPHANUMEXT);
$slaveid = optional_param('id', '', PARAM_INT);
$confirm = optional_param('confirm', 0, PARAM_BOOL);

$context = context_system::instance();


require_capability('mod/centraladmin:manageslave', $context);


$PAGE->set_cacheable(false);

$PAGE->set_context($context);
$PAGE->set_url('/mod/centraladmin/slavemanager.php');
$PAGE->set_pagetype('site-index');
$PAGE->set_pagelayout('standard');

$PAGE->set_title("Slave Manager");
$PAGE->set_heading("Slave Manager");

admin_externalpage_setup('mod_centraladmin_slave_manager');

$renderer = $PAGE->get_renderer('mod_centraladmin');

$form = new slavemanager_form();

if ($action === 'delete' && $slaveid) {

    $slave = SlaveQuery::get_slave($slaveid);

    if ($confirm and confirm_sesskey()) {
        try {
            SlaveQuery::unpair($slaveid);
            redirect($PAGE->url, get_string('removeslave_success','mod_centraladmin'), null, \core\output\notification::NOTIFY_SUCCESS);
        } catch(Exception $e) {
            if (is_siteadmin()) {
                $error = $e->getMessage();
            } else {
                $error = get_string('error', 'error');
            }
            redirect($PAGE->url, $error, null, \core\output\notification::NOTIFY_ERROR);
        }
    }

    echo $OUTPUT->header();
    echo $renderer->delete_slave_confirmation($slave);
    echo $OUTPUT->footer();
    die();
} 

if ($action === 'ping' && $slaveid) {
    $slave = SlaveQuery::get_slave($slaveid);
    try {
        SlaveQuery::ping($slave);
        redirect($PAGE->url, get_string('ping_success','mod_centraladmin', $slave->url), null, \core\output\notification::NOTIFY_SUCCESS);
    } catch(Exception $e) {
        if (is_siteadmin()) {
            $error = $e->getMessage();
        } else {
            $error = get_string('error', 'error');
        }
        \core\notification::error($error);
    }
}


if ($data = $form->get_data()) {
    try {
        SlaveQuery::pair($data->slave_url, $data->username, $data->password);
        redirect($PAGE->url, get_string('addslave_success','mod_centraladmin'), null, \core\output\notification::NOTIFY_SUCCESS);
    } catch(Exception $e ) {
        if (is_siteadmin()) {
            $error = $e->getMessage();
        } else {
            if ($e->getCode() === SlaveQuery::ERROR_LOGIN) {
                $error = get_string('errorlogin', 'mod_centraladmin');    
            } elseif ($e->getCode() === SlaveQuery::ERROR_URL) {
                $error = get_string('errorurl', 'mod_centraladmin');    
            } else {
                $error = get_string('error', 'error');
            }
        }
        \core\notification::error($error);
        
    }
}


$pagedesc = get_string("pagedesc", 'mod_centraladmin');

echo $OUTPUT->header();
echo $OUTPUT->heading($pagedesc);
echo $renderer->slavemanager_slaves_list();
$form->display();

echo $OUTPUT->footer();
