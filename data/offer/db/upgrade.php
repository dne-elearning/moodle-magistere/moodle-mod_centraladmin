<?php
/**
 * This file keeps track of upgrades to
 * the centraladmin module
 *
 * Sometimes, changes between versions involve
 * alterations to database structures and other
 * major things that may break installations.
 *
 * The upgrade function in this file will attempt
 * to perform all the necessary actions to upgrade
 * your older installation to the current version.
 *
 * If there's something it cannot do itself, it
 * will tell you what you need to do.
 *
 * The commands in here will all be database-neutral,
 * using the methods of database_manager class
 *
 * Please do not forget to use upgrade_set_timeout()
 * before any action that may take longer time to finish.
 *
 * @package   centraladmindata_offer
 * @copyright 2022 DNE - Ministere de l'Education Nationale
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function xmldb_centraladmindata_offer_upgrade($oldversion) {

    global $DB;

    $dbman = $DB->get_manager();

    if($oldversion < 2021072000) {
        // Define table local_coursehub_course to be created
        $table_oldname = 'centraladmindata_achievement';
        $table_newname = 'centraladmindata_offercmpl';

        $oldtable = new xmldb_table($table_oldname);
        $newtable = new xmldb_table($table_newname);

        if (!$dbman->table_exists($newtable) && $dbman->table_exists($oldtable)) {
            $dbman->rename_table($oldtable, $table_newname);
        }
        
        upgrade_plugin_savepoint(true, 2021072000, 'centraladmindata', 'offer');
    }

    if($oldversion < 2021072101) {

        $table_name = 'centraladmindata_offer';
        $table = new xmldb_table($table_name);

        $field = new xmldb_field('nbparticipants',XMLDB_TYPE_INTEGER,10,null,true,false, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('nbparticipantscurrentyear',XMLDB_TYPE_INTEGER,10,null,true,false, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('nbparticipantslastyear',XMLDB_TYPE_INTEGER,10,null,true,false, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        upgrade_plugin_savepoint(true, 2021072101, 'centraladmindata', 'offer');
    }

    if($oldversion < 2021072102) {

        $table_name = 'centraladmindata_attendance';
        $table = new xmldb_table($table_name);

        if (!$dbman->table_exists($table)) {
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE);
            $table->add_field('slaveid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null);
            $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null);

            $table->add_field('nbparticipants1d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbparticipantscurrentyear1d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbparticipantslastyear1d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);

            $table->add_field('nbstudents1d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbstudentscurrentyear1d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbstudentslastyear1d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);

            $table->add_field('nbparticipants2d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbparticipantscurrentyear2d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbparticipantslastyear2d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);

            $table->add_field('nbstudents2d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbstudentscurrentyear2d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbstudentslastyear2d', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);

            $table->add_field('nbparticipantsndef', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbparticipantscurrentyearndef', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbparticipantslastyearndef', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);

            $table->add_field('nbstudentsndef', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbstudentscurrentyearndef', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);
            $table->add_field('nbstudentslastyearndef', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, null, null, 0);

            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'), null, null);
            $table->add_index('slaveid', XMLDB_INDEX_NOTUNIQUE, array('slaveid'));

            $dbman->create_table($table);
        }

        upgrade_plugin_savepoint(true, 2021072102, 'centraladmindata', 'offer');
    }

    if($oldversion < 2021072103) {

        $table_name = 'centraladmindata_offer';
        $table = new xmldb_table($table_name);

        $field = new xmldb_field('description',XMLDB_TYPE_CHAR,1333,null,XMLDB_NOTNULL,false, ' ');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('objectif',XMLDB_TYPE_CHAR,1333,null,XMLDB_NOTNULL,false, ' ');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        upgrade_plugin_savepoint(true, 2021072103, 'centraladmindata', 'offer');
    }

    if($oldversion < 2021091400) {

        $table_name = 'centraladmindata_offer';
        $table = new xmldb_table($table_name);

        $field = new xmldb_field('gaiausers',XMLDB_TYPE_INTEGER,10,null,true,false, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('durationlocal',XMLDB_TYPE_INTEGER,10,null,true,false, 0, 'duration');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('durationremote',XMLDB_TYPE_INTEGER,10,null,true,false, 0, 'durationlocal');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('courseachivement',XMLDB_TYPE_INTEGER,1,null,true,false, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->rename_field($table, $field, 'courseachievement');
        }

        $field = new xmldb_field('courseachivements',XMLDB_TYPE_INTEGER,10,null,true,false, 0);
        if ($dbman->field_exists($table, $field)) {
            $dbman->rename_field($table, $field, 'courseachievements');
        }

        upgrade_plugin_savepoint(true, 2021091400, 'centraladmindata', 'offer');
    }

    if ($oldversion < 2022071200) {

        // Rename field nbengagedparticipants on table centraladmindata_offer to NEWNAMEGOESHERE.
        $table = new xmldb_table('centraladmindata_offer');
        $field = new xmldb_field('nbstudents', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null, 'durationremote');

        // Launch rename field nbengagedparticipants.
        $dbman->rename_field($table, $field, 'nbengagedparticipants');

        $field = new xmldb_field('authors', XMLDB_TYPE_CHAR, '1333', null, XMLDB_NOTNULL, null, null, 'gaiausers');
        // Conditionally launch add field authors.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('courseurl', XMLDB_TYPE_CHAR, '1333', null, XMLDB_NOTNULL, null, null, 'authors');
        // Conditionally launch add field courseurl.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Offer savepoint reached.
        upgrade_plugin_savepoint(true, 2022071200, 'centraladmindata', 'offer');
    }

    if ($oldversion < 2022072500) {

        // Define field startdate to be added to encentraladmindata_offer.
        $table = new xmldb_table('centraladmindata_offer');
        $field = new xmldb_field('startdate', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'timecreated');

        // Conditionally launch add field startdate .
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field nbbadgedparticipants to be added to centraladmindata_offer.
        $field = new xmldb_field('nbbadgedparticipants', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0, 'nbdeliveredbadges');

        // Conditionally launch add field nbbadgedparticipants.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field category to be added to centraladmindata_offer.
        $field = new xmldb_field('category', XMLDB_TYPE_CHAR, '1333', null, null, null, null, 'courseurl');

        // Conditionally launch add field category.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }


        $table = new xmldb_table('centraladmindata_attendance');

        // Define field nbparticipantspublic to be added to centraladmindata_attendance.
        $field = new xmldb_field('nbparticipantspublic', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0, 'nbstudentslastyearndef');

        // Conditionally launch add field nbparticipantspublic.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field nbparticipantsprive to be added to centraladmindata_attendance.
        $field = new xmldb_field('nbparticipantsprive', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0, 'nbparticipantspublic');

        // Conditionally launch add field nbparticipantsprive.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Define field nbparticipantsnoschool to be added to centraladmindata_attendance.
        $field = new xmldb_field('nbparticipantsnoschool', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0, 'nbparticipantsprive');

        // Conditionally launch add field nbparticipantsnoschool.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Offer savepoint reached.
        upgrade_plugin_savepoint(true, 2022072500, 'centraladmindata', 'offer');
    }
    
    if ($oldversion < 2022113000) {
        
        $table = new xmldb_table('centraladmindata_offer');
        $field = new xmldb_field('nbcollaborativeactivities');
        if ($dbman->field_exists($table, $field)) {
            $field->set_attributes(XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0, 'nbactivities');
            $dbman->rename_field($table, $field, 'nbcreativeactivities');
        }
        
        $field = new xmldb_field('nbactiveactivities', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0, 'nbactivities');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        $field = new xmldb_field('nbreceptiveactivities', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0, 'nbactivities');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        upgrade_plugin_savepoint(true, 2022113000, 'centraladmindata', 'offer');
    }

    return true;
}
