<?php

class mod_centraladmin_renderer extends plugin_renderer_base {
    public function slavemanager_slaves_list() {
        global $CFG, $DB;
        
        $slaves = $DB->get_records('centraladmin_slave', array('deleted'=>false));
        if (!empty($slaves)) {
            $table = new html_table();
            $table->head = array(get_string('name', 'mod_centraladmin'),
                get_string('url','mod_centraladmin'), get_string('lastquery', 'mod_centraladmin'), '', '');
            $table->align = array('left', 'left', 'left', 'center', 'center');
            $table->size = array('15%', '50%', '15%', '10%', '10%');
            $table->align[] = 'left';
            
            foreach($slaves as $slave) {
                $lastquery = date('d/m/Y H:i:s', $slave->lastquery);
                $removeUrl = new moodle_url($CFG->wwwroot.'/mod/centraladmin/slavemanager.php', array('id' => $slave->id, 'action' => 'delete'));
                $removelink = html_writer::tag('a',
                    get_string('remove', 'mod_centraladmin'),
                    array('href' => $removeUrl));
                $pingUrl = new moodle_url($CFG->wwwroot.'/mod/centraladmin/slavemanager.php', array('id' => $slave->id, 'action' => 'ping'));
                $pingLink = html_writer::tag('a',
                    get_string('ping', 'mod_centraladmin'),
                    array('href' => $pingUrl));
                $table->data[] = array($slave->name, $slave->url, $lastquery, $removelink, $pingLink);
            }
            $html = html_writer::table($table);
            return $html;
        }
        return '';
    }
    
    
    public function delete_slave_confirmation($slave) {
        global $CFG;
        $optionsyes = array('id' => $slave->id, 'action' => 'delete',
            'confirm' => 1, 'sesskey' => sesskey());
        $optionsno = array('sesskey' => sesskey());
        $formcontinue = new single_button(
            new moodle_url($CFG->wwwroot.'/mod/centraladmin/slavemanager.php', $optionsyes),
            get_string('delete'));
        $formcancel = new single_button(
            new moodle_url($CFG->wwwroot.'/mod/centraladmin/slavemanager.php', $optionsno),
            get_string('cancel'), 'get');
        
        return $this->output->confirm(get_string('removeslaveconfirm', 'mod_centraladmin', $slave->name),
            $formcontinue, $formcancel);
    }
    
    
    public function permissionmanager(array $statsusers, array $slaves, array $views, array $labelviews) {
        global $PAGE, $OUTPUT;
        
        $PAGE->requires->js_call_amd('mod_centraladmin/permission', 'init', array(
            get_string('unselectall', 'mod_centraladmin'), 
            get_string('selectall', 'mod_centraladmin'), 
            get_string('search', 'mod_centraladmin')
        ));
        
        $statsusers_options = array_map(function($u){
            return '<option value="'.$u->id.'">'.ucfirst($u->firstname) . ' ' . ucfirst($u->lastname).'</option>';
        }, $statsusers);
            
        $renderviews = [];
        foreach($views as $pluginstats => $collection) {
            $renderviews[] = array(
                'name' => $pluginstats,
                'value' => isset($labelviews[$pluginstats])?$labelviews[$pluginstats]:$pluginstats,
                'child' => 0
            );
            foreach($collection as $subplugin) {
                $renderviews[] = array(
                    'name' => $subplugin['type'].'_'. $subplugin['id'],
                    'value' => $subplugin['name'],
                    'child' => 1
                );
            }
        }
        
        $permissiondata = array(
            'statsusers_options' => implode('', $statsusers_options),
            'views' => $renderviews,
            'slaves' => array_map(function($s){
                return array(
                    'name' => $s
                );
            }, $slaves),
            'str_selectall' => get_string('selectall', 'mod_centraladmin'),
            'str_availableviews' => get_string('available_views', 'mod_centraladmin'),
            'str_availableslaves' => get_string('available_slaves', 'mod_centraladmin'),
            'str_submit' => get_string('submit', 'mod_centraladmin'),
            'str_reset' => get_string('reset', 'mod_centraladmin'),
        );
        return $OUTPUT->render_from_template('mod_centraladmin/permission', $permissiondata);
    }
}