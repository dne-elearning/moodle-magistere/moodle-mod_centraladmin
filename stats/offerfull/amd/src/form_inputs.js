/* jshint ignore:start */
define(['jquery', 'centraladminstats_offerfull/jquery-dropdown'], function($) {
    function init() {
        multiSelectDropdown('#d_origins');
        multiSelectDropdown('#d_publics');
        multiSelectDropdown('#d_domains');
        multiSelectDropdown('#d_durationconduct');
        multiSelectDropdown('#d_gaiausers');

        var options = {
            "monthNames": ['janvier', 'f&eacute;vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao&ucirc;t', 'septembre', 'octobre', 'novembre', 'd&eacute;cembre'] ,
            "monthNamesShort": ['janv.', 'f&eacute;vr.', 'mars', 'avril', 'mai', 'juin', 'juil.', 'ao&ucirc;t', 'sept.', 'oct.', 'nov.', 'd&eacute;c.'] ,
            "dayNames": ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'] ,
            "dayNamesShort": ['dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'] ,
            "dayNamesMin": ['D','L','M','M','J','V','S'] ,
            "dateFormat": 'dd/mm/yy',
            "weekHeader": 'Sem.',
            "firstDay": 1,
            "currentText": 'Aujourd\'hui',
            "closeText": 'Fermer',
            "prevText": 'Pr&eacute;c&eacute;dent',
            "nextText": 'Suivant',
            "isRTL": false,
            "showMonthAfterYear": false,
            "showOn": 'button',
            "buttonText": "<i class='fa fa-calendar'></i>"
        };

        $('.datepicker').datepicker(options).next('button').button({
        	text: false
        });

        $("#f_engagedusers").slider({
            range: true,
            min: parseInt($("#f_engagedusers_min").text()),
            max: parseInt($("#f_engagedusers_max").text()),
            values: [parseInt($("#f_engagedusers_min").text()), parseInt($("#f_engagedusers_max").text())],
            slide: function( event, ui ) {
                $("#f_engagedusers_min").text(ui.values[0]);
                $("#f_engagedusers_max").text(ui.values[1]);
            }
        });

		function multiSelectDropdown(selector) {
			$(selector).dropdown({
				// read only
				readOnly: false,
				// min count
				minCount: 0,
				// error message
				minCountErrorMessage: '',
				// the maximum number of options allowed to be selected
				limitCount: Infinity,
				// error message
				limitCountErrorMessage: '',
				// search field
				input: '<input type="text" maxLength="20" placeholder="Rechercher">',
				// dynamic data here
				data: [],
				// is search able?
				searchable: true,
				// when there's no result
				searchNoData: '<li style="color:#ddd">Pas de résultat</li>',
				// callback
				choice: function () {},
				multipleMode: 'label',
				// custom props
				extendProps: []
			});
		}
	}
	
	return {
    	init : function() {
            init();
        }
    };
});