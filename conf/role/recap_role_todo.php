<?php

defined('MOODLE_INTERNAL') || die();

require_once(dirname(dirname(dirname(dirname(__DIR__)))).'/config.php');
require_once($CFG->dirroot.'/mod/centraladmin/conf/role/role.php');
require_once($CFG->dirroot.'/mod/centraladmin/conf/role/lib.php');
require_once($CFG->dirroot.'/mod/centraladmin/conf/role/recap_role_todo.php');

class recap_role_todo {

    public function __construct() {
        $this->result = new stdClass();
        $this->result->roles = array();
        $this->result->instance_name_list = array();
        $this->result->capabilities_list = array();
        $this->result->instances_sum = 0;
        $this->result->capabilities_sum = 0;
        $this->result->conflicts_sum = 0;

        foreach ($this->get_todo_role_data() as $row) {
            if (!in_array($row->instance_name, $this->result->instance_name_list)) {
                $this->result->instance_name_list[] = $row->instance_name;
            }

            if (!in_array($row->capability, $this->result->capabilities_list)) {
                $this->result->capabilities_list[] = $row->capability;
            }

            if (!$this->role_exist($row->roleshortname)) {
                $this->create_new_role($row->roleshortname, $row->rolename);
            }
            $role = $this->result->roles[$row->roleshortname];

            if (!$this->instance_exist($role->conflicts, $row->slaveid)) {
                $this->create_new_instance($role->conflicts, $row->slaveid, $row->instance_name);
            }

            if (!$this->instance_exist($role->changerequests, $row->slaveid)) {
                $this->create_new_instance($role->changerequests, $row->slaveid, $row->instance_name);
            }

            if (isset($row->conflict_permission)) {
                $this->create_new_capability_conflict($row->capability, $role, $row->slaveid, $row->permissionold, $row->permission, $row->conflict_permission);
            } else {
                $this->create_new_capability_request($row->capability, $role, $row->slaveid, $row->permissionold, $row->permission);
            }
        }

        $this->result->instances_sum = count($this->result->instance_name_list);
        $this->result->capabilities_sum = count($this->result->capabilities_list);

        $this->result = $this->prepare_data_for_template($this->result);
    }

    public function role_exist($roleshortname) {
        return isset($this->result->roles[$roleshortname]);
    }

    public function instance_exist($conflicts_or_change, $slaveid) {
        return isset($conflicts_or_change->instances[$slaveid]);
    }

    public function create_new_role($roleshortname, $rolename) {
        $new_role = new StdClass();
        $new_role->rolename = $rolename;
        $new_role->roleshortname = $roleshortname;
        $new_role->conflicts = new StdClass();
        $new_role->changerequests = new StdClass();

        $new_role->conflicts->instances = array();
        $new_role->changerequests->instances = array();
        $new_role->isactive = false;
        if(count($this->result->roles) == 0){
            $new_role->isactive = true;
        }

        $this->result->roles[$roleshortname] = $new_role;
    }

    public function create_new_instance($conflicts_or_change, $slaveid, $instance_name) {
        $new_instance = new StdClass();
        $new_instance->instance_name = $instance_name;
        $new_instance->clean_instance_name = $this->prepare_instance_name($instance_name);
        $new_instance->slaveid = $slaveid;
        $new_instance->capabilities = array();

        $conflicts_or_change->instances[$slaveid] = $new_instance;
    }

    public function create_new_capability_conflict($capa, $role, $slaveid, $oldpermission, $permission, $conflict_permission) {
        $capa_names_array = centraladminconf_role::getCapaNamesArray();

        $new_capability = new stdClass();
        $new_capability->capability = $capa;
        $new_capability->permission = $permission;
        $new_capability->oldpermission = $oldpermission;
        $new_capability->conflict_permission = $conflict_permission;
        $new_capability->capabilityfortemplate = $capa . " : " . $capa_names_array[$oldpermission] . " (" . $capa_names_array[$conflict_permission]. ") -> " . $capa_names_array[$permission];

        $role->conflicts->instances[$slaveid]->capabilities[$capa] = $new_capability;
        $this->result->conflicts_sum ++;
    }

    public function create_new_capability_request($capa, $role, $slaveid, $oldpermission, $permission) {
        $capa_names_array = centraladminconf_role::getCapaNamesArray();

        $new_capability = new stdClass();
        $new_capability->capability = $capa;
        $new_capability->permission = $permission;
        $new_capability->oldpermission = $oldpermission;
        $new_capability->capabilityfortemplate = $capa . " : " . $capa_names_array[$oldpermission] . " -> " . $capa_names_array[$permission];

        $role->changerequests->instances[$slaveid]->capabilities[$capa] = $new_capability;
    }

    private function get_todo_role_data() {
        global $DB, $USER;

        $sql = 'SELECT crt.*, cs.name as instance_name, crc.roleshortname, crc.rolename, crtconflict.permission as conflict_permission  
FROM {'.centraladminconf_role::TABLE_ROLE_TODO.'} crt
INNER JOIN {'.centraladminconf_role::TABLE_ROLE_CACHE.'} crc ON (crt.roleid = crc.roleid AND crt.slaveid = crc.slaveid AND crt.capability = crc.capability)
INNER JOIN {'.centraladminconf_role::TABLE_SLAVE.'} cs ON (crt.slaveid = cs.id)
LEFT JOIN {'.centraladminconf_role::TABLE_ROLE_TODO.'} crtconflict ON (crt.roleid = crtconflict.roleid AND crt.slaveid = crtconflict.slaveid AND crt.capability = crtconflict.capability AND crt.permission != crtconflict.permission AND crt.userid != crtconflict.userid)
WHERE crt.userid = :userid
GROUP BY crc.roleshortname, cs.name, crt.capability
HAVING MAX(crt.timecreated)
ORDER BY crc.roleshortname, cs.name, crt.capability';

        return $DB->get_records_sql($sql, array('userid' => $USER->id));
    }

    private function prepare_data_for_template($raw_result) {
        $result = [];
        foreach ($raw_result as $keyresult => $valueresult) {
            if($keyresult == "roles"){
                $roles = [];
                foreach ($raw_result->roles as $raw_role) {
                    $role=[];
                    foreach ($raw_role as $keyrole => $valuerole){
                        if($keyrole == "conflicts" || $keyrole == "changerequests"){
                            $i = 0;
                            foreach ($valuerole->instances as $keyinstance => $instance) {
                                if (count($instance->capabilities) == 0) {
                                    continue;
                                }
                                $inst = new stdClass();
                                foreach ($instance as $key => $value) {
                                    if ($key == "capabilities") {
                                        foreach ($instance->capabilities as $capability){
                                            $inst->capabilities[] = $capability;
                                        }
                                    } else {
                                        $inst->$key = $value;
                                    }
                                }
                                $role[$keyrole]->instances[$i] = $inst;
                                $i++;
                            }
                        } else {
                            $role[$keyrole] = $valuerole;
                        }
                    }
                    $roles[] = $role;
                }
                $result[$keyresult] = $roles;
            } else {
                $result[$keyresult] = $valueresult;
            }

        }
        return $result;
    }

    private function prepare_instance_name($instance_name) {
        $clean_instance_name = ltrim($instance_name, 'ac-');
        return ucfirst($clean_instance_name);
    }
}