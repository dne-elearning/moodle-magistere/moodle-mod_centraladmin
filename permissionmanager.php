<?php

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once($CFG->dirroot.'/mod/centraladmin/lib.php');
require_once($CFG->libdir.'/adminlib.php');

$context = context_system::instance();

$PAGE->set_cacheable(false);

$PAGE->set_context($context);
$PAGE->set_url('/mod/centraladmin/permissionmanager.php');
$PAGE->set_pagetype('site-index');
$PAGE->set_pagelayout('standard');

$pagedesc = get_string("permission_pagedesc", 'mod_centraladmin');
$PAGE->set_title($pagedesc);
$PAGE->set_heading($pagedesc);

require_capability('mod/centraladmin:managepermission', $context);

//admin_externalpage_setup('mod_centraladmin_permission_manager');

$submitted = optional_param('b_submit', '', PARAM_TEXT);

if ($submitted) {

    $users = optional_param('users_selected', '', PARAM_TEXT);
    $instances = optional_param_array('switchinstance', array(), PARAM_ALPHANUMEXT);
    $views = optional_param_array('switchview', array(), PARAM_ALPHANUMEXT);

    try{
        $users = explode(',', $users);
        centraladmin_permission_set_view($users, $views);
        centraladmin_permission_set_instance($users, $instances);
        redirect($PAGE->url, get_string('addpermission_success', 'mod_centraladmin'), null, \core\output\notification::NOTIFY_SUCCESS);
    }
    catch(Exception $e){

        if (is_siteadmin()) {
            $error = $e->getMessage();
        } else {
            $error = get_string('errordelete', 'mod_centraladmin');
        }
        \core\notification::error($error);
    }
}

// list all users with statsuser role
$users_with_statsrole = $DB->get_records_sql("
SELECT u.id, u.firstname, u.lastname 
FROM {role_assignments} ra, {role} r, {user} u 
WHERE ra.roleid = r.id
      AND ra.userid = u.id
      AND u.deleted = 0
      AND r.shortname = ?
ORDER BY u.firstname ASC", array('statsuser'));

// list all active slaves
$slaves = array_keys($DB->get_records('centraladmin_slave', array('deleted' => 0), 'name ASC', 'name'));

// list all plugins (stats) / subplugin
$views = array();
$enabled_plugins = array_keys(\mod_centraladmin\plugininfo\centraladminstats::get_enabled_plugins());
foreach($enabled_plugins as $plugin){
    $classPath = $CFG->dirroot.'/mod/centraladmin/stats/'.$plugin.'/'.$plugin.'.php';
    $views['centraladminstats_'.$plugin] = [];
    $labelviews['centraladminstats_'.$plugin] = get_string('pluginname', 'centraladminstats_'.$plugin);
    if (file_exists($classPath)) {
        require_once($classPath);
        $classname = 'centraladminstats_'.$plugin;
        $statsplugininstance = new $classname;
        foreach($statsplugininstance->get_subplugins() as $subplugin){
            $views['centraladminstats_'.$plugin][] = $subplugin;
        }
    }
}

$renderer = $PAGE->get_renderer('mod_centraladmin');

echo $OUTPUT->header();
echo $OUTPUT->heading($pagedesc);
echo $renderer->permissionmanager($users_with_statsrole, $slaves, $views, $labelviews);
echo $OUTPUT->footer();
