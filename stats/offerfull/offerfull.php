<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/centraladmin/centraladminstats_plugin.php');
require_once($CFG->dirroot.'/mod/centraladmin/data/offer/offer.php');
require_once($CFG->dirroot.'/mod/centraladmin/lib.php');

class centraladminstats_offerfull extends centraladminstats_plugin {
    
    public function get_name() {
        return get_string('pluginname', 'centraladminstats_offerfull');
    }
    
    /**
     * Return enabled subplugins list from collection's centraladminstatistics plugin instance
     */
    public function get_subplugins() {
        global $CFG;
        
        $plugins = \mod_centraladmin\plugininfo\centraladmincollec::get_enabled_plugins();
        
        $subplugins = array();
        foreach ($plugins AS $plugin) {
            $classPath = $CFG->dirroot.'/mod/centraladmin/stats/offerfull/collection/'.$plugin.'/'.$plugin.'.php';
            if (file_exists($classPath)) {
                require_once($classPath);
                $classname = 'centraladmincollec_'.$plugin;
                $statsplugininstance = new $classname;
                $subplugins[] = array('id' => $plugin, 'name' => $statsplugininstance->get_name(), 'type' => 'centraladmincollec');
            }
        }
        return $subplugins;
    }

    public function get_content() {
        global $DB, $CFG, $PAGE, $OUTPUT;

        $this->check_permission();

        $strs = new stdClass();
        $strs->preview_header_title = get_string('preview_header_title','centraladminstats_offerfull');
        $strs->preview_header_name = get_string('preview_header_name','centraladminstats_offerfull');
        $strs->preview_header_lastpublish = get_string('preview_header_lastpublish','centraladminstats_offerfull');
        $strs->preview_header_keywords = get_string('preview_header_keywords','centraladminstats_offerfull');
        
        $PAGE->requires->js_call_amd('centraladminstats_offerfull/form_inputs', 'init');
        $PAGE->requires->js_call_amd('centraladminstats_offerfull/form', 'init', array('str'=>$strs));
        
        $options = $DB->get_record_sql("SELECT
(SELECT GROUP_CONCAT(DISTINCT `domaines` SEPARATOR '||') FROM {".centraladmindata_offer::$tablename."}) AS domains,
(SELECT GROUP_CONCAT(DISTINCT `origine` SEPARATOR '||') FROM {".centraladmindata_offer::$tablename."}) AS origins,
(SELECT GROUP_CONCAT(DISTINCT `publics` SEPARATOR ',') FROM {".centraladmindata_offer::$tablename."}) AS publics,
(SELECT MIN(gaiausers) FROM {".centraladmindata_offer::$tablename."}) AS gaiausers_min,
(SELECT MAX(gaiausers) FROM {".centraladmindata_offer::$tablename."}) AS gaiausers_max,
(SELECT MIN(durationlocal) FROM {".centraladmindata_offer::$tablename."}) AS durationlocal_min,
(SELECT MAX(durationlocal) FROM {".centraladmindata_offer::$tablename."}) AS durationlocal_max,
(SELECT MIN(nbengagedparticipants) FROM {".centraladmindata_offer::$tablename."}) AS nbengagedparticipants_min,
(SELECT MAX(nbengagedparticipants) FROM {".centraladmindata_offer::$tablename."}) AS nbengagedparticipants_max");
        
        $data = array();
        
        $data['highchartsurl'] = (new moodle_url('/mod/centraladmin/js/highcharts/highcharts.js'))->out();
        $data['highcharts_nodata_url'] = (new moodle_url('/mod/centraladmin/js/highcharts/modules/no-data-to-display.js'))->out();
        
        $publics_ex = explode(',',$options->publics);
        $data['publics'] = array();
        $dedup = array();
        foreach ($publics_ex AS $public){
            $public = trim($public);
            if (!in_array($public,$dedup)){
                $dedup[] = $public;
                $data['publics'][] = array('name'=>$public,'value'=>base64_encode($public));
            }
        }
        usort($data['publics'], array('self','sort_order'));
        
        $origins_ex = explode('||', $options->origins);
        $data['origins'] = array();
        foreach ($origins_ex AS $origin) {
            $data['origins'][] = array('name'=>$origin,'value'=>base64_encode($origin));
        }
        usort($data['origins'], array('self','sort_order'));
        
        $domains_ex = explode('||', $options->domains);
        $data['domains'] = array();
        foreach ($domains_ex AS $domain) {
            $data['domains'][] = array('name'=>$domain,'value'=>base64_encode($domain));
        }
        usort($data['domains'], array('self','sort_order'));
/*
        $natures_ex = explode('||', $options->natures);
        $data['natures'] = array();
        foreach ($natures_ex AS $nature) {
            $data['natures'][] = array('name'=>$nature,'value'=>base64_encode($nature));
        }
        usort($data['natures'], array('self','sort_order'));
*/
        $data['durationconduct'] = array();
        if ($options->durationlocal_min == 0) {
            $data['durationconduct'][] = array('name'=>get_string('duration_remote', 'centraladminstats_offerfull'), 'value'=>'duration_remote');
        }
        if ($options->durationlocal_max > 0) {
            $data['durationconduct'][] = array('name'=>get_string('duration_hybrid', 'centraladminstats_offerfull'),'value'=>'duration_local');
        }

        $data['gaiausers'] = array();
        if ($options->gaiausers_min == 0) {
            $data['gaiausers'][] = array('name'=>get_string('gaia_selftraining', 'centraladminstats_offerfull'),'value'=>'gaia_selftraining');
        }
        if ($options->gaiausers_max > 0) {
            $data['gaiausers'][] = array('name'=>get_string('gaia_prescription', 'centraladminstats_offerfull'),'value'=>'gaia_prescription');
        }

        $data['enroled_users_min'] = $options->nbengagedparticipants_min;
        $data['enroled_users_max'] = $options->nbengagedparticipants_max;
        
        
        $plugins = \mod_centraladmin\plugininfo\centraladmincollec::get_enabled_plugins();
        
        $data['collections'] = array();
        $first = true;
        foreach ($plugins AS $plugin) {
            if (!centraladmin_permission_can_access_plugin($plugin,'centraladmincollec')) {
                continue;
            }
            $classPath = $CFG->dirroot.'/mod/centraladmin/stats/offerfull/collection/'.$plugin.'/'.$plugin.'.php';
            if (file_exists($classPath)) {
                require_once($classPath);
                $classname = 'centraladmincollec_'.$plugin;
                $statspluginInstance = new $classname;
                $data['collections'][] = array('id'=>$plugin,'name'=>$statspluginInstance->get_name(),'class'=>($first?' active':''));
                $first = false;
            }
        }

        if (count($data['collections']) == 0) {
            print_error('no_collection_available', 'centraladmin');
        }

        $data['lastupdate_time_str'] = centraladmindata_offer::get_lastupdate_timecode_str();
        $data['documentation_link'] = get_config('centraladminstats_offerfull', 'documentationlink');

        return $OUTPUT->render_from_template('centraladminstats_offerfull/offerfull', $data);
    }
    
    private function sort_order($a, $b){
        if ($a['name'] == $b['name']) {
            return 0;
        }
        return ($a['name'] < $b['name']) ? -1 : 1;
    }
    
    
    public function get_collection_content($collection){
        global $CFG;
        $plugins = \mod_centraladmin\plugininfo\centraladmincollec::get_enabled_plugins();
        
        if(!in_array($collection, $plugins)){
            return 'Plugin not found';
        }
        
        $classPath = $CFG->dirroot.'/mod/centraladmin/stats/offerfull/collection/'.$collection.'/'.$collection.'.php';
        if (file_exists($classPath)) {
            require_once($classPath);
            $classname = 'centraladmincollec_'.$collection;
            $statspluginInstance = new $classname;
            return $statspluginInstance->get_content();
        }else{
            return 'Plugin do not exist';
        }
    }
    
    
    public function search_coursesids($search, $origins, $keywords, $firstpublish_from, $firstpublish_to, $lastpublish_from, $lastpublish_to,
    $engagedusers_min, $engagedusers_max, $publics, $domains, $durationconduct, $gaiausers)
    {
        global $DB, $USER;

        $where = '';
        $params = array();

        if (!centraladmin_permission_isadmin()) {
            $instance = centraladmin_permission_get_instances();

            if (count($instance) > 0) {
                $where .= ' AND o.slaveid IN (' . implode(',', $instance) . ')';
            } else {
                return array();
            }
        }
        
        $idsearch = false;
        if (!empty($search)) {
            $words = explode(' ', $search);
            foreach ($words AS $str){
                if (strpos($str, 'id=') === 0){
                    $idsearch = substr($str, 3);
                }
            }
            
            if ($idsearch) {
                $idsearch_ex = explode(',', $idsearch);
                $ids = array();
                foreach ($idsearch_ex AS $id) {
                    $ids[] = intval(trim($id));
                }
                $where .= ' AND o.courseid IN('.implode(',', $ids).')';
            }else{
                $words_where = '';
                $i=0;
                foreach ($words AS $word) {
                    if (!empty($words_where)){$words_where .= ' OR ';}
                    $words_where .= 'CONCAT(o.name," ",o.description," ",o.objectif) LIKE :word'.$i;
                    $params['word'.$i] = '%'.$word.'%';
                    $i++;
                }
                $where .= ' AND ('.$words_where.')';
            }
        }
        
        if (count($origins) > 0) {
            $where_origins = '';
            $i = 0;
            foreach ($origins AS $origin){
                if (!empty($where_origins)){$where_origins .= ' OR ';}
                $where_origins .= 'o.origine LIKE :origins'.$i;
                $params['origins'.$i] = '%'.base64_decode($origin).'%';
                $i++;
            }
            $where .= ' AND ('.$where_origins.')';
        }
        
        if (count($publics) > 0) {
            $where_publics = '';
            $i = 0;
            foreach ($publics AS $public){
                if (!empty($where_publics)){$where_publics .= ' OR ';}
                $where_publics .= 'o.publics LIKE :publics'.$i;
                $params['publics'.$i] = '%'.base64_decode($public).'%';
                $i++;
            }
            $where .= ' AND ('.$where_publics.')';
        }
        
        if (count($domains) > 0) {
            $where_domains = '';
            $i = 0;
            foreach ($domains AS $domain){
                if (!empty($where_domains)){$where_domains .= ' OR ';}
                $where_domains .= 'o.domaines LIKE :domains'.$i;
                $params['domains'.$i] = '%'.base64_decode($domain).'%';
                $i++;
            }
            $where .= ' AND ('.$where_domains.')';
        }

        /*
        if (count($natures) > 0) {
            $where_nature = '';
            $i = 0;
            foreach ($natures AS $nature){
                if (!empty($where_nature)){$where_nature .= ' OR ';}
                $where_nature .= 'o.nature LIKE :nature'.$i;
                $params['nature'.$i] = '%'.base64_decode($nature).'%';
                $i++;
            }
            $where .= ' AND ('.$where_nature.')';
        }
        */

        if (count($durationconduct) == 1) {
            $where_durationconduct = array();
            if (in_array('duration_remote', $durationconduct)) {
                $where_durationconduct[] = 'o.durationlocal = 0';
            }
            if (in_array('duration_local', $durationconduct)) {
                $where_durationconduct[] = 'o.durationlocal > 0';
            }
            $where .= ' AND ('.implode(' AND ', $where_durationconduct).')';
        }

        if (count($gaiausers) == 1) {
            $where_gaiausers = array();
            if (in_array('gaia_selftraining', $gaiausers)) {
                $where_gaiausers[] = 'o.gaiausers = 0';
            }
            if (in_array('gaia_prescription', $gaiausers)) {
                $where_gaiausers[] = 'o.gaiausers > 0';
            }
            $where .= ' AND ('.implode(' AND ', $where_gaiausers).')';
        }
        
        if (!empty($keywords)){
            $keywords_ex = explode(' ', $keywords);
            $where_keywords = '';
            $i = 0;
            foreach ($keywords_ex AS $keyword) {
                if (!empty($where_keywords)){$where_keywords .= ' AND ';}
                $where_keywords .= 'o.keywords LIKE :keyword'.$i;
                $params['keyword'.$i] = '%'.$keyword.'%';
                $i++;
            }
            $where .= ' AND ('.$where_keywords.')';
        }
        
        $firstpublish_from_matches = null;
        $firstpublish_to_matches = null;
        if (!empty($firstpublish_from) && preg_match('~(\d{2})/(\d{2})/(\d{4})~', $firstpublish_from, $firstpublish_from_matches) && !empty($firstpublish_to) && preg_match('~(\d{2})/(\d{2})/(\d{4})~', $firstpublish_to, $firstpublish_to_matches)){
            $firstpublish_from_time = mktime(0,0,0,$firstpublish_from_matches[2],$firstpublish_from_matches[1],$firstpublish_from_matches[3]);
            $firstpublish_to_time = mktime(23,59,59,$firstpublish_to_matches[2],$firstpublish_to_matches[1],$firstpublish_to_matches[3]);
            if ($firstpublish_from_time < $firstpublish_to_time){
                $where .= ' AND o.firstpublish BETWEEN '.$firstpublish_from_time.' AND '.$firstpublish_to_time;
            }
        }
        
        $lastpublish_from_matches = null;
        $lastpublish_to_matches = null;
        if (!empty($lastpublish_from) && preg_match('~(\d{2})/(\d{2})/(\d{4})~', $lastpublish_from, $lastpublish_from_matches) && !empty($lastpublish_to) && preg_match('~(\d{2})/(\d{2})/(\d{4})~', $lastpublish_to, $lastpublish_to_matches)){
            $lastpublish_from_time = mktime(0,0,0,$lastpublish_from_matches[2],$lastpublish_from_matches[1],$lastpublish_from_matches[3]);
            $lastpublish_to_time = mktime(23,59,59,$lastpublish_to_matches[2],$lastpublish_to_matches[1],$lastpublish_to_matches[3]);
            if ($lastpublish_from_time < $lastpublish_to_time){
                $where .= ' AND o.lastpublish BETWEEN '.$lastpublish_from_time.' AND '.$lastpublish_to_time;
            }
        }
        
        $where .= ' AND o.nbengagedparticipants BETWEEN '.$engagedusers_min.' AND '.$engagedusers_max;

        $sql = "SELECT id FROM {".centraladmindata_offer::$tablename."} o WHERE 1".$where;
        
        $res = $DB->get_records_sql($sql, $params);
        
        $courseids = array();
        foreach ($res AS $r){
            $courseids[] = $r->id;
        }

        if(count($courseids) == 0) {
            return array(0);
        }

        return $courseids;
    }
}