define(["core/ajax", "mod_centraladmin/colors"],function(ajax, colors) {

    function getDateOfWeek(w, y) {
        var d = (1 + (w - 1) * 7); // 1st of January + 7 days for each week
    
        return Date.UTC(y, 0, d);
    }

    var divId = 'offer_achievement';

    function init() { 

        var promises = ajax.call([
            { methodname: 'centraladminstats_offerview_achievement', args: {} },
        ]);

        promises[0].done(function(response) {

            var badges = {
                name: 'Badges délivrés',
                data: [],
             }
            var completions = {
                name: 'Achèvement de parcours',
                data: [],
             }
            xAxisCategories = [];

            response.forEach(element => {
                // compute first day of week to have a proper date
                var year = element.week.substring(0,4);
                var week = element.week.substring(5);
                var weekDate = getDateOfWeek(week,year);

                xAxisCategories.push(weekDate);
                badges.data.push(element.badges);
                completions.data.push(element.completions);
            });


            var series = [
                badges,
                completions, 
            ];

            // Highcharts.dateFormats.W = function (timestamp) {
            //     var date = new Date(timestamp),
            //         day = date.getUTCDay() === 0 ? 7 : date.getUTCDay(),
            //         dayNumber;
            
            //     date.setDate(date.getUTCDate() + 4 - day);
            //     dayNumber = Math.floor((date.getTime() - new Date(date.getUTCFullYear(), 0, 1, -6)) / 86400000);
            
            //     return 1 + Math.floor(dayNumber / 7);
            // };

            // add custom code for negative logarythme
            (function (H) {
                H.addEvent(H.Axis, 'afterInit', function () {
                    const logarithmic = this.logarithmic;
            
                    if (logarithmic && this.options.custom.allowNegativeLog) {
            
                        // Avoid errors on negative numbers on a log axis
                        this.positiveValuesOnly = false;
            
                        // Override the converter functions
                        logarithmic.log2lin = num => {
                            const isNegative = num < 0;
            
                            let adjustedNum = Math.abs(num);
            
                            if (adjustedNum < 10) {
                                adjustedNum += (10 - adjustedNum) / 10;
                            }
            
                            const result = Math.log(adjustedNum) / Math.LN10;
                            return isNegative ? -result : result;
                        };
            
                        logarithmic.lin2log = num => {
                            const isNegative = num < 0;
            
                            let result = Math.pow(10, Math.abs(num));
                            if (result < 10) {
                                result = (10 * (result - 1)) / (10 - 1);
                            }
                            return isNegative ? -result : result;
                        };
                    }
                });
            }(Highcharts));

            Highcharts.chart(divId, {
                title: {
                    text: 'Évolution de l\'achèvement de parcours'
                },

                xAxis: {
                    type: "datetime",
                    title: {
                        text: ''
                    },
                    tickInterval: 24 * 3600 * 1000 * 7 * 4, // 1 month
                    labels: {
                        format: '{value:%d-%m-%Y}',
                        align: 'right',
                    }
                    
                },
                yAxis: {
                    type: 'logarithmic',
                    custom: {
                        allowNegativeLog: true
                    },
                    title: {
                        text: '',
                    },
                },
                plotOptions: {
                    series: {
                        pointStart: xAxisCategories[0],
                        pointInterval: 24 * 3600 * 1000 *7, // a week
                    }
                },
                tooltip: {
                    xDateFormat: '<b>%d-%m-%Y</b>',
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: {point.y} <br/>',
                    shared: true,
                },
                series: series,
                lang: {
                    noData: "Aucune donnée à afficher"
                },
                noData: {
                    style: {
                        fontWeight: 'bold',
                        fontSize: '15px',
                        color: '#303030'
                    }
                },
                credits: {
                    enabled: false,
                },
                colors: colors.yellowredgradient,
            });

        }).fail(function(ex) {
            document.getElementById(divId).innerHTML = '<p>Une erreur est survenue lors de la récupération des données du graphiques "Évolution de l\'achèvement de parcours"</p>';
            console.error(ex);
        });

    }

    return {
    	init : function() {
            init();
        }
    };
});