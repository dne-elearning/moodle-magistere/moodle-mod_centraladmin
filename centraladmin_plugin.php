<?php

defined('MOODLE_INTERNAL') || die();

abstract class centraladmin_plugin {

    public abstract function get_subtype();

    public abstract function get_name();
    
    public function get_subplugins() {
        return array();
    }
}