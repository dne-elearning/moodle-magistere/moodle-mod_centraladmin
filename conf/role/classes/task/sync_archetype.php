<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file defines a schedule task to clean the todo table
 *
 * @package    centraladminconf_role
 * @copyright  2022 Reseau Canope
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace centraladminconf_role\task;

require_once($CFG->dirroot.'/mod/centraladmin/conf/role/role.php');
require_once($CFG->dirroot.'/mod/centraladmin/conf/role/externallib.php');


defined('MOODLE_INTERNAL') || die();

class sync_archetype extends \core\task\scheduled_task {
    
    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('sync_archetype', 'centraladminconf_role');
    }
    
    /**
     * Execute the scheduled task.
     */
    public function execute() {
        $role = new \centraladminconf_role();
        $role->update_archetype_cache();
    }

}

