<?php

use mod_centraladmin\plugininfo\centraladminconf;

require_once(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->dirroot.'/mod/centraladmin/data/offer/offer.php');
require_once($CFG->dirroot.'/mod/centraladmin/lib.php');

@ini_set('display_errors', '0');
$CFG->debug = false; //E_ALL | E_STRICT;   // DEBUG_DEVELOPER // NOT FOR PRODUCTION SERVERS!
$CFG->debugdisplay = false;

require_login();
$context = context_system::instance();

require_capability('centraladminstats/offerview:view', $context);

$where = '';
if (!centraladmin_permission_isadmin()) {
    $instances = centraladmin_permission_get_instances();
    $where = ' AND cs.id IN('.implode(',', $instances).')';
}

$query = 
'SELECT 
co.name, 
co.timecreated, 
co.startdate, 
co.lastpublish, 
co.duration, 
co.durationlocal, 
co.durationremote, 
co.duration as durationconduct, 
co.gaiausers, 
co.nbparticipants, 
co.nbengagedparticipants, 
a.nbparticipants1d,
a.nbparticipants2d,
a.nbparticipantsndef,
a.nbparticipantspublic,
a.nbparticipantsprive,
a.nbparticipantsnoschool,
co.domaines, 
cs.name as origine, 
co.category,
co.publics, 
co.keywords, 
co.courseachievement, 
co.courseachievements, 
co.nbactivities, 
co.nbreceptiveactivities,
co.nbactiveactivities,
co.nbcreativeactivities,
co.nbinteractiveactivities,
co.nbbadges, 
co.nbdeliveredbadges, 
co.nbbadgedparticipants,
co.authors,
co.courseurl
FROM mdl_'.centraladmindata_offer::$tablename.' co 
INNER JOIN mdl_'.centraladmindata_offer::$attendance_tablename.' a ON co.courseid = a.courseid AND co.slaveid = a.slaveid
INNER JOIN mdl_centraladmin_slave cs ON co.slaveid = cs.id 
WHERE cs.disabled = 0 AND cs.deleted = 0'.$where.'
ORDER BY lastpublish DESC';
$records = $DB->get_records_sql($query);

$delimiter = get_string('listsep','core_langconfig');
$dateformat = 'd/m/Y';

$headers = array(
    get_string('csv_name', 'centraladminstats_offerview'),
    get_string('csv_createdate', 'centraladminstats_offerview'),
    get_string('csv_startdate', 'centraladminstats_offerview'),
    get_string('csv_lastpublish', 'centraladminstats_offerview'),
    get_string('csv_duration', 'centraladminstats_offerview'),
    get_string('csv_durationlocal', 'centraladminstats_offerview'),
    get_string('csv_durationremote', 'centraladminstats_offerview'),
    get_string('csv_durationconduct', 'centraladminstats_offerview'),
    get_string('csv_gaiausers', 'centraladminstats_offerview'),
    get_string('csv_nbparticipants', 'centraladminstats_offerview'),
    get_string('csv_nbengagedparticipants', 'centraladminstats_offerview'),
    get_string('csv_1D', 'centraladminstats_offerview'),
    get_string('csv_2D', 'centraladminstats_offerview'),
    get_string('csv_other', 'centraladminstats_offerview'),
    get_string('csv_public', 'centraladminstats_offerview'),
    get_string('csv_private', 'centraladminstats_offerview'),
    get_string('csv_withoutschool', 'centraladminstats_offerview'),
    get_string('csv_domaines', 'centraladminstats_offerview'),
    get_string('csv_origine', 'centraladminstats_offerview'),
    get_string('csv_category', 'centraladminstats_offerview'),
    get_string('csv_publics', 'centraladminstats_offerview'),
    get_string('csv_keywords', 'centraladminstats_offerview'),
    get_string('csv_courseachievement', 'centraladminstats_offerview'),
    get_string('csv_courseachievements', 'centraladminstats_offerview'),
    get_string('csv_nbactivities', 'centraladminstats_offerview'),
    get_string('csv_nbreceptiveactivities', 'centraladminstats_offerview'),
    get_string('csv_nbactiveactivities', 'centraladminstats_offerview'),
    get_string('csv_nbcreativeactivities', 'centraladminstats_offerview'),
    get_string('csv_nbinteractiveactivities', 'centraladminstats_offerview'),
    get_string('csv_nbbadges', 'centraladminstats_offerview'),
    get_string('csv_nbdeliveredbadges', 'centraladminstats_offerview'),
    get_string('csv_nbbadgedparticipants', 'centraladminstats_offerview'),
    get_string('csv_authors', 'centraladminstats_offerview'),
    get_string('csv_courseurl', 'centraladminstats_offerview'),
);

$filename = 'magistere_offre_de_formation.csv';
header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename="'.$filename.'";');

$out = fopen('php://output', 'w+');

// for excel
fputs($out, (chr(0xEF) . chr(0xBB) . chr(0xBF)));

fputcsv($out, $headers, $delimiter);

foreach($records AS $record) {
    $record->duration = number_format(($record->duration / 60), 2, get_string('decsep','core_langconfig'), get_string('thousandssep','core_langconfig'));
    $record->durationlocal = number_format(($record->durationlocal / 60), 2, get_string('decsep','core_langconfig'), get_string('thousandssep','core_langconfig'));
    $record->durationremote = number_format(($record->durationremote / 60), 2, get_string('decsep','core_langconfig'), get_string('thousandssep','core_langconfig'));

    $record->courseachievement = $record->courseachievement
        ? get_string('yes', 'centraladminstats_offerview')
        : get_string('no', 'centraladminstats_offerview');
    $record->durationconduct = $record->durationlocal == 0
        ? get_string('duration_remote', 'centraladminstats_offerview')
        : get_string('duration_hybrid', 'centraladminstats_offerview');
    $record->gaiausers = $record->gaiausers > 0
        ? get_string('gaia_prescription', 'centraladminstats_offerview')
        : get_string('gaia_selftraining', 'centraladminstats_offerview');

    // Take only the three first keywords
    $record->keywords = implode(', ', array_slice(explode(",", $record->keywords), 0, 3));

    $record->timecreated = date($dateformat,$record->timecreated);
    $record->startdate = $record->startdate ? date($dateformat,$record->startdate) : null;
    $record->lastpublish = date($dateformat,$record->lastpublish);

    fputcsv($out, (array) $record, $delimiter);
}

fclose($out);