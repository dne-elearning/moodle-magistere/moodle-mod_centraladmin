<?php
defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');


class slavemanager_form extends moodleform {

    public const PLUGIN_NAME = 'mod_centraladmin';

    /**
     * Form definition.
     */
    function definition() {

		$mform    = $this->_form;

		$mform->addElement('header', 'add_slave', get_string('addslave', self::PLUGIN_NAME));
		$mform->setExpanded('add_slave');
        
		$mform->addElement('text', 'slave_url', get_string('slaveurl', self::PLUGIN_NAME), array('size'=>1000));
        $mform->setType('slave_url', PARAM_URL);
        $mform->addRule('slave_url', get_string('required'), 'required', null, 'client');
		$mform->addRule('slave_url', get_string('required'), 'required', null, 'server');

        $mform->addElement('text', 'username', get_string('username', self::PLUGIN_NAME), array('size'=>200));
        $mform->setType('username', PARAM_USERNAME);
        $mform->addRule('username', get_string('required'), 'required', null, 'client');
		$mform->addRule('username', get_string('required'), 'required', null, 'server');

        $mform->addElement('password', 'password', get_string('password', self::PLUGIN_NAME), array('size'=>200));
        $mform->setType('password', PARAM_TEXT);
        $mform->addRule('password', get_string('required'), 'required', null, 'client');
		$mform->addRule('password', get_string('required'), 'required', null, 'server');
		
		// When two elements we need a group.
		$buttonarray = array();
		$classarray = array('class' => 'form-submit');
		        
		$buttonarray[] = &$mform->createElement('submit', 'saveanddisplay', get_string('confirmslave', self::PLUGIN_NAME), $classarray);

		$mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
    }

	function validation($data, $files) {
		$errors = parent::validation($data, $files);
/*
		$arrContextOptions=array(
			"ssl"=>array(
				"verify_peer"=>false,
				"verify_peer_name"=>false,
			),
		);*/

        // check http, https 
		if (strpos($data['slave_url'], 'http://') === false && strpos($data['slave_url'], 'https://') === false) {
			$errors['slave_url'] = 'L\'URL n\'est pas formée correctement';
		} //elseif (!file_get_contents($data['slave_url'], false, stream_context_create($arrContextOptions))) {
		// 	$errors['slave_url'] = 'Impossible de contacter le domaine, vérifiez l\'URL';
		// }

		return $errors;

	}
}
