<?php

class mod_centraladmin_external extends external_api {
    
    
    public static function ping_parameters() {
        return new external_function_parameters(array());
    }
    
    
    public static function ping_returns() {
        return new external_single_structure(
            array(
                'ping' => new external_value(PARAM_ALPHA, '')
            )
        );
    }
    
    public static function ping() {
        return array('ping'=>'pong');
    }
    
    
    public static function get_user_permission_parameters() {
        return new external_function_parameters(array(
            'userid' => new external_value(PARAM_INT, 'user id', VALUE_DEFAULT, 0),
        ));
    }
    
    public static function get_user_permission_returns() {
        return new external_single_structure(
            array(
                'slaves' => new external_value(PARAM_RAW, ''),
                'views' => new external_value(PARAM_RAW, '')
            )
        );
    }
    
    public static function get_user_permission(int $userid) {
        global $DB;
        
        $views = $DB->get_records('centraladmin_perms_plugin', array('userid' => $userid), 'type ASC, plugin ASC');
        $slaves = $DB->get_records('centraladmin_perms_instance', array('userid' => $userid), 'instance ASC');
        
        $views_list = array_map(function($v){
            return $v->type.'_'.$v->plugin;
        }, $views);
        
        $slaves_list = array_map(function($s){
            return $s->instance;
        }, $slaves);
        
        return array(
            'views' => implode(',', $views_list),
            'slaves'=> implode(',', $slaves_list)
        );
    }
}
