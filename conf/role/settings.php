<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   mod_centraladmin_conf_role
 * @copyright Reseau_Canope 2022
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/mod/centraladmin/conf/role/role.php');

if ($ADMIN->fulltree) {
    $settings->add(new admin_setting_configtext(
        'centraladminconf_role/cacherefreshdelay',
        new lang_string('cacherefreshdelay', 'centraladminconf_role'),
        new lang_string('cacherefreshdelay_desc', 'centraladminconf_role'),
        centraladminconf_role::DEFAULT_CACHE_REFRESH_DELAY, // 1 day
        PARAM_INT
    ));

    $settings->add(new admin_setting_configtext(
        'centraladminconf_role/todolifetime',
        new lang_string('todolifetime', 'centraladminconf_role'),
        new lang_string('todolifetime_desc', 'centraladminconf_role'),
        centraladminconf_role::DEFAULT_TODO_LIFETIME, // 2 days
        PARAM_INT
    ));
}