<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lib
 *
 * @package   mod_centraladmin
 * @copyright 2021 TCS
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use mod_centraladmin\plugininfo\centraladminstats;

defined('MOODLE_INTERNAL') || die();


function centraladmin_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_ARCHETYPE:           return MOD_ARCHETYPE_SYSTEM;
        case FEATURE_GROUPS:                  return false;
        case FEATURE_GROUPINGS:               return false;
        case FEATURE_MOD_INTRO:               return false;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return false;
        case FEATURE_GRADE_HAS_GRADE:         return false;
        case FEATURE_GRADE_OUTCOMES:          return false;
        case FEATURE_BACKUP_MOODLE2:          return false;
        case FEATURE_SHOW_DESCRIPTION:        return false;
        case FEATURE_NO_VIEW_LINK:            return true;
        
        default: return null;
    }
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @global object
 * @param object $label
 * @return bool|int
 */
function centraladmin_add_instance($centraladmin) {
    global $DB;
    
    $centraladmin->name = 'centraladmin';
    $centraladmin->intro = '';
    $centraladmin->timemodified = time();
    $centraladmin->timecreated = time();
    
    $id = $DB->insert_record("centraladmin", $centraladmin);
    
    return $id;
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @global object
 * @param object $label
 * @return bool
 */
function centraladmin_update_instance($centraladmin) {
    global $DB;
    
    $centraladmin->timemodified = time();
    $centraladmin->id = $centraladmin->instance;
    
    return $DB->update_record("centraladmin", $centraladmin);
}

/**
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @global object
 * @param int $id
 * @return bool
 */
function centraladmin_delete_instance($id) {
    global $DB;
    
    if (! $centraladmin = $DB->get_record("centraladmin", array("id"=>$id))) {
        return false;
    }
    
    return  $DB->delete_records("centraladmin", array("id"=>$centraladmin->id));
}


/**
 * Given a course_module object, this function returns any
 * "extra" information that may be needed when printing
 * this activity in a course listing.
 * See get_array_of_activities() in course/lib.php
 *
 * @global object
 * @param object $coursemodule
 * @return cached_cm_info|null
 */
function centraladmin_get_coursemodule_info($coursemodule) {
    $info = new cached_cm_info();
    $info->content = '';
    $info->name  = get_string('pluginname', 'mod_centraladmin');
    return $info;
}

//cm_info_dynamic


function centraladmin_cm_info_view(cm_info $cm) {
    global $CFG;

    $plugins = centraladminstats::get_enabled_plugins();
    
    $view = optional_param('view', '', PARAM_ALPHA);

    $plugin = 'offerview';
    if ($view == 'offerfull' && in_array('offerfull', $plugins)){
        $plugin = 'offerfull';
    }
    
    $html = '';
    $classPath = $CFG->dirroot.'/mod/centraladmin/stats/'.$plugin.'/'.$plugin.'.php';
    if (file_exists($classPath)) {
        require_once($classPath);
        $classname = 'centraladminstats_'.$plugin;
        $statspluginInstance = new $classname;
        $html = $statspluginInstance->get_content();
    }
    
    $css_url = new moodle_url('/lib/jquery/ui-1.12.1/jquery-ui.min.css');
    $cm->set_content('<link rel="stylesheet" href="'.$css_url->out().'"><p>'.$html.'</p>', true);
}



function centraladmin_permission_set_instance(array $users, array $data = array(), bool $autoset = false) {
    global $DB;
    
    $table = 'centraladmin_perms_instance';
    
    if (count($users) > 0){
        
        $userids = implode(',', $users);
        if (!$DB->delete_records_select($table, 'userid IN ('. $userids .')')) {
            print_error('Error: deleting records for users ('.$userids.') the table "'.$table.'" failed');
        }
        
        foreach($users AS $user){
            foreach(array_keys($data) AS $name) {
                $user_data = array(
                    'userid' => $user,
                    'instance' => $name,
                    'autoset' => $autoset
                );
                $DB->insert_record($table, $user_data);
            }
        }
    }
    return true;
}

function centraladmin_permission_set_view(array $users, array $data = array(), $autoset = false) {
    global $DB;
    
    $table = 'centraladmin_perms_plugin';
    
    if (count($users) > 0){
        $userids = implode(',', $users);
        if (!$DB->delete_records_select($table, 'userid IN ('. $userids .')')) {
            print_error('Error: deleting records for users ('.$userids.') the table "'.$table.'" failed');
        }
        foreach($users AS $user){
            foreach($data as $name => $value) {
                $pos = strpos($name, '_');
                $type = substr($name,0, $pos);
                $plugin = substr($name,$pos+1);
                $user_data = array(
                    'userid' => $user,
                    'type' => $type,
                    'plugin' => $plugin,
                    'autoset' => $autoset
                );
                $DB->insert_record($table, $user_data);
            }
        }
    }
    return true;
}

function centraladmin_permission_get_instances() {
    global $DB, $USER;
    $instances = $DB->get_records_sql('
    SELECT cs.id 
    FROM {centraladmin_slave} cs
    INNER JOIN {centraladmin_perms_instance} cpi ON (cpi.instance = cs.name)
    WHERE cpi.userid = :userid
    ', array('userid' => $USER->id));

    $instance_ids = array();
    foreach ($instances AS $ins) {
        $instance_ids[] = $ins->id;
    }
    if (count($instance_ids) == 0){
        return array(0);
    }
    return $instance_ids;
}

function centraladmin_permission_can_access_plugin($pluginname, $type = null) {
    global $DB, $USER;
    if (centraladmin_permission_isadmin()) {
        return true;
    }
    if ($type == null) {
        $pos = strpos($pluginname, '_');
        $type = substr($pluginname, 0, $pos);
        $pluginname = substr($pluginname, $pos + 1);
    }

    $instances = $DB->get_records_sql('
    SELECT cpp.id 
    FROM {centraladmin_perms_plugin} cpp
    WHERE cpp.userid = :userid AND cpp.plugin = :plugin AND cpp.type = :type
    ', array('userid' => $USER->id, 'plugin' => $pluginname, 'type' => $type));

    return count($instances) > 0;
}

function centraladmin_permission_isadmin() {
    return is_siteadmin() || has_capability('mod/centraladmin:managepermission', context_system::instance());
}