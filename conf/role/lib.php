<?php


function createMultiSelect($htmlid, $optionsValue, $optionsLabel, $classes = '', $multiple=true) {
    $html = '<select id="'.$htmlid.'" class="'.$classes.'" '.($multiple ? 'multiple' : '').'>';

    for($i = 0; $i < count($optionsValue); $i++) {
        if (!$multiple) {
            $html.= '<option disabled selected value></option>';
        }
        $html .= '<option value="'.$optionsValue[$i].'">'.$optionsLabel[$i].'</option>';
    }
    $html .= '</select>'; 
    return $html;
}