<?php

require_once("$CFG->libdir/externallib.php");
require_once($CFG->dirroot.'/mod/centraladmin/data/offer/offer.php');
require_once($CFG->dirroot.'/mod/centraladmin/lib.php');

class centraladminstats_offerview_external extends external_api {

    public const PLUGIN_NAME = 'centraladminstats_offerview';

    public static function get_courses_by_domain_parameters() {
        return new external_function_parameters(array());
    }

    public static function get_courses_by_domain_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'domaine' => new external_value(PARAM_TEXT, ''),
                    'nbcourses' => new external_value(PARAM_INT, ''),
                )
            )
        );
    }

    public static function get_courses_by_domain() {
        global $DB;
        $where = '';
        if (!centraladmin_permission_isadmin()) {
            $instances = centraladmin_permission_get_instances();
            $where = ' AND slave.id IN('.implode(',', $instances).')';
        }

        $sql = "SELECT offer.domaines as domaine, COUNT(offer.id) as nbcourses 
FROM {".centraladmindata_offer::$tablename."} offer 
INNER JOIN {centraladmin_slave} slave ON offer.slaveid = slave.id
WHERE slave.disabled = 0 AND slave.deleted = 0".$where."
GROUP BY domaines";
        $data = $DB->get_records_sql($sql);
        return $data;
    }

    public static function get_courses_by_origin_parameters() {
        return new external_function_parameters(array());
    }

    public static function get_courses_by_origin_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'origine' => new external_value(PARAM_TEXT, ''),
                    'nbcourses' => new external_value(PARAM_INT, ''),
                )
            )
        );
    }

    public static function get_courses_by_origin() {
        global $DB;
        $where = '';
        if (!centraladmin_permission_isadmin()) {
            $instances = centraladmin_permission_get_instances();
            $where = ' AND slave.id IN('.implode(',', $instances).')';
        }

        $sql = "SELECT slave.name as origine, COUNT(offer.id) as nbcourses 
FROM {".centraladmindata_offer::$tablename."} offer
INNER JOIN {centraladmin_slave} slave ON offer.slaveid = slave.id
WHERE slave.disabled = 0 AND slave.deleted = 0".$where."
GROUP BY origine ORDER BY nbcourses DESC";
        $data = $DB->get_records_sql($sql);
        return $data;
    }

    public static function get_offer_evolution_parameters() {
        return new external_function_parameters(array());
    }

    public static function get_offer_evolution_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'year' => new external_value(PARAM_INT, ''),
                    'nbcoursesupdated' => new external_value(PARAM_INT, ''),
                    'nbcoursescreated' => new external_value(PARAM_INT, ''),
                )
            )
        );
    }

    public static function get_offer_evolution() {
        global $DB;

        $offerByYears = array();
        $where = '';
        if (!centraladmin_permission_isadmin()) {
            $instances = centraladmin_permission_get_instances();
            $where = ' AND slave.id IN('.implode(',', $instances).')';
        }

        $offers = $DB->get_records(centraladmindata_offer::$tablename);
        $offers = $DB->get_records_sql(
"SELECT offer.firstpublish, offer.lastpublish
FROM {".centraladmindata_offer::$tablename."} offer
INNER JOIN {centraladmin_slave} slave ON offer.slaveid = slave.id
WHERE slave.disabled = 0 AND slave.deleted = 0".$where
        );
        foreach($offers as $offer) {
            // we need to extract the year of both timestamp
            $yearCreated = idate('Y', $offer->firstpublish);
            $yearUpdated = idate('Y', $offer->lastpublish);

            if ($yearCreated === $yearUpdated) {
                if (!isset($offerByYears[$yearCreated])) {
                    $offerByYears[$yearCreated] = new stdClass();
                    $offerByYears[$yearCreated]->year = $yearCreated;
                    $offerByYears[$yearCreated]->nbcoursescreated = 0;
                    $offerByYears[$yearCreated]->nbcoursesupdated = 0;
                }
                $offerByYears[$yearUpdated]->nbcoursescreated += 1; 
            } else if ($yearUpdated > $yearCreated) {
                if (!isset($offerByYears[$yearUpdated])) {
                    $offerByYears[$yearUpdated] = new stdClass();
                    $offerByYears[$yearUpdated]->year = $yearUpdated;
                    $offerByYears[$yearUpdated]->nbcoursescreated = 0;
                    $offerByYears[$yearUpdated]->nbcoursesupdated = 0;
                }
                $offerByYears[$yearUpdated]->nbcoursesupdated += 1; 
            }
        }

        // sort the array by descending order
        krsort($offerByYears);

        return $offerByYears;
    }

    public static function get_offer_achievement_parameters() {
        return new external_function_parameters(array());
    }

    public static function get_offer_achievement_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'week' => new external_value(PARAM_ALPHANUMEXT, ''),
                    'completions' => new external_value(PARAM_INT, ''),
                    'badges' => new external_value(PARAM_INT, ''),
                )
            )
        );
    }

    public static function get_offer_achievement() {
        global $DB;
        $where = '';
        if (!centraladmin_permission_isadmin()) {
            $instances = centraladmin_permission_get_instances();
            $where = ' AND slave.id IN('.implode(',', $instances).')';
        }

        $sql = 
"SELECT 
CONCAT(YEAR(daydate),'-',WEEK(daydate)) AS week, 
sum(nbbadge) AS badges, 
sum(nbcompletion) AS completions
FROM {".centraladmindata_offer::$completion_tablename."} offercmpl INNER JOIN {centraladmin_slave} slave ON offercmpl.slaveid = slave.id
WHERE slave.disabled = 0 AND slave.deleted = 0".$where."
GROUP BY(week)
";

        $records = $DB->get_records_sql($sql);
        return $records;
    }

}

