<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/centraladmin/centraladmincollec_plugin.php');

class centraladmincollec_offerperf extends centraladmincollec_plugin {
    
    public function get_name() {
        return get_string('pluginname', 'centraladmincollec_offerperf');
    }

    public function get_content() {
        global $DB, $CFG;

        
        $html = '<div id="offerperf"></div>';
        
        
        return $html;
    }
}