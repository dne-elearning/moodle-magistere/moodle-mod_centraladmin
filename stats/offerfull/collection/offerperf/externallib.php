<?php

require_once($CFG->libdir.'/externallib.php');
require_once($CFG->dirroot.'/mod/centraladmin/data/offer/offer.php');
require_once($CFG->dirroot.'/mod/centraladmin/stats/offerfull/offerfull.php');

class centraladmincollec_attendance_external extends external_api {

    public const PLUGIN_NAME = 'centraladminstats_offerfull';

    public static function get_top_courses_data_parameters() {
        return new external_function_parameters(
            array(
                'search'   => new external_value(PARAM_RAW, ''),
                'origins'  => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'keywords' => new external_value(PARAM_RAW, ''),
                'firstpublish_from'=> new external_value(PARAM_RAW, ''),
                'firstpublish_to'  => new external_value(PARAM_RAW, ''),
                'lastpublish_from' => new external_value(PARAM_RAW, ''),
                'lastpublish_to'   => new external_value(PARAM_RAW, ''),
                'engagedusers_min' => new external_value(PARAM_INT, ''),
                'engagedusers_max' => new external_value(PARAM_INT, ''),
                'publics' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'domains' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'durationconduct' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'gaiausers' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'collection' => new external_value(PARAM_ALPHA, ''),
                'interval' => new external_value(PARAM_ALPHA, ''),
            )
        );
    }

    public static function get_top_courses_data_returns() {
        return new external_single_structure(
            array(
                'coursename' => new external_value(PARAM_RAW, ''),
                'courseattendance' => new external_value(PARAM_RAW, '')
            )
        );
    }

    public static function get_top_courses_data($search, $origins, $keywords, $firstpublish_from, $firstpublish_to, $lastpublish_from, $lastpublish_to, 
                                                $engagedusers_min, $engagedusers_max, $publics, $domains, $durationconduct, $gaiausers, $collection, $interval)
    {

        global $DB; 

        $offerfull = new centraladminstats_offerfull();
        $coursesids = $offerfull->search_coursesids($search, $origins, $keywords, $firstpublish_from, $firstpublish_to, $lastpublish_from, $lastpublish_to, $engagedusers_min, $engagedusers_max, $publics, $domains, $durationconduct, $gaiausers);
        
        if (count($coursesids) == 0) {
            return array(
                'coursename'=>'',
                'courseattendance'=>''
            );
        }

        $nbparticipantField = 'nbparticipants';
        if ($interval == 'currentyear') {
            $nbparticipantField = 'nbparticipantscurrentyear';
        } else if ($interval == 'lastyear') {
            $nbparticipantField = 'nbparticipantslastyear';
        }
        $sql = 
"SELECT offer.publishid, offer.name as coursename, ".$nbparticipantField." as nbparticipant
FROM {".centraladmindata_offer::$tablename."} offer
WHERE id IN (".implode(',', $coursesids).")
ORDER BY nbparticipant DESC
LIMIT 20 ";

        $records = $DB->get_records_sql($sql);

        $coursename = '';
        $courseattendance = '';
        foreach($records as $record) {
            $coursename .= $record->coursename.'|';
            $courseattendance .= $record->nbparticipant.',';
        }

        return array(
            'coursename'=>$coursename,
            'courseattendance'=>$courseattendance
        );


    }



}

