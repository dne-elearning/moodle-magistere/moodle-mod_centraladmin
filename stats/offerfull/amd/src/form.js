/* jshint ignore:start */
define(['jquery', 'core/ajax', 'mod_centraladmin/jtable'], function($, ajax) {

    var collection_amd = '';
    var is_collection_loaded = false;
	var str;    

    function init(lstr) {
		str = lstr;
        setevents();
    }
    
    function setevents() {
    	$('#f_filter').on('click', function(){
    		if (!is_collection_loaded){
    			loadstats();
    		}else{
    			refreshstats();
    		}
    	});
    	$('#f_clear_filter').on('click', function(){
    		resetform();
    	});
    	$('#f_preview').on('click', function(){
    		showpreview();
    	});
    	$('#f_export').on('click', function(){
    		exportdata();
    	});
    	$('.collection').on('click', function(e){
    		if ($(this).hasClass('active')){return}
    		$('.collection.active').removeClass('active');
    		$(this).addClass('active');
    		loadstats();
    	});
    }
    
    function formdata() {
    	var data = new Object();
    	data.search = $('#f_search').val();
    	data.origins = $('#f_origins').val();
    	data.keywords = $('#f_keywords').val();
    	data.firstpublish_from = $('#f_firstpublish_from').val();
    	data.firstpublish_to = $('#f_firstpublish_to').val();
    	data.lastpublish_from = $('#f_lastpublish_from').val();
    	data.lastpublish_to = $('#f_lastpublish_to').val();
    	data.engagedusers_min = $('#f_engagedusers_min').text();
    	data.engagedusers_max = $('#f_engagedusers_max').text();
    	data.publics = $('#f_publics').val();
    	data.domains = $('#f_domains').val();
    	data.durationconduct = $('#f_durationconduct').val();
        data.gaiausers = $('#f_gaiausers').val();
    	data.collection = getActiveCollection();
    	return data;
    }
    
    function getActiveCollection() {
    	return $('.collection.active').data('id');
    }
    
    function resetform(){
    	$('#f_search').val('');
    	$('#f_keywords').val('');
    	$('#f_firstpublish_from').val('');
    	$('#f_firstpublish_to').val('');
    	$('#f_lastpublish_from').val('');
    	$('#f_lastpublish_to').val('');
    	$('#f_engagedusers').slider("values", [$("#f_engagedusers").slider("option", "min"),$("#f_engagedusers").slider("option", "max")]);
    	$('#f_engagedusers_min').text($("#f_engagedusers").slider("option", "min"));
    	$('#f_engagedusers_max').text($("#f_engagedusers").slider("option", "max"));

    	$('#d_origins').find('.del').each(function (index, el) {$(el).trigger('click');});
    	$('#d_origins .dropdown-search input').val('');
    	$('#d_publics').find('.del').each(function (index, el) {$(el).trigger('click');});
    	$('#d_publics .dropdown-search input').val('');
    	$('#d_domains').find('.del').each(function (index, el) {$(el).trigger('click');});
    	$('#d_domains .dropdown-search input').val('');
    	$('#d_durationconduct').find('.del').each(function (index, el) {$(el).trigger('click');});
    	$('#d_durationconduct .dropdown-search input').val('');
        $('#d_gaiausers').find('.del').each(function (index, el) {$(el).trigger('click');});
        $('#d_gaiausers .dropdown-search input').val('');
    }
    
    function showpreview() {
        $("#previewtable").jtable({
            title: str.preview_header_title,
            paging: true,
            pageSize: 10,
            pageSizes: [10,20,30],
            selecting: false,
            multiselect: false,
            selectingCheckboxes: false,
            sorting: true,
            defaultSorting: "name ASC",
            jqueryuiTheme: true,
            defaultDateFormat: "dd/mm/yy",
            gotoPageArea: "none",
            selectOnRowClick: false,
            actions: {
                listAction: function (postData, jtParams) {
                    return $.Deferred(function ($dfd) {
                        var postdata = formdata();
						postdata.si = (jtParams.jtStartIndex==undefined?0:jtParams.jtStartIndex);
						postdata.ps = (jtParams.jtPageSize==undefined?0:jtParams.jtPageSize);
			            postdata.so = (jtParams.jtSorting==undefined?'':jtParams.jtSorting);

						var promises = ajax.call([
				            { methodname: 'centraladminstats_offerfull_getpreview', args: postdata },
				        ])
				
				        promises[0].done((response) => {
			    		    $dfd.resolve(JSON.parse(response.data));
				        });
                    });
                }
            },
            fields: {
                name: {
                    title: str.preview_header_name,
                    width: "40%",
                    create: false,
                    edit: false,
                    list: true
                },
                lastpublish: {
                    title: str.preview_header_lastpublish,
                    width: "20%",
                    create: false,
                    edit: false,
                    list: true,
					type: 'date'
                },
                keywords: {
                    title: str.preview_header_keywords,
                    width: "40%",
                    create: false,
                    edit: false,
                    list: true,
					sorting: false
                }
            }
        });
		
		$('#previewtable').jtable('load');
		
		$("#preview").modal("show");
    }
    
    function exportdata() {
    	// download CSV
    	
    	var promises = ajax.call([
            { methodname: 'centraladminstats_offerfull_getexport', args: formdata() },
        ])

        promises[0].done((response) => {
	    	var a = document.createElement('a');
    		if (window.URL && window.Blob && ('download' in a) && window.atob) {
    		    var blob = base64ToBlob(response.file, 'text/octet-stream');
    		    var url = window.URL.createObjectURL(blob);
    		    a.href = url;
    		    a.download = 'magistere_offre_de_formation.csv';
    		    a.click();
    		    window.URL.revokeObjectURL(url);
    		}
        });
    }
    
    function loadstats() {
    	// load graphs et recup la liste des fonction de refresh
    	
    	var promises = ajax.call([
            { methodname: 'centraladminstats_offerfull_getcollection', args: formdata() },
        ])

        promises[0].done((response) => {
            $('#collection').html(response.html);
            
            collection_amd = 'centraladmincollec_'+getActiveCollection()+'/main';
            
            require([collection_amd], (collectionamd) => {
        		collectionamd.init();
                
                refreshstats();
            	is_collection_loaded = true;
            });
        });
    	
    	
    }
    
    function refreshstats() {
    	require([collection_amd], (collectionamd) => {
    		collectionamd.refresh(formdata());
        });
    	
    }
    
	function base64ToBlob(base64, mimetype, slicesize) {
	    if (!window.atob || !window.Uint8Array) {
	        return null;
	    }
	    mimetype = mimetype || '';
	    slicesize = slicesize || 512;
	    var bytechars = atob(base64);
	    var bytearrays = [];
	    for (var offset = 0; offset < bytechars.length; offset += slicesize) {
	        var slice = bytechars.slice(offset, offset + slicesize);
	        var bytenums = new Array(slice.length);
	        for (var i = 0; i < slice.length; i++) {
	            bytenums[i] = slice.charCodeAt(i);
	        }
	        var bytearray = new Uint8Array(bytenums);
	        bytearrays[bytearrays.length] = bytearray;
	    }
	    return new Blob(bytearrays, {type: mimetype});
	};
	
	return {
    	init : function(str) {
            init(str);
        }
    };
});
