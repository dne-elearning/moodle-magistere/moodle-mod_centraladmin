<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/centraladmin/centraladminstats_plugin.php');
require_once($CFG->dirroot.'/mod/centraladmin/data/offer/offer.php');

class centraladminstats_offerview extends centraladminstats_plugin {
    
    public function get_name() {
        return get_string('pluginname', 'centraladminstats_offerview');
    }

    public function get_content() {
        global $DB, $CFG, $PAGE;

        $this->check_permission();
        
        $highchartsurl = new moodle_url('/mod/centraladmin/js/highcharts/highcharts.js');
        $highchart_nodata_surl = new moodle_url('/mod/centraladmin/js/highcharts/modules/no-data-to-display.js');

        $PAGE->requires->js_call_amd('centraladminstats_offerview/courses_by_domain', 'init');
        $PAGE->requires->js_call_amd('centraladminstats_offerview/courses_by_origin', 'init');
        $PAGE->requires->js_call_amd('centraladminstats_offerview/offer_evolution', 'init');
        $PAGE->requires->js_call_amd('centraladminstats_offerview/achievement_evolution', 'init');

        $where = '';
        if (!centraladmin_permission_isadmin()) {
            $instances = centraladmin_permission_get_instances();
            $where = ' AND cs.id IN('.implode(',', $instances).')';
        }

        $query = 
        'SELECT (SELECT count(co.id) FROM {'.centraladmindata_offer::$tablename.'} co INNER JOIN mdl_centraladmin_slave cs ON co.slaveid = cs.id WHERE cs.disabled = 0 AND cs.deleted = 0'.$where.') as nb_published,
(SELECT count(co.id) FROM {'.centraladmindata_offer::$tablename.'} co INNER JOIN mdl_centraladmin_slave cs ON co.slaveid = cs.id WHERE cs.disabled = 0 AND cs.deleted = 0 AND lastpublish > ?'.$where.') as nb_lastpublished,
(SELECT count(co.id) FROM {'.centraladmindata_offer::$tablename.'} co INNER JOIN mdl_centraladmin_slave cs ON co.slaveid = cs.id WHERE cs.disabled = 0 AND cs.deleted = 0 AND firstpublish > ?'.$where.') as nb_firstpublish';

        
        $currentYear = $this->getCurrentYear();
        $beginYearTS = mktime(0, 0, 0, 9, 1, $currentYear);

        $res = $DB->get_record_sql($query, array($beginYearTS, $beginYearTS)); 
        $nb_published = $res->nb_published;
        $nb_lastpublished = $res->nb_lastpublished;
        $nb_firstpublish = $res->nb_firstpublish;


        $backgroundColor1 = '#5770BE';
        $color1 = 'white';
        $backgroundColor2 = '#5770BE7F';
        $color2 = 'black';
        $backgroundColor3 = '#484D7A';
        $color3 = 'white';

        $html = '<div class=centraladminstats>';
        $html .= '<h1 class="centraladmin_title">'.$this->get_name().'</h1>';

        $html .= '<hr/>';

        $html .= '<div class="centraladminstats_offerview">';

        // timecode part
        $html .= self::createTimecode();

        // doc part 
        $html .= self::docLink();

        // tiles part
        $html .= '<div class="centraladminstats_offerview_tiles">';
        $html .= $this->createTile(get_string('totalpublishedseptember', 'centraladminstats_offerview'), $nb_firstpublish, $backgroundColor3, $color3);
        $html .= $this->createTile(get_string('totaleditedseptember', 'centraladminstats_offerview'), $nb_lastpublished, $backgroundColor2, $color2);
        $html .= $this->createTile(get_string('totalpublished', 'centraladminstats_offerview'), $nb_published, $backgroundColor1, $color1);
        $html .= '</div>';

        // button part
        $url = new moodle_url($CFG->wwwroot.'/mod/centraladmin/stats/offerview/export.php');
        $html .= '<div class="centraladminstats_offerview_export">';
        $html .= '<a href="'.$url.'" class="btn btn-lg btn-default">'.get_string('exporttocsv', 'centraladminstats_offerview').'</a>';
        $html .= '</div>';

        $html .= '</div>';

        $html .= '<hr/>';
        // charts part
        $html .= '<div class="chartgroup">';
        $html .= '<div class="chartrow">';
        $html .= '<div class="chartcontainer" id="courses_by_domain"> </div>';
        $html .= '</div>';   
        $html .= '<div class="chartrow">';
        $html .= '<div class="chartcontainer" id="courses_by_origin"> </div>';
        $html .= '<div class="chartcontainer" id="offer_evolution"> </div>';
        $html .= '</div>'; 
        $html .= '</div>';
        $html .= '</div>';

        $html .= "<script src='".$highchartsurl."'></script>";
        $html .= "<script src='".$highchart_nodata_surl."'></script>";
        return $html;
    }

    private function createTile($desc, $value, $backgroundColor='white', $color='black') {
        $html = '';
        $html .= '<div class="centraladmin_tile" style="background-color:'.$backgroundColor.'; color:'.$color.' ">';
        $html .= '<h1>';
        $html .= $desc;
        $html .= '</h1>';
        $html .= '<span>';
        $html .= $value;
        $html .= '</span>';
        $html .= '</div>';
        return $html;
    }

    private static function createTimecode() {
        $html = '<div class="timecode_container" id="id_timecode">';
        $html .= get_string('lastupdate', 'centraladmindata_offer').' : '.centraladmindata_offer::get_lastupdate_timecode_str();
        $html .= '</div>';
        return $html;
    }

    public static function docLink() {
        $link = get_config('centraladminstats_offerview', 'documentationlink');
        $html = '<div class="doclink_container" id="id_docklink">';
        $html .= html_writer::link($link, get_string('documentation', 'centraladminstats_offerview'));
        $html .= '</div>';
        return $html;
    }

    /**
     * Get current school year
     */
    private function getCurrentYear() {
        $year = idate('Y');
        if (time() < mktime(0, null, null, 9, 1)) {
            $year--; 
        } 
        return $year;
    }
}