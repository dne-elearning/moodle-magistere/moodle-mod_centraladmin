define(["core/ajax", "mod_centraladmin/colors"],function(ajax, colors) {
    function init() { 

        var divId = 'courses_by_domain';

        var promises = ajax.call([
            { methodname: 'centraladminstats_offerview_domains', args: {} },
        ])

        promises[0].done(function(response) {
            // prepare the series using the response

            var series = [{
                name: 'Domaines professionnels',
                data : response.map((val) => {
                    var domaine = (val.domaine === '') ? 'Non défini' : val.domaine;
                    return ({domaine: domaine, y : val.nbcourses})
                }),
            }];

            Highcharts.chart(divId, {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Répartition des formations par domaines professionnels'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.domaine}</b>: {point.percentage:.2f}% ({point.y})'
                        },
                        tooltip: {
                            headerFormat: '',
                            pointFormat: '<b>{point.domaine}</b> : {point.percentage:.2f}% ({point.y})'
                        },
                    }
                },
                series: series,
                noData: {
                    style: {
                        fontWeight: 'bold',
                        fontSize: '15px',
                        color: '#303030'
                    }
                },
                lang: {
                    noData: "Aucune donnée à afficher"
                },
                credits: {
                    enabled: false,
                },
                colors: colors.greenbluegradient,
            });

        }).fail(function(ex) {
            document.getElementById(divId).innerHTML = '<p>Une erreur est survenue lors de la récupération des données du graphiques "Répartition des formations par domaines professionnels"</p>';
            console.error(ex);
        });

    }

    return {
    	init : function() {
            init();
        }
    };
});