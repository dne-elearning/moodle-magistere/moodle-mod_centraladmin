define(['jquery', "core/ajax", 'centraladminstats_offerfull/jquery-dropdown'], function($, ajax) {
    
    function init(strUnselectall, strSelectall, strSearch) {

        this.strunselectall = strUnselectall;
        this.strselectall = strSelectall;
        this.strsearch = strSearch;
        
        const dropdown = multiSelectDropdown('.dropdown-mul-1');
        //dropdown.changeStatus('readonly');
        initBtnSelectAll('view');
        initBtnSelectAll('instance');
        $('#b_view_selectall').on('click', function(e){
            select_allornone('view');
        });
        $('#b_instance_selectall').on('click', function(e){
            select_allornone('instance');
        });
        $("input[id^='switchview_']").on('change', function(){
            initBtnSelectAll('view');
        });
        $("input[id^='switchinstance_']").on('change', function(){
            initBtnSelectAll('instance');
        });
        $('#b_reset').on('click', function(){
            $('.dropdown-chose-list').find('.del').each(function (index, el) {$(el).trigger('click');});
            $('#usersearch').val('');
            setTimeout(() => disabledForm(), 100);
        });
        disabledForm();

    }

    const initBtnSelectAll = (type) => {
        $('#b_' + type + '_selectall').html(isAllChecked(type)?this.strunselectall:this.strselectall);
    }

    const getSelector = (type) => type=='view'?$("input[id^='switchview_']"):$("input[id^='switchinstance_']");

    const isAllChecked = (type) => {

        let allChecked = true;
        const selector = getSelector(type);
        selector.each(function(item) {
            if ($(this).is(':not(:checked)')){
                allChecked = false;
            }
        });
        return allChecked;
    }

    const select_allornone = (type) => {

        const newState = !isAllChecked(type);
        const selector = getSelector(type);
        selector.each(function(item) {
            $(this).prop('checked', newState);
        }) ;
        initBtnSelectAll(type);
    }

    const disabledForm = () => {
        $('.users_form').css('opacity', '0.2');
        $('.users_buttons').css('opacity', '0.2');
        $('.users_form').css('pointer-events', 'none');
        $('.permission_form').hide();
		$('.permission_buttons').hide();
        $('#b_submit').prop('disabled', true);
    }

    const enabledForm = () => {
        $('.users_form').css('opacity', '1');
        $('.users_buttons').css('opacity', '1');
        $('.users_form').css('pointer-events', 'all');
		$('.permission_form').show();
		$('.permission_buttons').show();
        $('#b_submit').prop('disabled', false);
    }

    const displayPermissions = (permissions) => {

        let aca = '';
        let foundSlave = [];
        if (!permissions.slaves){
            return;
        }
        $("input[id^='switchinstance_']").each(function(item) {
            aca = $(this).attr("id").replace('switchinstance_', '');
            foundSlave = permissions.slaves.filter(slave => slave===aca);
            $(this).prop('checked', foundSlave.length>0);
        });
        $("input[id^='switchview_']").each(function(item) {
            view = $(this).attr("id").replace('switchview_', '');
            foundView = permissions.views.filter(v => view===v);
            $(this).prop('checked', foundView.length>0);
        });
        initBtnSelectAll('view');
        initBtnSelectAll('instance');
    }

    const multiSelectDropdown = (selector) => {
        return $(selector).dropdown({
            // read only
            readOnly: false,
            // min count
            minCount: 0,
            // error message
            minCountErrorMessage: '',
            // the maximum number of options allowed to be selected
            limitCount: Infinity,
            // error message
            limitCountErrorMessage: '',
            // search field
            input: `<input type="text" maxLength="20" name="usersearch" id="usersearch" placeholder="${this.strsearch}">`,
            // dynamic data here
            data: [],
            // is search able?
            searchable: true,
            // when there's no result
            searchNoData: '<li style="color:#ddd">Pas de resultat</li>',
            // callback
            choice: function () {
				var selectedusers = $('#users').find(":selected").map(function(i, el) {return $(el).val();}).get();
                $('#users_selected').val(selectedusers.join(',')); 
                $('.dropdown-single,.dropdown-multiple,.dropdown-multiple-label').removeClass('active');
                if (selectedusers.length == 1) {
                    const userid = selectedusers[0];
                    disabledForm();
                    var promises = ajax.call([
                        { methodname: 'mod_centraladmin_permission', args: {userid: userid} },
                    ])
            
                    promises[0].done(permissions => {
                        displayPermissions({
                            slaves: permissions.slaves.split(","),
                            views: permissions.views.split(",")
                        });
                        enabledForm();
                    })
                    .fail(err => {
                        enabledForm();
                    });
                }
                else if (selectedusers.length == 0) {
                    disabledForm();
                }
            },
            multipleMode: 'label',
            // custom props
            extendProps: []
        }).data('dropdown');
    }

    return {
    	init : function(strUnselectall, strSelectall, strSearch) {
            init(strUnselectall, strSelectall, strSearch);
        }
    };
});