<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/centraladmin/centraladminconf_plugin.php');
require_once($CFG->dirroot.'/mod/centraladmin/SlaveQuery.php');

class centraladminconf_role extends centraladminconf_plugin {
    
    const TABLE_ROLE_CACHE = 'centraladminconf_role_cache';
    const TABLE_ROLE_TODO = 'centraladminconf_role_todo';
    const TABLE_ROLE_LOG = 'centraladminconf_role_log';
    const TABLE_ROLE_ARCH = 'centraladminconf_role_arch';
    const TABLE_SLAVE = 'centraladmin_slave';

    const DEFAULT_CACHE_REFRESH_DELAY = 86400; // 1 day
    const DEFAULT_TODO_LIFETIME = 172800; // 2 days
    
    const CACHEUPDATE_RUNNING = 2;
    const CACHEUPDATE_TODO = 3;
    const CACHEUPDATE_NONE = 4;
    
    const CONFIG_CACHEUPDATE_LAST = 'lastcacheupdate';
    const CONFIG_CACHEUPDATE_RUNNING = 'runningcacheupdate';
    const CONFIG_CACHEUPDATE_DELAY = 'cacherefreshdelay';
    
    private $arch_cache = array();

    public function __construct() {
        /*
        $count = 43;
        $start = microtime(true);
        for($i=0;$i<$count;$i++) {
            $time2 = microtime(true);
            $this->compare_role_cache();
            echo '####TOTAL='.(microtime(true)-$time2)."####\n\n\n";
        }
        
        echo 'average='.((microtime(true)-$start)/$count)."\n";
        die;
        */
    }
    
    public function get_cache_status(){
        if (empty($this->get_cache_running())){
            return ($this->get_cache_last_update() < time()-$this->get_cache_refresh_delay())?self::CACHEUPDATE_TODO:self::CACHEUPDATE_NONE;
        }else{
            return self::CACHEUPDATE_RUNNING;
        }
    }
    
    public function refresh_cache(){
        $s = $this->get_cache_status();
        if ($s == self::CACHEUPDATE_TODO) {
            $this->compare_role_cache();
        }
    }
    
    private function compare_role_cache() {
        global $DB;
        
        $this->set_cache_running(true);

        $slaves = SlaveQuery::get_all_slaves();
        
        foreach($slaves as $slave) {
            
            $permissions = $DB->get_records(self::TABLE_ROLE_CACHE, array('slaveid'=>$slave->id), '', 'CONCAT(roleid,"|",capability) AS uid,permission');
            
            $perms = array();
            foreach($permissions AS $permission){
                $perms[$permission->uid] = $permission->permission;
            }

            $res = SlaveQuery::compare_permissions($slave, $perms);
            
            if (count($res->insert)>0) {
                $insert_buffer = array();
                foreach ($res->insert AS $i) {
                    $i->slaveid=$slave->id;
                    $insert_buffer[] = $i;
                }
                $DB->insert_records(self::TABLE_ROLE_CACHE, $insert_buffer);
            }
            
            if (count($res->update)>0) {
                foreach ($res->update AS $u) {
                    list($roleid,$capability) = explode('|',$u->uid);
                    $rc = $DB->get_record(self::TABLE_ROLE_CACHE, array('slaveid'=>$slave->id,'roleid'=>$roleid,'capability'=>$capability), 'id');
                    $u->id = $rc->id;
                    $DB->update_record(self::TABLE_ROLE_CACHE, $u);
                }
            }
            
            if (count($res->delete)>0) {
                foreach ($res->delete AS $key=>$d) {
                    list($roleid,$capability) = explode('|',$key);
                    $DB->delete_records(self::TABLE_ROLE_CACHE,array('slaveid'=>$slave->id,'roleid'=>$roleid,'capability'=>$capability));
                }
            }
        }
        $this->set_cache_last_update(time());
        $this->set_cache_running(false);
    }

    public function get_name() {
        return get_string('pluginname', 'centraladminconf_role');
    }

    public function get_permissions($roleshortname, $instances, $capabilities, $diffinstance, $diffarchetype, $si = null, $ps = null, $so = null, $selected = null, $readOnly = true, $exportMode = false) {
        global $DB, $USER;

        $order = '';
        if (!empty($so)){
            $order = ' ORDER BY '.$so;
        }
        $limit = '';
        if (isset($si) && isset($ps)) {
            $limit = ' LIMIT '.$si.','.$ps;
        }

        $params = array('roleshortname'=>$roleshortname);
        $extraJoin = '';
        $whereClause = '';

        $paramsInstance = array();
        $whereClauseInstance = '';
        
        if ($instances && count($instances) > 0) {
            list($instancesInSql, $instancesInParams) = $DB->get_in_or_equal($instances, SQL_PARAMS_NAMED, 'slaveid');
            $whereClauseInstance = ' AND slaveid '.$instancesInSql; 
            $whereClause .= $whereClauseInstance;
            $paramsInstance = $instancesInParams;
            $params = array_merge($params, $paramsInstance);
        }

        if ($capabilities && count($capabilities) > 0) {
            list($capabilitiesInSql, $capabilitiesInParams) = $DB->get_in_or_equal($capabilities, SQL_PARAMS_NAMED, 'capabilities');
            $whereClause .= ' AND crc.capability '.$capabilitiesInSql;
            $params = array_merge($params, $capabilitiesInParams);
        }

        if ($diffinstance) {
            $subWhereClause = '';
            if ($instances && count($instances) > 0) {
                list($instancesInSql, $instancesInParams) = $DB->get_in_or_equal($instances, SQL_PARAMS_NAMED, 'slaveid2');
                $subWhereClause .= ' WHERE crc2.slaveid '.$instancesInSql;
                $params = array_merge($params, $instancesInParams);
            }

            $extraJoin .= ' INNER JOIN (
                SELECT crc2.roleshortname, crc2.capability,  count(DISTINCT crc2.permission) as nbdiff
                FROM {'.self::TABLE_ROLE_CACHE.'} crc2 '.$subWhereClause.'
                GROUP BY crc2.roleshortname, crc2.capability
            ) crcdiff ON crcdiff.roleshortname = crc.roleshortname AND crcdiff.capability = crc.capability ';

            $whereClause .= ' AND crcdiff.nbdiff > 1';
        }

        if ($diffarchetype) {
            $whereClause .= ' AND crc.permission != cra.permission';
        }

        $sql = 'SELECT SQL_CALC_FOUND_ROWS crc.capability, cra.permission as archetype
FROM {'.self::TABLE_ROLE_CACHE.'} crc 
INNER JOIN {centraladmin_slave} cs ON crc.slaveid = cs.id 
LEFT JOIN {'.self::TABLE_ROLE_ARCH.'} cra ON (cra.archetype = crc.archetype AND cra.capability = crc.capability)
'.$extraJoin.'
WHERE crc.roleshortname = :roleshortname '.$whereClause.' 
AND cs.disabled = 0 AND cs.deleted = 0 
GROUP BY crc.capability 
'.$order.$limit;

        $recordsCapa = $DB->get_records_sql($sql, $params);
        $recordCount = $DB->get_record_sql("SELECT FOUND_ROWS() AS found_rows")->found_rows;
        
        $capaNamesArray = self::getCapaNamesArray(); 
        $capaClassArray = self::getCapaClassArray();  

        $results = array();

        $selectParam = array();
        if ($readOnly) {
            $selectParam = array('disabled' => true);
        }

        foreach ($recordsCapa AS $recordCapa) {
            $capaNameArray = self::getCapaNamesArray();
            // get the info for all the aca
            $sql = 'SELECT crc.*, cs.name as slavename
FROM {'.self::TABLE_ROLE_CACHE.'} crc 
INNER JOIN {centraladmin_slave} cs ON crc.slaveid = cs.id 
WHERE crc.capability = :capability AND crc.roleshortname = :roleshortname '.$whereClauseInstance; 
            $slavesCapaInfo = $DB->get_records_sql($sql, array_merge(array('capability' => $recordCapa->capability, 'roleshortname' => $roleshortname), $paramsInstance));

            foreach ($slavesCapaInfo AS $slaveCapa) {
                if (!isset($results[$recordCapa->capability])) {
                    $result = new stdClass();
                    $result->roleshortname = $slaveCapa->roleshortname;
                    $result->capability = $recordCapa->capability;
                    $result->archetype = $capaNamesArray[$recordCapa->archetype];

                    if (!$exportMode) {
                        $result->changeall = html_writer::select($capaNameArray, 'changeall_'.$slaveCapa->id, '', array('' => 'choosedots'), $selectParam);
                    }
                    $results[$result->capability] = $result; 
                } 

                $slaveInfo = new StdClass();
                $slaveInfo->slaveid = $slaveCapa->slaveid;
                if (!$exportMode) {
                    $slaveInfo->permission = html_writer::select($capaNameArray, 'changepermission_'.$slaveCapa->id.'_'.$slaveCapa->slaveid, $slaveCapa->permission, array('' => 'choosedots'), $selectParam);
                } else {
                    $slaveInfo->permission = $capaNameArray[$slaveCapa->permission];
                }
                $slaveInfo->classes = $capaClassArray[$slaveCapa->permission];
                $slaveInfo->timemodified = $slaveCapa->timemodified;
                $slaveInfo->slavename = $slaveCapa->slavename;

                if ($role_todo_data = $DB->get_record(self::TABLE_ROLE_TODO, array('slaveid' => $slaveCapa->slaveid,
                    'roleid' => $slaveCapa->roleid,
                    'userid' => $USER->id,
                    'capability' => $recordCapa->capability,
                    'permissionold' => $slaveCapa->permission))) {

                    $slaveInfo->classes = $capaClassArray[$role_todo_data->permission]. ' select-changing';
                    
                    if(!$exportMode) {
                        $slaveInfo->permission = html_writer::select(self::getCapaNamesArray(), 'changepermission_'.$slaveCapa->id.'_'.$slaveCapa->slaveid, $role_todo_data->permission, array('' => 'choosedots'), $selectParam);
                    } else {
                        $slaveInfo->permission = $capaNameArray[$slaveCapa->permission];
                    }
                }


                if ($selected != null && $selected != "") {
                    $role_todo = new stdClass();
                    $role_todo->slaveid = $slaveCapa->slaveid;
                    $role_todo->roleid = $slaveCapa->roleid;
                    $role_todo->userid = $USER->id;
                    $role_todo->capability = $recordCapa->capability;
                    $role_todo->permissionold = $slaveCapa->permission;
                    $role_todo->permission = $selected;

                    if ($role_todo_data) {
                        $role_todo_data->permission = $selected;

                        if ($role_todo->permissionold == $role_todo_data->permission) {
                            $slaveInfo->classes = $capaClassArray[$selected];
                            $this->delete_role_todo($role_todo_data);
                        } else {
                            $slaveInfo->classes = $capaClassArray[$selected]. ' select-changing';
                            $this->update_role_todo($role_todo_data);
                        }

                    } else {
                        if ($role_todo->permissionold != $role_todo->permission) {
                            $slaveInfo->classes = $capaClassArray[$selected]. ' select-changing';
                            $this->add_role_todo($role_todo);
                        }
                    }
                }

                $results[$recordCapa->capability]->instances[$slaveCapa->slaveid] = $slaveInfo;
            }    
        }
        
        return array('result' => $results, 'count' => $recordCount);
    }

    private function add_role_todo($data) {
        global $DB;

        $data->timecreated = time();

        try {
            return $DB->insert_record(self::TABLE_ROLE_TODO, $data);
        } catch (moodle_exception $e) {
            debugging('Error retrieving : role_todo cannot be added: ' .
                $e->getMessage(), DEBUG_DEVELOPER);
            return false;
        }
    }

    private function update_role_todo($data) {
        global $DB;

        $data->timecreated = time();

        try {
            return $DB->update_record(self::TABLE_ROLE_TODO, $data);
        } catch (moodle_exception $e) {
            debugging('Error retrieving : role_todo cannot be updated: ' .
                $e->getMessage(), DEBUG_DEVELOPER);
            return false;
        }
    }

    private function delete_role_todo($data) {
        global $DB;

        try {
            return $DB->delete_records(self::TABLE_ROLE_TODO, array('id' => $data->id));
        } catch (moodle_exception $e) {
            debugging('Error retrieving : role_todo cannot be deleted: ' .
                $e->getMessage(), DEBUG_DEVELOPER);
            return false;
        }
    }

    private function fetch_archetypes_permissions() {
        // to do : don't hardcode the instance
        $slavename = 'dgesco';
        $slave = SlaveQuery::get_slave_by_name($slavename);
        if ($slave === false){
            return false;
        }

        $query = new stdClass();
        $query->wsfunction = 'local_centraladmin_agent_get_archetype_permissions';
        $res = SlaveQuery::api_query($slave, $query);
        return SlaveQuery::decode($res->permissions);
    }
    
    private function build_arch_cache(){
        global $DB;
        $arch = $DB->get_records(self::TABLE_ROLE_ARCH);
        
        foreach ($arch AS $a) {
            if (!isset($this->arch_cache[$a->archetype])){$a->arch_cache[$a->archetype] = array();}
            if (!isset($this->arch_cache[$a->archetype][$a->capability])){$a->arch_cache[$a->archetype][$a->capability] = array();}
            $this->arch_cache[$a->archetype][$a->capability]['id'] = $a->id;
            $this->arch_cache[$a->archetype][$a->capability]['permission'] = $a->permission;
        }
    }

    public function update_archetype_cache() {
        global $DB;
        if (count($this->arch_cache) == 0){
            $this->build_arch_cache();
        }
        $perms = $this->fetch_archetypes_permissions();
        if (!$perms) {
            // todo : do something
            //throw new Error('')
        } else {

            $insert_buffer = array();
            foreach($perms as $archName => $capas) {
                foreach($capas as $capa => $perm) {
                    
                    if (isset($this->arch_cache[$archName][$capa]['permission']) && $this->arch_cache[$archName][$capa]['permission'] == $perm){
                        continue;
                    }
                    
                    if (isset($this->arch_cache[$archName][$capa]['permission'])) {
                        $this->arch_cache[$archName][$capa]['permission'] = $perm;
                        $arch = new stdClass();
                        $arch->id = $this->arch_cache[$archName][$capa]['id'];
                        $arch->permission = $perm;
                        
                        $DB->update_record(self::TABLE_ROLE_ARCH, $arch, true);
                    }else{
                        $arch = new stdClass();
                        $arch->archetype = $archName;
                        $arch->capability = $capa;
                        $arch->permission = $perm;
                        
                        $insert_buffer[] = $arch;
                    
                        if (!isset($this->arch_cache[$arch->archetype])){$this->arch_cache[$arch->archetype] = array();}
                        if (!isset($this->arch_cache[$arch->archetype][$arch->capability])){$this->arch_cache[$arch->archetype][$arch->capability] = array();}
                        $this->arch_cache[$arch->archetype][$arch->capability]['permission'] = $arch->permission;
                    }
                }
            }
            $DB->insert_records(self::TABLE_ROLE_ARCH, $insert_buffer);
        }
    }

    public function fetch_role_permissions($slave) {
        $sql = 'SELECT 
CONCAT(c.id,"-",r.id) as uid,
c.name AS capability, 
r.id AS roleid,
rc.permission AS permission,
rc.timemodified AS timemodified,
r.name AS rolename,
r.shortname AS roleshortname,
r.archetype 
FROM {capabilities} c 
INNER JOIN {role} r
LEFT JOIN {role_capabilities} rc ON c.name = rc.capability AND rc.roleid = r.id AND rc.contextid = 1';

        return SlaveQuery::get_records_sql($slave, $sql);
    }
    
    public function get_cache_refresh_delay(){
        $cacheRefreshDelay = get_config('centraladminconf_role',self::CONFIG_CACHEUPDATE_DELAY);
        return empty($cacheRefreshDelay)?self::DEFAULT_CACHE_REFRESH_DELAY:$cacheRefreshDelay;
    }
    
    public function get_cache_last_update(){
        $lastupdate = get_config('centraladminconf_role',self::CONFIG_CACHEUPDATE_LAST);
        return empty($lastupdate)?0:$lastupdate;
    }
    
    public function get_cache_running(){
        return get_config('centraladminconf_role', self::CONFIG_CACHEUPDATE_RUNNING);
    }
    
    public function set_cache_last_update($lastupdate){
        return set_config(self::CONFIG_CACHEUPDATE_LAST,$lastupdate,'centraladminconf_role');
    }
    
    public function set_cache_running($running){
        return set_config(self::CONFIG_CACHEUPDATE_RUNNING,$running,'centraladminconf_role');
    }

    public function cache_need_refresh() {
        return $this->get_cache_last_update() < time()-$this->get_cache_refresh_delay();
    }
/*
    public function update_role_cache() {
        return false;
        global $DB;
        
        $time2 = microtime(true);
        $slaves = $this->getInstances();
        echo '####2.2='.(microtime(true)-$time2)."####\n";
        $timecache = time();

        $time2 = microtime(true);
        $DB->delete_records('centraladminconf_role_cache');
        echo '####2.3='.(microtime(true)-$time2)."####\n";
        
        $time2 = microtime(true);
        foreach($slaves as $slavename) {
            $slave = SlaveQuery::get_slave_by_name($slavename);
            $records = $this->fetch_role_permissions($slave);
            
            echo '####2.4='.(microtime(true)-$time2)."####\n";
            $time2 = microtime(true);

            $role_caches_arrays = array();
            foreach($records as $record) {

                //$archetype = $DB->get_record('centraladminconf_role_arch', array('archetype' => $record->archetype, 'capability' => $record->capability));
                $archetype = null;
                if (isset($this->arch_cache[$record->archetype][$record->capability])){
                    $archetype = $this->arch_cache[$record->archetype][$record->capability]['id'];
                }
                
                $role_cache = new stdClass();
                $role_cache->slaveid = $slave->id;
                $role_cache->roleid = $record->roleid;
                $role_cache->rolename = $record->rolename;
                $role_cache->roleshortname = $record->roleshortname;
                $role_cache->archetype = $archetype; // ? $archetype->id : null;
                $role_cache->capability = $record->capability;
                $role_cache->permission = isset($record->permission) ? $record->permission : 0;
                $role_cache->timemodified = isset($record->timemodified) ? $record->timemodified : 0;
                $role_cache->timecache = $timecache;
 
                $role_caches_arrays[] = $role_cache;
            }
            $DB->insert_records('centraladminconf_role_cache', $role_caches_arrays);
        }
        echo '####2.5='.(microtime(true)-$time2)."####\n";
    }
*/
    public function getRolesNames() {
        global $DB;
        $roles = $DB->get_records_sql('SELECT rolename, roleshortname FROM {'.self::TABLE_ROLE_CACHE.'} GROUP BY rolename, roleshortname');
        $rolesnames = array();
        foreach($roles AS $role) {
            $rolesnames[$role->roleshortname] =  $role->rolename;
        }
        return $rolesnames;
    }

    public function getCapabilities() {
        global $DB;
        //$capabititiesRecords = $DB->get_records_sql('SELECT capability FROM {'.self::TABLE_ROLE_CACHE.'} GROUP BY capability');
        $capabititiesRecords = $DB->get_records(self::TABLE_ROLE_CACHE, null, '', 'DISTINCT capability');
        $capabilities = array();
        foreach($capabititiesRecords AS $record) {
            $capabilities[$record->capability] = $record->capability;
        }
        return $capabilities;
    }
    
    public function getInstances() {
        $slaves = SlaveQuery::get_all_slaves();
        $instances = array();
        foreach($slaves AS $slave) {
            $instances[$slave->id] = $slave->name;
        }
        return $instances;
    }

    /*
    * Propagate the permissions of the todo table of a specified user
    */
    public function propagatePermission($userid) {
        global $DB;


        $todos = $DB->get_records_sql(
'SELECT 
  crt.id AS id,
  crc.id AS cid,
  crt.slaveid AS slaveid,
  crt.roleid AS roleid,
  crc.roleshortname AS roleshortname,
  crt.userid AS userid,
  crt.capability AS capability,
  crt.permission AS permission,
  crt.permissionold AS permissionold
FROM {'.centraladminconf_role::TABLE_ROLE_TODO.'} crt 
INNER JOIN {'.centraladminconf_role::TABLE_ROLE_CACHE.'} crc ON crc.roleid = crt.roleid AND crc.slaveid = crt.slaveid AND crc.capability = crt.capability 
WHERE crt.userid = :userid', 
            array('userid' => $userid));

        $res = true;

        foreach($todos AS $todo) {
            $slave = SlaveQuery::get_slave($todo->slaveid);
            if ($slave === false){
                continue;
            }

            $query = new stdClass();
            $query->wsfunction = 'local_centraladmin_agent_update_permission';
            $query->capability = $todo->capability;
            $query->permission = $todo->permission;
            $query->roleshortname = $todo->roleshortname; 

            $resQuery = SlaveQuery::api_query($slave, $query);

            if (!$resQuery) {
                $res = false;
            } else {
                $this->log_permission($todo->slaveid, $todo->roleid, $todo->userid, $todo->capability, $todo->permissionold, $todo->permission);
                $DB->set_field(centraladminconf_role::TABLE_ROLE_CACHE, 'permission', $todo->permission, array('id' => $todo->cid));
                
                $DB->delete_records(centraladminconf_role::TABLE_ROLE_CACHE, array('id'=>$todo->id));
            } 
            
            // deal with error here, we can maybe stock all the error in an array ?
        }

        return $res;
    }

    public function log_permission($slaveid, $roleid, $userid, $capability, $permissionold, $permission) {
        global $DB;

        $rec = new stdClass();
        $rec->slaveid = $slaveid; 
        $rec->roleid = $roleid;
        $rec->userid = $userid;
        $rec->capability = $capability; 
        $rec->permissionold = $permissionold;
        $rec->permission = $permission;
        $rec->timecreated = time();

        $DB->insert_record(self::TABLE_ROLE_LOG, $rec);
    }
    
    public static function getCapaNamesArray() {
        return array(
            CAP_PROHIBIT => get_string('prohibit', 'role'),
            CAP_INHERIT => get_string('inherit', 'role'),
            CAP_PREVENT => get_string('prevent', 'role'),
            CAP_ALLOW => get_string('allow', 'role')
        );
    }

    public static function getCapaClassArray() {
        return array(
            CAP_PROHIBIT => 'prohibit',
            CAP_INHERIT => 'inherit',
            CAP_PREVENT => 'prevent',
            CAP_ALLOW => 'allow'
        );
    }
}