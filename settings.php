<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   mod_centraladmin
 * @copyright 2022 DNE - Ministere de l'Education Nationale
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$modcentraladmin = new admin_category('mod_centraladmin_manager', new lang_string('pluginname', 'mod_centraladmin'), $module->is_enabled() === false);
$ADMIN->add('modsettings', $modcentraladmin);

$settings = new admin_settingpage($section, get_string('settings', 'mod_centraladmin'), 'moodle/site:config', $module->is_enabled() === false);

if ($ADMIN->fulltree) {
    
    $settings->add(new admin_setting_configtext(
        'mod_centraladmin/ws_usagemonitor_username',
        new lang_string('ws_usagemonitor_username', 'mod_centraladmin'),
        new lang_string('ws_usagemonitor_username_desc', 'mod_centraladmin'),
        'username', 
        PARAM_TEXT
    ));

    $settings->add(new admin_setting_configtext(
        'mod_centraladmin/ws_usagemonitor_password',
        new lang_string('ws_usagemonitor_password', 'mod_centraladmin'),
        new lang_string('ws_usagemonitor_password_desc', 'mod_centraladmin'),
        'password', 
        PARAM_TEXT
    ));
    
}

$ADMIN->add('mod_centraladmin_manager', $settings);
$settings = null;

$ADMIN->add('mod_centraladmin_manager', new admin_externalpage('mod_centraladmin_slave_manager',
get_string('manageslaves', 'mod_centraladmin'), new moodle_url('/mod/centraladmin/slavemanager.php'), 'mod/centraladmin:manageslave'));
$ADMIN->add('mod_centraladmin_manager', new admin_externalpage('mod_centraladmin_permission_manager',
get_string('managepermission', 'mod_centraladmin'), new moodle_url('/mod/centraladmin/permissionmanager.php'), 'mod/centraladmin:managepermission'));


foreach (core_plugin_manager::instance()->get_plugins_of_type('centraladminconf') as $plugin) {
    $plugin->load_settings($ADMIN, 'mod_centraladmin_manager', $hassiteconfig);
}


