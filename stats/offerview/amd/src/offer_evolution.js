define(["core/ajax", "mod_centraladmin/colors"],function(ajax, colors) {
    function init() { 

        var divId = 'offer_evolution';

        var promises = ajax.call([
            { methodname: 'centraladminstats_offerview_evolution', args: {} },
        ]);

        promises[0].done(function(response) {
            // prepare the series using the response

            var created = {
                name: 'Parcours créés',
                data: [],
            }
            var updated = {
                name: 'Parcours mis à jour',
                data: [],
            }
            xAxisCategories = [];

            response.forEach(element => {
                xAxisCategories.push(element.year);
                created.data.push(element.nbcoursescreated);
                updated.data.push(element.nbcoursesupdated);
            });


            var series = [
                updated, 
                created,
            ];

            Highcharts.chart(divId, {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Évolution des formations en libre accès'
                },
                xAxis: {
                    categories: xAxisCategories,
                    title: {
                        text: 'Années civiles'
                    },
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Nombre de formations',
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: ( // theme
                                Highcharts.defaultOptions.title.style &&
                                Highcharts.defaultOptions.title.style.color
                            ) || 'gray'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                series: series,
                lang: {
                    noData: "Aucune donnée à afficher"
                },
                noData: {
                    style: {
                        fontWeight: 'bold',
                        fontSize: '15px',
                        color: '#303030'
                    }
                },
                credits: {
                    enabled: false,
                },
                colors: [colors.list.o[0], colors.list.p[0]],
            });

        }).fail(function(ex) {
            document.getElementById(divId).innerHTML = '<p>Une erreur est survenue lors de la récupération des données du graphiques "Évolution es formations en libre accès"</p>';
            console.error(ex);
        });

    }

    return {
    	init : function() {
            init();
        }
    };
});