<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/centraladmin/centraladmin_plugin.php');

abstract class centraladminconf_plugin extends centraladmin_plugin {

     /**
     * Return subtype name of the plugin.
     *
     * @return string
     */
    public function get_subtype() {
        return 'centraladminconf';
    }

    public abstract function get_name();


}