<?php

require_once($CFG->libdir.'/externallib.php');
require_once($CFG->dirroot.'/mod/centraladmin/data/offer/offer.php');
require_once($CFG->dirroot.'/mod/centraladmin/stats/offerfull/offerfull.php');

class centraladminstats_offerfull_external extends external_api {

    public const PLUGIN_NAME = 'centraladminstats_offerfull';
    
    public static function get_collection_parameters() {
        return new external_function_parameters(
            array(
                'search'   => new external_value(PARAM_RAW, ''),
                'origins'  => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'keywords' => new external_value(PARAM_RAW, ''),
                'firstpublish_from'=> new external_value(PARAM_RAW, ''),
                'firstpublish_to'  => new external_value(PARAM_RAW, ''),
                'lastpublish_from' => new external_value(PARAM_RAW, ''),
                'lastpublish_to'   => new external_value(PARAM_RAW, ''),
                'engagedusers_min' => new external_value(PARAM_INT, ''),
                'engagedusers_max' => new external_value(PARAM_INT, ''),
                'publics' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'domains' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'durationconduct' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'gaiausers' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'collection' => new external_value(PARAM_ALPHA, ''),
            )
            );
    }
    
    public static function get_collection_returns() {
        return new external_single_structure(
            array(
                'html' => new external_value(PARAM_RAW, '')
            )
            );
    }
    
    public static function get_collection($search, $origins, $keywords, $firstpublish_from, $firstpublish_to, $lastpublish_from, $lastpublish_to,
        $engagedusers_min, $engagedusers_max, $publics, $domains, $durationconduct, $gaiausers, $collection)
    {
        
        $offerfull = new centraladminstats_offerfull();
        
        return array('html'=>$offerfull->get_collection_content($collection));
        
    }
    
    
    
    public static function get_export_parameters() {
        return new external_function_parameters(
            array(
                'search'   => new external_value(PARAM_RAW, ''),
                'origins'  => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'keywords' => new external_value(PARAM_RAW, ''),
                'firstpublish_from'=> new external_value(PARAM_RAW, ''),
                'firstpublish_to'  => new external_value(PARAM_RAW, ''),
                'lastpublish_from' => new external_value(PARAM_RAW, ''),
                'lastpublish_to'   => new external_value(PARAM_RAW, ''),
                'engagedusers_min' => new external_value(PARAM_INT, ''),
                'engagedusers_max' => new external_value(PARAM_INT, ''),
                'publics' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'domains' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'durationconduct' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'gaiausers' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'collection' => new external_value(PARAM_ALPHA, ''),
            )
        );
    }
    
    public static function get_export_returns() {
        return new external_single_structure(
            array(
                'file' => new external_value(PARAM_RAW, '')
            )
            );
    }
    
    public static function get_export($search, $origins, $keywords, $firstpublish_from, $firstpublish_to, $lastpublish_from, $lastpublish_to,
        $engagedusers_min, $engagedusers_max, $publics, $domains, $durationconduct, $gaiausers, $collection)
    {
        global $DB;
        
        $offerfull = new centraladminstats_offerfull();
        $coursesids = $offerfull->search_coursesids($search, $origins, $keywords, $firstpublish_from, $firstpublish_to, $lastpublish_from, $lastpublish_to, $engagedusers_min, $engagedusers_max, $publics, $domains, $durationconduct, $gaiausers);
        
        
        $query =
        'SELECT 
co.name, 
co.timecreated, 
co.startdate,
co.lastpublish, 
co.duration, 
co.durationlocal, 
co.durationremote, 
co.duration as durationconduct, 
co.gaiausers, 
co.nbparticipants,
co.nbengagedparticipants, 
a.nbparticipants1d,
a.nbparticipants2d,
a.nbparticipantsndef,
a.nbparticipantspublic,
a.nbparticipantsprive,
a.nbparticipantsnoschool,
co.domaines,
cs.name as origine,
co.category,
co.publics,
co.keywords,
co.courseachievement,
co.courseachievements,
co.nbactivities,
co.nbreceptiveactivities,
co.nbactiveactivities,
co.nbcreativeactivities,
co.nbinteractiveactivities,
co.nbbadges,
co.nbdeliveredbadges,
co.nbbadgedparticipants,
co.authors,
co.courseurl
FROM mdl_'.centraladmindata_offer::$tablename.' co
INNER JOIN mdl_'.centraladmindata_offer::$attendance_tablename.' a ON co.courseid = a.courseid
INNER JOIN mdl_centraladmin_slave cs ON co.slaveid = cs.id
WHERE cs.disabled = 0 AND cs.deleted = 0 AND co.id IN('.implode(',',$coursesids).')
ORDER BY lastpublish DESC';
        $records = $DB->get_records_sql($query);
        
        $delimiter = get_string('listsep','core_langconfig');
        $dateformat = 'd/m/Y';
        
        $headers = array(
            get_string('csv_name', 'centraladminstats_offerfull'),
            get_string('csv_createdate', 'centraladminstats_offerfull'),
            get_string('csv_startdate', 'centraladminstats_offerfull'),
            get_string('csv_lastpublish', 'centraladminstats_offerfull'),
            get_string('csv_duration', 'centraladminstats_offerfull'),
            get_string('csv_durationlocal', 'centraladminstats_offerfull'),
            get_string('csv_durationremote', 'centraladminstats_offerfull'),
            get_string('csv_durationconduct', 'centraladminstats_offerfull'),
            get_string('csv_gaiausers', 'centraladminstats_offerfull'),
            get_string('csv_nbparticipants', 'centraladminstats_offerfull'),
            get_string('csv_nbengagedparticipants', 'centraladminstats_offerfull'),
            get_string('csv_1D', 'centraladminstats_offerfull'),
            get_string('csv_2D', 'centraladminstats_offerfull'),
            get_string('csv_other', 'centraladminstats_offerfull'),
            get_string('csv_public', 'centraladminstats_offerfull'),
            get_string('csv_private', 'centraladminstats_offerfull'),
            get_string('csv_withoutschool', 'centraladminstats_offerfull'),
            get_string('csv_domaines', 'centraladminstats_offerfull'),
            get_string('csv_origine', 'centraladminstats_offerfull'),
            get_string('csv_category', 'centraladminstats_offerfull'),
            get_string('csv_publics', 'centraladminstats_offerfull'),
            get_string('csv_keywords', 'centraladminstats_offerfull'),
            get_string('csv_courseachievement', 'centraladminstats_offerfull'),
            get_string('csv_courseachievements', 'centraladminstats_offerfull'),
            get_string('csv_nbactivities', 'centraladminstats_offerfull'),
            get_string('csv_nbreceptiveactivities', 'centraladminstats_offerfull'),
            get_string('csv_nbactiveactivities', 'centraladminstats_offerfull'),
            get_string('csv_nbcreativeactivities', 'centraladminstats_offerfull'),
            get_string('csv_nbinteractiveactivities', 'centraladminstats_offerfull'),
            get_string('csv_nbbadges', 'centraladminstats_offerfull'),
            get_string('csv_nbdeliveredbadges', 'centraladminstats_offerfull'),
            get_string('csv_nbbadgedparticipants', 'centraladminstats_offerfull'),
            get_string('csv_authors', 'centraladminstats_offerfull'),
            get_string('csv_courseurl', 'centraladminstats_offerfull'),
        );
        
        
        $filename = tempnam(sys_get_temp_dir(),'exp');
        $tfile = fopen($filename,'w+');
        
        fputs($tfile, (chr(0xEF) . chr(0xBB) . chr(0xBF)));
        
        fputcsv($tfile, $headers, $delimiter);
        
        foreach($records AS $record) {
            $record->duration = number_format(($record->duration / 60), 2, get_string('decsep','core_langconfig'), get_string('thousandssep','core_langconfig'));
            $record->durationlocal = number_format(($record->durationlocal / 60), 2, get_string('decsep','core_langconfig'), get_string('thousandssep','core_langconfig'));
            $record->durationremote = number_format(($record->durationremote / 60), 2, get_string('decsep','core_langconfig'), get_string('thousandssep','core_langconfig'));

            $record->courseachievement = $record->courseachievement
                ? get_string('yes', 'centraladminstats_offerfull')
                : get_string('no', 'centraladminstats_offerfull');
            $record->durationconduct = $record->durationlocal == 0
                ? get_string('duration_remote', 'centraladminstats_offerfull')
                : get_string('duration_hybrid', 'centraladminstats_offerfull');
            $record->gaiausers = $record->gaiausers > 0
                ? get_string('gaia_prescription', 'centraladminstats_offerfull')
                : get_string('gaia_selftraining', 'centraladminstats_offerfull');

            // Take only the three first keywords
            $record->keywords = implode(', ', array_slice(explode(",", $record->keywords), 0, 3));

            $record->timecreated = date($dateformat,$record->timecreated);
            $record->startdate = $record->startdate ? date($dateformat,$record->startdate) : null;
            $record->lastpublish = date($dateformat,$record->lastpublish);
            fputcsv($tfile, (array) $record, $delimiter);
        }
        
        fclose($tfile);
        $csvfile = base64_encode(file_get_contents($filename));
        
        return array('file'=>$csvfile);
    }
    
    
    
    public static function get_preview_parameters() {
        return new external_function_parameters(
            array(
                'search'   => new external_value(PARAM_RAW, ''),
                'origins'  => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'keywords' => new external_value(PARAM_RAW, ''),
                'firstpublish_from'=> new external_value(PARAM_RAW, ''),
                'firstpublish_to'  => new external_value(PARAM_RAW, ''),
                'lastpublish_from' => new external_value(PARAM_RAW, ''),
                'lastpublish_to'   => new external_value(PARAM_RAW, ''),
                'engagedusers_min' => new external_value(PARAM_INT, ''),
                'engagedusers_max' => new external_value(PARAM_INT, ''),
                'publics' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'domains' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'durationconduct' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'gaiausers' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'collection' => new external_value(PARAM_ALPHA, ''),
                'si' => new external_value(PARAM_INT, ''),
                'ps' => new external_value(PARAM_INT, ''),
                'so' => new external_value(PARAM_RAW, ''),
            )
        );
    }
    
    public static function get_preview_returns() {
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_RAW, '')
            )
        );
    }
    
    public static function get_preview($search, $origins, $keywords, $firstpublish_from, $firstpublish_to, $lastpublish_from, $lastpublish_to,
        $engagedusers_min, $engagedusers_max, $publics, $domains, $durationconduct, $gaiausers, $collection, $si, $ps, $so)
    {
        global $DB;
        
        $offerfull = new centraladminstats_offerfull();
        $coursesids = $offerfull->search_coursesids($search, $origins, $keywords, $firstpublish_from, $firstpublish_to, $lastpublish_from, $lastpublish_to, $engagedusers_min, $engagedusers_max, $publics, $domains, $durationconduct, $gaiausers);
        
        $order = '';
        if (!empty($so)){
            $order = ' ORDER BY '.$so;
        }
        
        $limit = ' LIMIT '.$si.','.$ps;
        
        $query =
        'SELECT SQL_CALC_FOUND_ROWS co.name, CONCAT("/Date(",co.lastpublish,"000)/") AS lastpublish, co.keywords
FROM mdl_'.centraladmindata_offer::$tablename.' co
INNER JOIN mdl_centraladmin_slave cs ON co.slaveid = cs.id
WHERE cs.disabled = 0 AND cs.deleted = 0 AND co.id IN('.implode(',',$coursesids).')'.$order.$limit;
        $records = $DB->get_records_sql($query);
        
        $recordCount = $DB->get_record_sql("SELECT FOUND_ROWS() AS found_rows")->found_rows;
        
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = $records;
        
        return array('data'=>json_encode($jTableResult));
    }
    
    
    
}

