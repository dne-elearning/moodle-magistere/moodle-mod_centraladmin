<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file defines an adhoc task to send notifications.
 *
 * @package    centraladmindata_offer
 * @copyright  2022 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace centraladmindata_offer\task;

defined('MOODLE_INTERNAL') || die();

class refresh_stats extends \core\task\scheduled_task {
    
    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('refresh_stats', 'centraladmindata_offer');
    }
    
    public function execute() {
        global $DB, $CFG;
        require_once($CFG->dirroot.'/mod/centraladmin/SlaveQuery.php');
        require_once($CFG->dirroot.'/mod/centraladmin/data/offer/offer.php');

        $username = get_config('mod_centraladmin', 'ws_usagemonitor_username');
        $password = get_config('mod_centraladmin', 'ws_usagemonitor_password');

        $slaves = \SlaveQuery::get_all_slaves();

        $transaction = $DB->start_delegated_transaction();
        
        $DB->delete_records(\centraladmindata_offer::$tablename);
        $DB->delete_records(\centraladmindata_offer::$completion_tablename);
        $DB->delete_records(\centraladmindata_offer::$attendance_tablename);

        foreach($slaves AS $slave) {
            $slaveresponse = \SlaveQuery::call_ws($slave, 'local_usagemonitor_service', 'local_usagemonitor_getdata', $username, $password, array('pluginname'=>'offer'));
            $data = \SlaveQuery::decode($slaveresponse->data);
            foreach($data->offer as $offerRecord) {
                $offerRecord->slaveid = $slave->id;
            }
            foreach($data->offer_completion as $offerCompletion) {
                $offerCompletion->slaveid = $slave->id;
            }
            foreach($data->offer_attendance as $offerAttendance) {
                $offerAttendance->slaveid = $slave->id;
            }
            
            $DB->insert_records(\centraladmindata_offer::$tablename, $data->offer);
            $DB->insert_records(\centraladmindata_offer::$completion_tablename, $data->offer_completion);
            $DB->insert_records(\centraladmindata_offer::$attendance_tablename, $data->offer_attendance);

        }

        // update last update timestamp :)
        set_config('lastupdate_timestamp', time(), 'centraladmindata_offer');

        $transaction->allow_commit();

    }

}

