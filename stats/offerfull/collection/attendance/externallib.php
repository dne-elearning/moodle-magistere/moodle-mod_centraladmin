<?php

require_once($CFG->libdir.'/externallib.php');
require_once($CFG->dirroot.'/mod/centraladmin/data/offer/offer.php');
require_once($CFG->dirroot.'/mod/centraladmin/stats/offerfull/offerfull.php');

class centraladmincollec_attendance_external extends external_api {

    public const PLUGIN_NAME = 'centraladminstats_offerfull';

    public static function get_per_degree_data_parameters() {
        return new external_function_parameters(
            array(
                'search'   => new external_value(PARAM_RAW, ''),
                'origins'  => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'keywords' => new external_value(PARAM_RAW, ''),
                'firstpublish_from'=> new external_value(PARAM_RAW, ''),
                'firstpublish_to'  => new external_value(PARAM_RAW, ''),
                'lastpublish_from' => new external_value(PARAM_RAW, ''),
                'lastpublish_to'   => new external_value(PARAM_RAW, ''),
                'engagedusers_min' => new external_value(PARAM_INT, ''),
                'engagedusers_max' => new external_value(PARAM_INT, ''),
                'publics' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'domains' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'durationconduct' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'gaiausers' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'collection' => new external_value(PARAM_ALPHA, ''),
                'interval' => new external_value(PARAM_ALPHA, ''),
            )
        );
    }

    public static function get_per_degree_data_returns() {
        return new external_single_structure(
            array(
                'enroled' => new external_value(PARAM_RAW, ''),
                'attended' => new external_value(PARAM_RAW, '')
            )
        );
    }

    public static function get_per_degree_data($search, $origins, $keywords, $firstpublish_from, $firstpublish_to, $lastpublish_from, $lastpublish_to, 
                                               $engagedusers_min, $engagedusers_max, $publics, $domains, $durationconduct, $gaiausers, $collection, $interval)
    {
        global $DB; 

        $offerfull = new centraladminstats_offerfull();
        $coursesids = $offerfull->search_coursesids($search, $origins, $keywords, $firstpublish_from, $firstpublish_to, $lastpublish_from, $lastpublish_to, $engagedusers_min, $engagedusers_max, $publics, $domains, $durationconduct, $gaiausers);
        
        if (count($coursesids) == 0) {
            return array(
                'enroled'=>'',
                'attended'=>''
            );
        }

        
        $duration = '';
        if ($interval == 'currentyear') {
            $duration = 'currentyear';
        } else if ($interval == 'lastyear') {
            $duration = 'lastyear';
        }


        $sql = 
"SELECT
SUM(a.nbparticipants".$duration."1d) as 'nbparticipants_1d', 
SUM(a.nbparticipants".$duration."2d) as 'nbparticipants_2d', 
SUM(a.nbparticipants".$duration."ndef)  as 'nbparticipants_nondefini',
SUM(a.nbstudents".$duration."1d) as 'nbstudents_1d', 
SUM(a.nbstudents".$duration."2d) as 'nbstudents_2d', 
SUM(a.nbstudents".$duration."ndef)  as 'nbstudents_nondefini'
FROM {".centraladmindata_offer::$attendance_tablename."} a
INNER JOIN {".centraladmindata_offer::$tablename."} offer ON offer.slaveid = a.slaveid AND offer.courseid = a.courseid
WHERE offer.id IN (".implode(',', $coursesids).")
";

        $record = $DB->get_record_sql($sql);
        return array(
            'enroled' => $record->nbstudents_1d.','.$record->nbstudents_2d.','.$record->nbstudents_nondefini,
            'attended' => $record->nbparticipants_1d.','.$record->nbparticipants_2d.','.$record->nbparticipants_nondefini
        );

    }


}

