<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * centraladminstats_offerfull external functions and service definitions.
 *
 * @package    centraladminstats_offerfull
 * @copyright  2022 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */



$services = array(
    'centraladminstats_offerfull_service' => array(
        'functions' => array(
            'centraladminstats_offerfull_getcollection',
            'centraladminstats_offerfull_getexport',
            'centraladminstats_offerfull_getpreview'
        ),
        'requiredcapability' => '',
        'restrictedusers' => 0,
        'enabled' => 1,
        'shortname' =>  'centraladminstats_offerfull_service',
        'downloadfiles' => 0,
        'uploadfiles'  => 0,
    )
);

$functions = array(
    'centraladminstats_offerfull_getcollection' => array(
        'classname' => 'centraladminstats_offerfull_external',
        'methodname' => 'get_collection',
        'classpath' => 'mod/centraladmin/stats/offerfull/externallib.php',
        'description' => 'Get data for a collection',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'centraladminstats_offerfull_getexport' => array(
        'classname' => 'centraladminstats_offerfull_external',
        'methodname' => 'get_export',
        'classpath' => 'mod/centraladmin/stats/offerfull/externallib.php',
        'description' => 'Get data for a collection',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    ),
    'centraladminstats_offerfull_getpreview' => array(
        'classname' => 'centraladminstats_offerfull_external',
        'methodname' => 'get_preview',
        'classpath' => 'mod/centraladmin/stats/offerfull/externallib.php',
        'description' => 'Get data for the jtable preview',
        'type' => 'read',
        'capabilities' => '',
        'ajax' => true,
    )
);
