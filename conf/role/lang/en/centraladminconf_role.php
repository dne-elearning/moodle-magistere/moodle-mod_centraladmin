<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'centraladmin', language 'en'
 *
 * @package   centraladminconf_role
 * @copyright 2022 Reseau Canope
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$string['pluginname'] = 'Capabilities management';
$string['pagedesc'] = 'Capabilities management';
$string['cacherefreshdelay'] = 'Cache refresh delay';
$string['cacherefreshdelay_desc'] = 'Capabilities cache refresh delay (in seconds)';
$string['array_header_capability'] = 'Capability';
$string['array_header_archetype'] = 'Archetype def';
$string['array_header_changeall'] = 'Change all';
$string['role'] = 'Rôle';
$string['instances'] = 'Instances';
$string['capabilities'] = 'Capabilities';
$string['onlyshowdiffarchetype'] = 'Only show capabilities that are different from the selected role archetype';
$string['onlyshowdiffinstance'] = 'Only show capabilities that are different on some instances';
$string['filter'] = 'Filter';
$string['save'] = 'Save';
$string['cancel'] = 'Cancel';
$string['export'] = 'Export';
$string['reset'] = 'Reset';
$string['changessummary'] = 'Summary of changes';
$string['todolifetime'] = 'Lifetime of the temporary edits';
$string['todolifetime_desc'] = 'Temporary permission modifications will be deleted after this duration (in seconds)';
$string['clean_todos'] = 'Clean the temporary edits';
$string['sync_archetype'] = 'Sync archetype';
$string['changerequestsrecap'] = '{$a->recap_capa} total permission changes on {$a->recap_instances} instances.';
$string['conflictsrecap'] = '<p>There are {$a} conflicts visible below.</p><p>Please, note that saving these changes will overwrite previous conflicts.</p>';
$string['role:viewpermissions'] = 'View the manage capabilities page';
$string['role:managepermissions'] = 'Manage the capabilities of the instances';